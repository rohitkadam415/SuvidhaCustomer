package com.suvidha.suvidharider;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;

import com.rengwuxian.materialedittext.MaterialEditText;

public class AddMoneyActivity extends AppCompatActivity implements View.OnClickListener {
    ImageButton backImageButton;
    private TextView tvTen, tvFifty, tvHundred, tvFiveHundred, btnAddMoney;
    private MaterialEditText amount;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_money);

        initialize();

        backImageButton.setOnClickListener(this);
        tvTen.setOnClickListener(this);
        tvFifty.setOnClickListener(this);
        tvHundred.setOnClickListener(this);
        tvFiveHundred.setOnClickListener(this);
        btnAddMoney.setOnClickListener(this);
    }

    private void initialize() {

        backImageButton=(ImageButton)findViewById(R.id.back);
        tvTen = (TextView) findViewById(R.id.tvTen);
        tvFifty = (TextView) findViewById(R.id.tvFifty);
        tvHundred = (TextView) findViewById(R.id.tvHundred);
        tvFiveHundred = (TextView) findViewById(R.id.tvFiveHundred);
        btnAddMoney = (TextView) findViewById(R.id.btnAddMoney);

        amount = (MaterialEditText) findViewById(R.id.amount);

    }

    @Override
    public void onClick(View view) {
        switch (view.getId())
        {
            case R.id.back:
                finish();
                break;

            case R.id.tvTen:
                addition(10);
                break;

            case R.id.tvFifty:
                addition(50);
                break;

            case R.id.tvHundred:
                addition(100);
                break;

            case R.id.tvFiveHundred:
                addition(500);
                break;
        }
    }

    public void addition(int i)
    {
        if (!amount.getText().toString().equals(""))
        {
            int a = Integer.valueOf(amount.getText().toString());
            int b = a + i;
            amount.setText(String.valueOf(b));
        }
        else
        {
            amount.setText(String.valueOf(i));
        }


    }
}
