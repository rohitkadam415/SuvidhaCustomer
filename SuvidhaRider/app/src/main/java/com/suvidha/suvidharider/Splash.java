package com.suvidha.suvidharider;

import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.RelativeLayout;

import com.google.gson.Gson;
import com.suvidha.suvidharider.Api.BaseClass;
import com.suvidha.suvidharider.Interface.GetRequestResponse;
import com.suvidha.suvidharider.Models.LastDriverDetails;
import com.suvidha.suvidharider.Models.RatingModel;
import com.suvidha.suvidharider.Models.SigninResponseModel;
import com.suvidha.suvidharider.Utilities.CheckPermission;
import com.suvidha.suvidharider.Utilities.Constants;
import com.suvidha.suvidharider.Utilities.SharedPreference;

import com.suvidha.suvidharider.Utilities.Camera_Gallery;
import com.suvidha.suvidharider.Utilities.TypefaceUtil;

public class Splash extends BaseClass {

    private static final int MY_PERMISSIONS_REQUEST_ACCOUNTS = 1;
    // Splash screen timer
    private static int SPLASH_TIME_OUT = 2000;
    private final int SPLASH_DISPLAY_LENGTH = 4000;
    private static final int REQUEST_CODE_ASK_MULTIPLE_PERMISSIONS = 5;
    private RelativeLayout activity_splash_page;
    Boolean isGpson=true;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        TypefaceUtil.overrideFont(getApplicationContext(), "SERIF", "ROBOTOREGULAR.TTF");

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (CheckPermission.checkAndRequestPermissions(Splash.this)) {
                startactivity();
            }

        } else {
            startactivity();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_ACCOUNTS:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    startactivity();
                } else {
                    finish();
                }
                break;
        }
    }

    public void startactivity() {
        if (Camera_Gallery.checkLocation(Splash.this)) {
            Handler handler = new Handler();
            handler.postDelayed(new Runnable() {
                public void run() {
                    // Actions to do after 3 seconds
                    if ((SharedPreference.getString(getApplicationContext(), Constants.MOBILE)) != null) {
                        checkLastDriverStatus();
                    } else {

                        Intent mainIntent = new Intent(getApplicationContext(), WelcomeActivity.class);
                        startActivity(mainIntent);
                        finish();
                    }

                }
            }, 3000);
        } else {
            isGpson=false;
            Camera_Gallery.showSettingsAlert(Splash.this);
        }
    }

    private void checkLastDriverStatus() {

        if (apiConnection != null)
        {
            RatingModel ratingModel = new RatingModel();
            ratingModel.setUserId(SharedPreference.getString(getBaseContext(), Constants.USER_ID));

            apiConnection.checkLastDriver(ratingModel, new GetRequestResponse() {
                @Override
                public void onResponse(String json) {

                    final Gson gson=new Gson();
                    final LastDriverDetails lastDriverDetails=gson.fromJson(json,LastDriverDetails.class);

                    Constants.lastDriverDetails = lastDriverDetails;

                    Intent mainIntent = new Intent(getApplicationContext(), ReviewYourTripActivity.class);
                    startActivity(mainIntent);
                    finish();
                }

                @Override
                public void onFailure(String json) {

                    Intent mainIntent = new Intent(getApplicationContext(), HomeActivity.class);
                    startActivity(mainIntent);
                    finish();
                }
            });

        }

    }


    @Override
    protected void onResume() {
        super.onResume();
        if(!isGpson && Camera_Gallery.checkLocation(Splash.this))
        {
            if ((SharedPreference.getString(getApplicationContext(), Constants.MOBILE)) != null) {
                Intent mainIntent = new Intent(getApplicationContext(), HomeActivity.class);
                startActivity(mainIntent);
            } else {
                Intent mainIntent = new Intent(getApplicationContext(), WelcomeActivity.class);
                startActivity(mainIntent);

            }
            finish();
        }
    }


}
