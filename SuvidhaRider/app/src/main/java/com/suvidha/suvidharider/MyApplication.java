package com.suvidha.suvidharider;

import android.app.Application;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.preference.PreferenceManager;
import android.text.TextUtils;


import com.suvidha.suvidharider.Utilities.Constants;
import com.suvidha.suvidharider.Utilities.SharedPreference;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;


public class MyApplication extends Application {

    private final String TAG = MyApplication.class.getSimpleName();

    public static int abc=0;

    private static MyApplication myApplicationInstance;

    /**
     * This variable is used to store the shared preferences
     */
    private SharedPreferences mySharedPreferences;

    private static Boolean isActive = false;


    private String myUserId;

    @Override
    public void onCreate() {
        super.onCreate();

        myApplicationInstance = this;

        mySharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);

        myUserId= SharedPreference.getString(myApplicationInstance, Constants.USER_ID);
        printHashKey();

    }

    public static MyApplication getMyApplicationInstance() {
        return myApplicationInstance;
    }

    public SharedPreferences getMySharedPreferencesInstance(){
        return mySharedPreferences;
    }



    public void setMyUserId()
    {
        myUserId= SharedPreference.getString(myApplicationInstance, Constants.USER_ID);
    }

    public String getMyUserId(){
        if (TextUtils.isEmpty(myUserId)){
            myUserId= SharedPreference.getString(myApplicationInstance, Constants.USER_ID);        }
        return myUserId;
    }

    public void printHashKey() {
        // Add code to print out the key hash
        try {
            PackageInfo info = getPackageManager().getPackageInfo(
                    getPackageName(),
                    PackageManager.GET_SIGNATURES);
            for (Signature signature : info.signatures) {
                MessageDigest md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
            }
        } catch (PackageManager.NameNotFoundException e) {

        } catch (NoSuchAlgorithmException e) {

        }
    }

    public static Boolean isActivityAvailable()
    {
        return isActive;
    }

    public static void setStart()
    {
         isActive = true;
    }

    public static void setStop()
    {
        isActive=false;
    }

}