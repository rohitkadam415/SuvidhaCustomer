package com.suvidha.suvidharider;

import android.app.ProgressDialog;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.makeramen.roundedimageview.RoundedImageView;
import com.suvidha.suvidharider.Api.BaseClass;
import com.suvidha.suvidharider.Interface.GetRequestResponse;
import com.suvidha.suvidharider.Models.RatingModel;
import com.suvidha.suvidharider.Utilities.Camera_Gallery;
import com.suvidha.suvidharider.Utilities.Constants;
import com.suvidha.suvidharider.Utilities.InternetCheck;
import com.suvidha.suvidharider.Utilities.SharedPreference;

import java.text.SimpleDateFormat;
import java.util.Calendar;

public class ReviewYourTripActivity extends BaseClass implements View.OnClickListener {

    private TextView tvTotalAmount, tvReviewTime, tvFrom, tvTo;
    private RatingBar ratingBar;
    private EditText etComment;
    private RoundedImageView imDriverPhoto;
    private Button btnHelp, btnRate;
    private ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_review_your_trip);

        initialize();
        btnRate.setOnClickListener(this);
        bindData();
    }

    private void bindData() {

//        tvFrom.setText(SharedPreference.getString(getBaseContext(), Constants.PREVIOUS_FROM_LOCATION));
//        tvTo.setText(SharedPreference.getString(getBaseContext(), Constants.PREVIOUS_TO_LOCATION));
//
//        if (SharedPreference.getString(getBaseContext(), Constants.PREVIOUS_PROFILE) != null) {
//            Glide.with(getApplicationContext())
//                    .load(SharedPreference.getString(getBaseContext(), Constants.PREVIOUS_PROFILE))
//                    .into(imDriverPhoto);
//        }
//
//        tvReviewTime.setText(SharedPreference.getString(getBaseContext(), Constants.PREVIOUS_TIME));

        tvFrom.setText(Camera_Gallery.getAddressFromLocation(getBaseContext(), Double.valueOf(Constants.lastDriverDetails.getRatingRideDetails().get(0).getFromLatitude()), Double.valueOf(Constants.lastDriverDetails.getRatingRideDetails().get(0).getFromLongitude())));
        tvTo.setText(Camera_Gallery.getAddressFromLocation(getBaseContext(), Double.valueOf(Constants.lastDriverDetails.getRatingRideDetails().get(0).getToLatitude()), Double.valueOf(Constants.lastDriverDetails.getRatingRideDetails().get(0).getToLongitude())));

        if (Constants.lastDriverDetails.getRatingRideDetails().get(0).getProfile() != null) {
            Glide.with(getApplicationContext())
                    .load(Constants.lastDriverDetails.getRatingRideDetails().get(0).getProfile())
                    .into(imDriverPhoto);
        }

        tvReviewTime.setText(Constants.lastDriverDetails.getRatingRideDetails().get(0).getDate());
        tvTotalAmount.setText(getString(R.string.Rs)+" "+Constants.lastDriverDetails.getRatingRideDetails().get(0).getTotalAmount());

    }

    private void initialize() {

        tvTotalAmount = (TextView) findViewById(R.id.tvTotalAmount);
        tvReviewTime = (TextView) findViewById(R.id.tvReviewTime);
        tvFrom = (TextView) findViewById(R.id.tvFrom);
        tvTo = (TextView) findViewById(R.id.tvTo);

        ratingBar = (RatingBar) findViewById(R.id.ratingBar);
        etComment = (EditText) findViewById(R.id.etComment);
        imDriverPhoto = (RoundedImageView) findViewById(R.id.imDriverPhoto);

        btnHelp = (Button) findViewById(R.id.btnHelp);
        btnRate = (Button) findViewById(R.id.btnRate);
    }

    @Override
    public void onClick(View view) {

        switch (view.getId())
        {
            case R.id.btnRate:

                if (etComment.getText().toString().equals(""))
                {
                    Camera_Gallery.openSnackBar(tvFrom, "Write comment", getApplicationContext());
                }
                else
                {
                    saveRatingDetails();
                }

                Log.e("Info", String.valueOf(ratingBar.getRating())+"//"+etComment.getText().toString());
                break;
        }
    }

    private void saveRatingDetails() {

        if(InternetCheck.getConnectivityStatus(getBaseContext()))
        {
            progressDialog = new ProgressDialog(ReviewYourTripActivity.this);
            progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            progressDialog.setMessage("Loading...");
            progressDialog.show();

            if (apiConnection != null) {

                RatingModel ratingModel = new RatingModel();
                ratingModel.setUserId(SharedPreference.getString(getBaseContext(), Constants.USER_ID));
                ratingModel.setDriverId(Constants.lastDriverDetails.getRatingRideDetails().get(0).getDriverId());
                ratingModel.setRating(String.valueOf(ratingBar.getRating()));
                ratingModel.setComment(etComment.getText().toString());
                ratingModel.setRideId(Constants.lastDriverDetails.getRatingRideDetails().get(0).getRideId());

                apiConnection.saveRating(ratingModel, new GetRequestResponse() {
                    @Override
                    public void onResponse(String json) {
                        progressDialog.dismiss();
                        Intent mainIntent = new Intent(getApplicationContext(), HomeActivity.class);
                        startActivity(mainIntent);
                    }

                    @Override
                    public void onFailure(String json) {
                        progressDialog.dismiss();
                    }
                });

            }
        }

    }
}
