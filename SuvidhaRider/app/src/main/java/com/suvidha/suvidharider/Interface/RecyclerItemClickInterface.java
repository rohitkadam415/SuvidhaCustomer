package com.suvidha.suvidharider.Interface;

/**
 * Created by cybertruimph on 3/17/2018.
 */

public interface RecyclerItemClickInterface {

    void onClick(String type, int position);
}
