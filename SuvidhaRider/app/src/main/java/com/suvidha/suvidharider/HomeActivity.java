package com.suvidha.suvidharider;

import android.app.Dialog;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.GradientDrawable;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.os.Bundle;

import android.os.IBinder;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.Window;
import android.view.animation.Animation;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlaceAutocomplete;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.makeramen.roundedimageview.RoundedImageView;
import com.suvidha.suvidharider.Adapters.RecyclerCabTypes;
import com.suvidha.suvidharider.Api.ApiConnection;
import com.suvidha.suvidharider.Api.SuvidhaRiderService;
import com.suvidha.suvidharider.Interface.GetRequestResponse;
import com.suvidha.suvidharider.Interface.RecyclerItemClickInterface;
import com.suvidha.suvidharider.Interface.RetrofitInterface;
import com.suvidha.suvidharider.Models.CarDetailsResponseModel;
import com.suvidha.suvidharider.Models.CarlocationModel;
import com.suvidha.suvidharider.Models.DistanceModel;
import com.suvidha.suvidharider.Models.DueDetail;
import com.suvidha.suvidharider.Models.Homelocation;
import com.suvidha.suvidharider.Models.Microdetail;
import com.suvidha.suvidharider.Models.Minidetail;
import com.suvidha.suvidharider.Models.RateDetail;
import com.suvidha.suvidharider.Models.Sedandetail;
import com.suvidha.suvidharider.MyBooking.Booking;
import com.suvidha.suvidharider.POJO.Example;
import com.suvidha.suvidharider.Utilities.Camera_Gallery;
import com.suvidha.suvidharider.Utilities.Constants;
import com.suvidha.suvidharider.Utilities.InternetCheck;
import com.suvidha.suvidharider.Utilities.SharedPreference;
import com.suvidha.suvidharider.Utilities.Validation;
import com.suvidha.suvidharider.Utilities.Wherebouts;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;


public class HomeActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener, OnMapReadyCallback, View.OnClickListener, RecyclerItemClickInterface {

    private static final int PLACE_AUTOCOMPLETE_FROM = 1;
    private static final int PLACE_AUTOCOMPLETE_TO = 2;
    private GoogleMap mMap;
    private int locationCount;
    private ImageView miniImageView, microImageView, sedanImageView;
    private RoundedImageView imageViewProfile;
    private TextView tvEditProfile;
    private TextView tvUserName, tvFrom, tvTo, tvMiniTime, tvMicroTime, tvSedanTime, tvService;
    private LinearLayout llEdit, llFrom, llTo, llCabs, miniLinearLayout, microLinearLayout, sedanLinearLayout, llLoad;
    private DrawerLayout drawer;
    private List<CarlocationModel> carlocationModels;
    private List<RateDetail> rateDetail = new ArrayList<>();
    private List<DueDetail> dueDetail = new ArrayList<>();
    Boolean isCancelled = false;
    private String fromLatitude = null, fromLongitude, fromPlaceName;
    private String toLatitude, toLongitude, toPlaceName;
    CameraPosition cameraPosition;
    Boolean mapcarsstatus = false;
    Polyline line;
    Boolean afterSelect = false;
    private String distance = "";
    private String encodedString = "";
    private String time, timewithunit;
    private double amt;
    GradientDrawable bgShapemaleImageView;
    private FloatingActionButton fabMyLocation;
    private ApiConnection apiConnection;
    private Double dueAmount = 0.0;
    long dueamt = 0;

    private ServiceConnection connection = new ServiceConnection() {

        public void onServiceConnected(ComponentName className,
                                       IBinder service) {
            apiConnection = (ApiConnection) service;
        }

        public void onServiceDisconnected(ComponentName arg0) {
        }

    };
    // private RelativeLayout rlNoInternet, rlMain;
    private Button btnRetry;
    private RecyclerView recycle_cab_list;
    private RecyclerCabTypes recyclerCabTypes;

    private static LatLngBounds BOUNDS_MOUNTAIN_VIEW =null;

    public ApiConnection getApiConnection() {
        return apiConnection;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        carlocationModels = new ArrayList<>();
        drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentByTag("mapFragment");
        if (mapFragment == null) {
            mapFragment = new SupportMapFragment();
            FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
            ft.add(R.id.mapFragmentContainer, mapFragment, "mapFragment");
            ft.commit();
            getSupportFragmentManager().executePendingTransactions();
        }
        mapFragment.getMapAsync(this);

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        View view = navigationView.getHeaderView(0);
        tvEditProfile = (TextView) view.findViewById(R.id.tvEditProfile);
        imageViewProfile = (RoundedImageView) view.findViewById(R.id.imageViewProfile);
        llEdit = (LinearLayout) view.findViewById(R.id.llEdit);
        llEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(HomeActivity.this, EditProfileActivity.class));

                if (drawer.isDrawerOpen(GravityCompat.START)) {
                    drawer.closeDrawer(GravityCompat.START);
                }
            }
        });

        tvUserName = (TextView) view.findViewById(R.id.tvUserName);

        initalize();


        tvFrom.setOnClickListener(this);
        tvTo.setOnClickListener(this);
        fabMyLocation.setOnClickListener(this);


        recyclerCabTypes = new RecyclerCabTypes(rateDetail, this);
        RecyclerView.LayoutManager lm1 = new LinearLayoutManager(getApplicationContext(), LinearLayoutManager.HORIZONTAL, false);
        recycle_cab_list.setLayoutManager(lm1);
        recycle_cab_list.setAdapter(recyclerCabTypes);

        bindService(new Intent(this, SuvidhaRiderService.class), connection, Context.BIND_AUTO_CREATE);

    }


    private void initalize() {

        tvFrom = (TextView) findViewById(R.id.tvFrom);
        tvTo = (TextView) findViewById(R.id.tvTo);
        tvService = (TextView) findViewById(R.id.tvService);

        llCabs = (LinearLayout) findViewById(R.id.llCabs);
        llLoad = (LinearLayout) findViewById(R.id.llLoad);

        miniImageView = (ImageView) findViewById(R.id.mini);
        microImageView = (ImageView) findViewById(R.id.micro);
        sedanImageView = (ImageView) findViewById(R.id.sedan);

        miniLinearLayout = (LinearLayout) findViewById(R.id.minitext);
        microLinearLayout = (LinearLayout) findViewById(R.id.microtext);
        sedanLinearLayout = (LinearLayout) findViewById(R.id.sedantext);

        tvMicroTime = (TextView) findViewById(R.id.tvMicroTime);
        tvMiniTime = (TextView) findViewById(R.id.tvMiniTime);
        tvSedanTime = (TextView) findViewById(R.id.tvSedanTime);

        fabMyLocation = (FloatingActionButton) findViewById(R.id.fabMyLocation);

        //   rlNoInternet = (RelativeLayout) findViewById(R.id.rlNoInternet);
        //  rlMain = (RelativeLayout) findViewById(R.id.rlMain);
        btnRetry = (Button) findViewById(R.id.btnRetry);

        recycle_cab_list = (RecyclerView) findViewById(R.id.recycle_cab_list);

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == PLACE_AUTOCOMPLETE_FROM) {
            if (resultCode == RESULT_OK) {
                Place place = PlaceAutocomplete.getPlace(this, data);

                fromLatitude = String.valueOf(place.getLatLng().latitude);
                fromLongitude = String.valueOf(place.getLatLng().longitude);
                fromPlaceName = place.getAddress().toString();
                tvFrom.setText(fromPlaceName);

                //  mMap.clear();

                cameraPosition = new CameraPosition.Builder().target(new LatLng(place.getLatLng().latitude, place.getLatLng().longitude)).zoom(17.0f).build();
                mMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));

                if (tvTo.getText().toString().length() > 0) {
                    afterSelect = true;
                    build_retrofit_and_get_response();
                }

            } else if (resultCode == PlaceAutocomplete.RESULT_ERROR) {
                Status
                        status = PlaceAutocomplete.getStatus(this, data);
                // TODO: Handle the error.
            } else if (resultCode == RESULT_CANCELED) {
                // The user canceled the operation.
            }
        }

        if (requestCode == PLACE_AUTOCOMPLETE_TO) {
            if (resultCode == RESULT_OK) {
                Place place = PlaceAutocomplete.getPlace(this, data);

                toLatitude = String.valueOf(place.getLatLng().latitude);
                toLongitude = String.valueOf(place.getLatLng().longitude);
                toPlaceName = place.getAddress().toString();
                tvTo.setText(toPlaceName);

                if (tvFrom.getText().toString().length() > 0) {
                    afterSelect = true;
                    build_retrofit_and_get_response();
                }

            } else if (resultCode == PlaceAutocomplete.RESULT_ERROR) {
                Status status = PlaceAutocomplete.getStatus(this, data);
                // TODO: Handle the error.
            } else if (resultCode == RESULT_CANCELED) {
                // The user canceled the operation.
            }
        }

//        if (requestCode == 2)
//        {
//                tv.setText(getIntent().getStringExtra("FromLocation"));
//                llCabs.setVisibility(View.VISIBLE);
//        }
    }

    @Override
    public void onBackPressed() {
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }


    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.home) {
            // Handle the camera action
        } else if (id == R.id.payment) {
            startActivity(new Intent(HomeActivity.this, PaymentActivity.class));
        } else if (id == R.id.wallet) {
            startActivity(new Intent(HomeActivity.this, WalletActivity.class));
        } else if (id == R.id.booking) {
            startActivity(new Intent(HomeActivity.this, Booking.class));
        } else if (id == R.id.notification) {
            startActivity(new Intent(HomeActivity.this, NotificationActivity.class));
        } else if (id == R.id.wallet) {
            startActivity(new Intent(HomeActivity.this, WalletActivity.class));
        } else if (id == R.id.setting) {
            startActivity(new Intent(HomeActivity.this, SettingsActivity.class));
        } else if (id == R.id.referearn) {
            startActivity(new Intent(HomeActivity.this, ReferEarnActivity.class));
        } else if (id == R.id.help) {
            startActivity(new Intent(HomeActivity.this, HelpActivity.class));
        } else if (id == R.id.about) {
            startActivity(new Intent(HomeActivity.this, AboutUsActivity.class));
        } else if (id == R.id.logout) {

            SharedPreference.removeAll(getApplicationContext());
            finish();
            startActivity(new Intent(HomeActivity.this, WelcomeActivity.class));
        }
        // drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        mMap.getUiSettings().setZoomControlsEnabled(false);
        mMap.getUiSettings().setZoomGesturesEnabled(false);

        mMap.setOnCameraIdleListener(new GoogleMap.OnCameraIdleListener() {
            @Override
            public void onCameraIdle() {
                // mMap.clear();
                fromLatitude = String.valueOf(mMap.getCameraPosition().target.latitude);
                fromLongitude = String.valueOf(mMap.getCameraPosition().target.longitude);
                llLoad.setVisibility(View.GONE);
                getAddressFromLocation();

            }
        });

    }

    @Override
    protected void onResume() {
        super.onResume();

        if (SharedPreference.getString(getApplicationContext(), Constants.PROFILE) != null) {
            Glide.with(getApplicationContext())
                    .load(SharedPreference.getString(getApplicationContext(), Constants.PROFILE))
                    .into(imageViewProfile);
        }
        tvUserName.setText(Validation.setCapitalizeletter(SharedPreference.getString(getApplicationContext(), Constants.FIRSTNAME)) + " " + Validation.setCapitalizeletter(SharedPreference.getString(getApplicationContext(), Constants.LASTNAME)));


        if (InternetCheck.getConnectivityStatus(getBaseContext())) {
            //rlMain.setVisibility(View.VISIBLE);
            Wherebouts.instance().onChange(new Wherebouts.Workable<Location>() {
                @Override
                public void work(Location gpsPoint) {
                    if (Camera_Gallery.checkLocation(HomeActivity.this)) {
                        SharedPreference.saveString(getBaseContext(), Constants.LATITUDE, String.valueOf(gpsPoint.getLatitude()));
                        SharedPreference.saveString(getBaseContext(), Constants.LONGITUDE, String.valueOf(gpsPoint.getLongitude()));

                        LatLng currentLocation = new LatLng(Double.valueOf(SharedPreference.getString(getBaseContext(), Constants.LATITUDE)), Double.valueOf(SharedPreference.getString(getBaseContext(), Constants.LONGITUDE)));
                        BOUNDS_MOUNTAIN_VIEW = new LatLngBounds(currentLocation, currentLocation);

                        locationCount = 0;
                        if (!isCancelled && !mapcarsstatus) {
                            fromLatitude = String.valueOf(gpsPoint.getLatitude());
                            fromLongitude = String.valueOf(gpsPoint.getLongitude());
                            //   getAddressFromLocation();
                            cameraPosition = new CameraPosition.Builder().target(new LatLng(gpsPoint.getLatitude(), gpsPoint.getLongitude())).zoom(17.0f).build();
                            mMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
                            mapcarsstatus = true;
                        }

                    } else {
                        if (locationCount == 0) {
                            locationCount = 1;
                            Camera_Gallery.showSettingsAlert(HomeActivity.this);
                        }
                    }

                }
            });
            Wherebouts.instance().start(this);
        }


    }

    @Override
    protected void onPause() {
        super.onPause();
        Wherebouts.instance().stop();
    }


    public void getMin(ArrayList<Double> inputArray, ArrayList<String> strings) {
        if (inputArray.size() > 0 && strings.size() > 0) {

            double minValue = inputArray.get(0);
            String minString = strings.get(0);
            for (int i = 1; i < inputArray.size(); i++) {
                if (inputArray.get(i) < minValue) {
                    minValue = inputArray.get(i);
                    minString = strings.get(i);
                }
            }

            switch (minString) {
                case "1":
                    microImageView.setImageResource(R.drawable.circlecar3_150);
                    miniImageView.setImageResource(R.drawable.circlecar1_100);
                    sedanImageView.setImageResource(R.drawable.circlecar2_100);
                    break;

                case "2":
                    miniImageView.setImageResource(R.drawable.circlecar1_150);
                    microImageView.setImageResource(R.drawable.circlecar3_100);
                    sedanImageView.setImageResource(R.drawable.circlecar2_100);
                    break;

                case "3":
                    microImageView.setImageResource(R.drawable.circlecar3_100);
                    miniImageView.setImageResource(R.drawable.circlecar1_100);
                    sedanImageView.setImageResource(R.drawable.circlecar2_150);
                    break;
            }
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        isCancelled = true;
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {

            case R.id.tvFrom:
                try {
                    Intent intent =
                            new PlaceAutocomplete.IntentBuilder(PlaceAutocomplete.MODE_FULLSCREEN)
                                    .setBoundsBias(BOUNDS_MOUNTAIN_VIEW)
                                    .build(this);
                    startActivityForResult(intent, PLACE_AUTOCOMPLETE_FROM);
                } catch (GooglePlayServicesRepairableException e) {
                    // TODO: Handle the error.
                } catch (GooglePlayServicesNotAvailableException e) {
                    // TODO: Handle the error.
                }
                break;

            case R.id.tvTo:
                try {
                    Intent intent =
                            new PlaceAutocomplete.IntentBuilder(PlaceAutocomplete.MODE_FULLSCREEN)
                                    .setBoundsBias(BOUNDS_MOUNTAIN_VIEW)
                                    .build(this);
                    startActivityForResult(intent, PLACE_AUTOCOMPLETE_TO);
                } catch (GooglePlayServicesRepairableException e) {
                    // TODO: Handle the error.
                } catch (GooglePlayServicesNotAvailableException e) {
                    // TODO: Handle the error.
                }
                break;

            case R.id.fabMyLocation:

                fromLatitude = SharedPreference.getString(getApplicationContext(), Constants.LATITUDE);
                fromLongitude = SharedPreference.getString(getApplicationContext(), Constants.LONGITUDE);
                cameraPosition = new CameraPosition.Builder().target(new LatLng(Double.valueOf(fromLatitude), Double.valueOf(fromLongitude))).zoom(17.0f).build();
                mMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));

                break;
        }
    }

    private void getAddressFromLocation() {

        Geocoder geocoder = new Geocoder(this, Locale.ENGLISH);
        try {
            List<Address> addresses = geocoder.getFromLocation(Double.valueOf(fromLatitude), Double.valueOf(fromLongitude), 1);

            if (addresses.size() > 0) {
                Address fetchedAddress = addresses.get(0);
                StringBuilder strAddress = new StringBuilder();
                for (int i = 0; i <= fetchedAddress.getMaxAddressLineIndex(); i++) {
                    strAddress.append(fetchedAddress.getAddressLine(i)).append(" ");
                }

                tvFrom.setText(strAddress.toString());
                getallcars1(fromLatitude, fromLongitude);

            } else {
                tvFrom.setText("Searching Current Address");
            }

        } catch (IOException e) {
            e.printStackTrace();
            //  printToast("Could not get address..!");
        }
    }

    private void getallcars1(final String fromLatitude, String fromLongitude) {
        llLoad.setVisibility(View.VISIBLE);
        tvService.setText("Loading....");

        Homelocation homelocation = new Homelocation();
        homelocation.setLatitude(fromLatitude);
        homelocation.setLongitude(fromLongitude);
        homelocation.setUserId(SharedPreference.getString(getBaseContext(), Constants.USER_ID));

        if (apiConnection != null) {
            apiConnection.getAllCars(homelocation, new GetRequestResponse() {
                @Override
                public void onResponse(String jsonString) {
                    final Gson gson = new Gson();
                    final CarDetailsResponseModel carDetailsResponseModel = gson.fromJson(jsonString, CarDetailsResponseModel.class);
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            if (carlocationModels.size() > 0) {
                                carlocationModels.clear();
                                mMap.clear();
                            }
                            llLoad.setVisibility(View.GONE);
                            recycle_cab_list.setVisibility(View.VISIBLE);
                            final LatLngBounds.Builder builder = LatLngBounds.builder();
                            carlocationModels = carDetailsResponseModel.getCarlocation();
                            rateDetail = carDetailsResponseModel.getRateDetails();
                            dueDetail = carDetailsResponseModel.getDueDetails();

                            recyclerCabTypes.updateRecycle(rateDetail);

                            for (int i = 0; i < carlocationModels.size(); i++) {
                                LatLng latLng = new LatLng(Double.valueOf(carlocationModels.get(i).getLatitude()), Double.valueOf(carlocationModels.get(i).getLongitude()));

                                if (carlocationModels.get(0).getService().equals("Cars")) {
                                    mMap.addMarker(new MarkerOptions().position(latLng).icon(BitmapDescriptorFactory.fromResource(R.drawable.micro)));
                                }
                                if (carlocationModels.get(0).getService().equals("ERikshaw")) {
                                    mMap.addMarker(new MarkerOptions().position(latLng).icon(BitmapDescriptorFactory.fromResource(R.drawable.mini)));
                                }
                                if (carlocationModels.get(0).getService().equals("Auto")) {
                                    mMap.addMarker(new MarkerOptions().position(latLng).icon(BitmapDescriptorFactory.fromResource(R.drawable.sedan)));
                                }

                                //////////////////////////


//                                if (carlocationModels.get(0).getServiceId().equals("1")) {
//                                    mMap.addMarker(new MarkerOptions().position(latLng).icon(BitmapDescriptorFactory.fromResource(R.drawable.micro)));
//                                }
//                                if (carlocationModels.get(0).getServiceId().equals("2")) {
//                                    mMap.addMarker(new MarkerOptions().position(latLng).icon(BitmapDescriptorFactory.fromResource(R.drawable.mini)));
//                                }
//                                if (carlocationModels.get(0).getServiceId().equals("3")) {
//                                    mMap.addMarker(new MarkerOptions().position(latLng).icon(BitmapDescriptorFactory.fromResource(R.drawable.sedan)));
//                                }



                                ////////////////////////////////

                                builder.include(latLng);
                            }
                            if (fromLatitude != null) {
                                build_retrofit_and_get_response();
                            }
                        }
                    });


                }

                @Override
                public void onFailure(String msg) {
                    mMap.clear();
                    tvService.setText("Service Not Available");
                    recycle_cab_list.setVisibility(View.GONE);
                }
            });
        }
    }


    public void animateLayout() {
//        TranslateAnimation animate = new TranslateAnimation(
//                0,                 // fromXDelta
//                0,                 // toXDelta
//                llCabs.getHeight(),  // fromYDelta
//                0);                // toYDelta
//        animate.setDuration(1200);
//        llCabs.startAnimation(animate);
        llLoad.setVisibility(View.VISIBLE);
    }

    private void build_retrofit_and_get_response() {
        DistanceModel distanceModel = new DistanceModel();
        distanceModel.setFromlatitude(fromLatitude);
        distanceModel.setFromlongitude(fromLongitude);
        distanceModel.setTolatitude(toLatitude);
        distanceModel.setTolongitude(toLongitude);

        if (apiConnection != null) {

            apiConnection.checkDistance(distanceModel, new GetRequestResponse() {
                @Override
                public void onResponse(String s) {
                    String json = s;
                    try {
                        JsonParser jsonParser = new JsonParser();
                        //convert from JSON string to JSONObject
                        JsonObject data = null;
                        data = (JsonObject) jsonParser.parse(s);
                        distance = data.get("distance").getAsString();
                        time = data.get("time").getAsString();
                        timewithunit = data.get("timewithunit").getAsString();
                        encodedString = data.get("encodedString").getAsString();
                        Constants.ENCODED_STRING = encodedString;

                    } catch (Exception e) {
                        String a = e.toString();
                    }
                }

                @Override
                public void onFailure(String msg) {

                }
            });

        }

    }


    @Override
    public void onClick(String type, int position) {

        if (toLatitude != null && toLongitude != null) {
            if (distance != null) {
                openDialog(type, rateDetail.get(position).getMaxpersons(), position);
            }

        } else {
            Camera_Gallery.openSnackBar(tvFrom, "Select To Location", getApplicationContext());
        }

    }

    private void openDialog(final String title, String maxpersons, int position) {

        final Dialog dialog = new Dialog(HomeActivity.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.setContentView(R.layout.dialog_city_cab);
        dialog.setCancelable(false);

        TextView tvFareEsti = (TextView) dialog.findViewById(R.id.tvFareEsti);
        TextView tvCancel = (TextView) dialog.findViewById(R.id.tvCancel);
        TextView tvTitle = (TextView) dialog.findViewById(R.id.tvTitle);
        TextView tvEstiTime = (TextView) dialog.findViewById(R.id.tvEstiTime);
        TextView tvMaxPerson = (TextView) dialog.findViewById(R.id.tvMaxPerson);
        TextView tvMinFare = (TextView) dialog.findViewById(R.id.tvMinFare);
        TextView tvGross = (TextView) dialog.findViewById(R.id.tvGross);
        TextView tvFare = (TextView) dialog.findViewById(R.id.tvFare);
        TextView tvDue = (TextView) dialog.findViewById(R.id.tvDue);
        LinearLayout llGross = (LinearLayout) dialog.findViewById(R.id.llGross);

        try {

            double distanceDouble = Double.valueOf(distance);
            double timeDouble = Double.valueOf(time);
            amt = Double.valueOf(rateDetail.get(position).getBasicRate()) + (distanceDouble * Double.valueOf(rateDetail.get(position).getKmRate())) + (timeDouble * Double.valueOf(rateDetail.get(position).getMinRate()));
        } catch (Exception e) {
            Log.e("Exception", e + "");
        }
        final long amount = Math.round(amt);


        if (dueDetail != null)
        {
            llGross.setVisibility(View.VISIBLE);
            dueAmount = Double.valueOf(dueDetail.get(0).getAmount());
            dueamt= Math.round(dueAmount);
            tvFare.setText(getResources().getString(R.string.Rs) + "" + String.valueOf(amount));
            tvDue.setText(getResources().getString(R.string.Rs) + "" + String.valueOf(dueamt));
        }
        else
        {
            llGross.setVisibility(View.GONE);
            tvGross.setVisibility(View.GONE);
        }

        final Double d = Double.valueOf(amount)+dueamt;
        tvTitle.setText(title);
        tvEstiTime.setText(timewithunit);
        tvMaxPerson.setText(maxpersons);
        tvMinFare.setText(getResources().getString(R.string.Rs) + "" + String.valueOf(Math.round(d)));

        tvFareEsti.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(HomeActivity.this, FareEstimateActivity.class);
                i.putExtra(Constants.APPROX_TIME, timewithunit);
                i.putExtra(Constants.FROM_LOCATION, tvFrom.getText().toString());
                i.putExtra(Constants.TO_LOCATION, tvTo.getText().toString());
                i.putExtra(Constants.FARE_AMT, String.valueOf(amount));
                i.putExtra(Constants.DUE_AMT, String.valueOf(dueamt));

                Constants.FROM_LATTITUDE = fromLatitude;
                Constants.FROM_LONGITUDE = fromLongitude;
                Constants.TO_LATTITUDE = toLatitude;
                Constants.TO_LONGITUDE = toLongitude;
                Constants.SERVICE = title;
              //  Constants.SERVICE = carlocationModels.get(0).getServiceId();

                startActivity(i);
                dialog.dismiss();
            }
        });

        tvCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });
        dialog.show();
    }

}

