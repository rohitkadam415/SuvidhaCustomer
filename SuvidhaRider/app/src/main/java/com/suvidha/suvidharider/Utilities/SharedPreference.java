package com.suvidha.suvidharider.Utilities;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by kedar on 12/12/2017.
 */

public class SharedPreference {

    private static SharedPreferences getSharePreferenceInstance(Context context) {
        SharedPreferences mySharedPreferences = context.getSharedPreferences("PREFERENCE", Context.MODE_PRIVATE);

        return mySharedPreferences;
    }
    public static void saveInt(Context context,String key, int value) {
        SharedPreferences.Editor editor=getSharePreferenceInstance(context).edit();
        editor.putInt(key, value);
        editor.apply();
    }

    public static void saveBoolean(Context context,String key, Boolean value) {
        SharedPreferences.Editor editor=getSharePreferenceInstance(context).edit();
        editor.putBoolean(key, value);
        editor.apply();
    }

    public static void saveString(Context context,String key, String value) {
        SharedPreferences.Editor editor=getSharePreferenceInstance(context).edit();
        editor.putString(key, value);
        editor.apply();
    }

    public static String getString(Context context,String key)
    {

        if(getSharePreferenceInstance(context).contains(key))
        return getSharePreferenceInstance(context).getString(key,null);
        else
            return null;
    }

    public static Integer getInt(Context context,String key)
    {
        if(getSharePreferenceInstance(context).contains(key))
            return getSharePreferenceInstance(context).getInt(key,0);
        else
            return 0;
    }

    public static boolean getBoolean(Context context,String key)
    {
        if(getSharePreferenceInstance(context).contains(key))
            return getSharePreferenceInstance(context).getBoolean(key,true);
        else
            return false;
    }

    public static void removeString(Context context,String key) {
        SharedPreferences.Editor editor=getSharePreferenceInstance(context).edit();
        editor.remove(key);
        editor.apply();
    }

    public static void removeAll(Context context) {
        String fcm_token = getString(context, Constants.FCM_TOKEN);
        SharedPreferences.Editor editor=getSharePreferenceInstance(context).edit();
        editor.clear();
        editor.apply();

        saveString(context, Constants.FCM_TOKEN, fcm_token);
    }

}
