package com.suvidha.suvidharider;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.suvidha.suvidharider.Api.BaseClass;
import com.suvidha.suvidharider.Interface.GetRequestResponse;
import com.suvidha.suvidharider.Models.DistanceModel;
import com.suvidha.suvidharider.Utilities.Constants;
import com.suvidha.suvidharider.Utilities.SharedPreference;

public class PaymentActivity extends BaseClass implements View.OnClickListener {

    private ImageButton back;
    private RelativeLayout rlPaytm, rlCreditDebit, rlCash;
    private ImageView imPaytm, imCreditDebit, imCash;
    private Button btnDone;
    private String payment_type = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_payment);

        intialize();

        back.setOnClickListener(this);
        rlPaytm.setOnClickListener(this);
        rlCreditDebit.setOnClickListener(this);
        rlCash.setOnClickListener(this);
        btnDone.setOnClickListener(this);
    }

    private void intialize() {

        back = (ImageButton) findViewById(R.id.back);
        rlPaytm = (RelativeLayout) findViewById(R.id.rlPaytm);
        rlCreditDebit = (RelativeLayout) findViewById(R.id.rlCreditDebit);
        rlCash = (RelativeLayout) findViewById(R.id.rlCash);

        imPaytm = (ImageView) findViewById(R.id.imPaytm);
        imCreditDebit = (ImageView) findViewById(R.id.imCreditDebit);
        imCash = (ImageView) findViewById(R.id.imCash);

        btnDone = (Button) findViewById(R.id.btnDone);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId())
        {
            case R.id.back:
                finish();
                break;

            case R.id.rlPaytm:
                imPaytm.setVisibility(View.VISIBLE);
                imCreditDebit.setVisibility(View.INVISIBLE);
                imCash.setVisibility(View.INVISIBLE);
                payment_type = "paytm";
                break;

            case R.id.rlCreditDebit:
                imPaytm.setVisibility(View.INVISIBLE);
                imCreditDebit.setVisibility(View.VISIBLE);
                imCash.setVisibility(View.INVISIBLE);
                payment_type = "creditdebit";
                break;

            case R.id.rlCash:
                imPaytm.setVisibility(View.INVISIBLE);
                imCreditDebit.setVisibility(View.INVISIBLE);
                imCash.setVisibility(View.VISIBLE);
                payment_type = "cash";
                break;

            case R.id.btnDone:

                changePaymentType();
                break;
        }
    }

    private void changePaymentType() {

        if (apiConnection != null)
        {
            DistanceModel distanceModel = new DistanceModel();
            distanceModel.setPaymenttype(payment_type);
            distanceModel.setUserId(SharedPreference.getString(getBaseContext(), Constants.USER_ID));
            apiConnection.changepaymentMode(distanceModel, new GetRequestResponse() {
                @Override
                public void onResponse(String json) {


                }

                @Override
                public void onFailure(String json) {

                }
            });
        }
    }
}
