package com.suvidha.suvidharider.Models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class UserDetailsModel {

    @SerializedName("Firstname")
    @Expose
    private String firstname;
    @SerializedName("Lastname")
    @Expose
    private String lastname;
    @SerializedName("Email")
    @Expose
    private String email;
    @SerializedName("UserId")
    @Expose
    private String userId;
    @SerializedName("Profile")
    @Expose
    private String profile;
    @SerializedName("Mobile")
    @Expose
    private String mobile;
    @SerializedName("CountryCode")
    @Expose
    private String countryCode;

//    @SerializedName("ReferCode")
//    @Expose
//    private String referCode;


    public String getCountryCode() {
        return countryCode;
    }

    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getProfile() {
        return profile;
    }

    public void setProfile(String profile) {
        this.profile = profile;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

//    public String getReferCode() {
//        return referCode;
//    }
//
//    public void setReferCode(String referCode) {
//        this.referCode = referCode;
//    }
}