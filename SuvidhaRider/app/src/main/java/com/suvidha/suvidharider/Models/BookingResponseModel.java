package com.suvidha.suvidharider.Models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by cybertruimph on 3/12/2018.
 */

public class BookingResponseModel {

    @SerializedName("success")
    @Expose
    private String success;
    @SerializedName("DriverDetails")
    @Expose
    private List<DriverDetailsModel> driverDetailsModels = null;
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("message")
    @Expose
    private String message;

    public String getSuccess() {
        return success;
    }

    public void setSuccess(String success) {
        this.success = success;
    }

    public List<DriverDetailsModel> getDriverDetailsModels() {
        return driverDetailsModels;
    }

    public void setDriverDetailsModels(List<DriverDetailsModel> driverDetailsModels) {
        this.driverDetailsModels = driverDetailsModels;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
