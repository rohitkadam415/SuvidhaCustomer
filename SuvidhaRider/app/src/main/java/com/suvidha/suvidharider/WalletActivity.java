package com.suvidha.suvidharider;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;

public class WalletActivity extends AppCompatActivity implements View.OnClickListener {

    ImageButton backImageButton;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_wallet);

        backImageButton=(ImageButton)findViewById(R.id.back);
        LinearLayout llAddMoney = (LinearLayout) findViewById(R.id.llAddMoney);
        backImageButton.setOnClickListener(this);
        llAddMoney.setOnClickListener(this);

        TextView tvAmt  = (TextView) findViewById(R.id.tvAmt);
        tvAmt.setText(getString(R.string.Rs)+" 0.00");
    }

    @Override
    public void onClick(View view) {
        switch (view.getId())
        {
            case R.id.back:
                finish();
                break;

            case R.id.llAddMoney:
                startActivity(new Intent(WalletActivity.this, AddMoneyActivity.class));
                break;
        }
    }
}
