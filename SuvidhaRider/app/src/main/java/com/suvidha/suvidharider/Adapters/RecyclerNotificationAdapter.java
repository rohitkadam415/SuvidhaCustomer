package com.suvidha.suvidharider.Adapters;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.suvidha.suvidharider.Models.MessageDetail;
import com.suvidha.suvidharider.Models.NotificationModel;
import com.suvidha.suvidharider.R;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by cybertruimph on 2/10/2018.
 */

public class RecyclerNotificationAdapter extends RecyclerView.Adapter<RecyclerNotificationAdapter.MyCredits> {

    private List<MessageDetail> pList;

    public RecyclerNotificationAdapter(List<MessageDetail> pList) {
        this.pList = pList;
    }

    public class MyCredits extends RecyclerView.ViewHolder {

        private final TextView tvDatee, tvMessage, tvTimee;
        private final ImageView imImage;

        public MyCredits(View itemView) {
            super(itemView);
            tvDatee = (TextView) itemView.findViewById(R.id.tvDatee);
            tvMessage = (TextView) itemView.findViewById(R.id.tvMessage);
            tvTimee = (TextView) itemView.findViewById(R.id.tvTimee);
            imImage = (ImageView) itemView.findViewById(R.id.imImage);
        }
    }

    @Override
    public RecyclerNotificationAdapter.MyCredits onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.custom_recycler_notifications, parent, false);
        return new RecyclerNotificationAdapter.MyCredits(itemView);
    }

    @Override
    public void onBindViewHolder(RecyclerNotificationAdapter.MyCredits holder, int position) {

//        String datetime = pList.get(position).getDate();
//        String date[] = datetime.split(" ");

        holder.tvDatee.setText(pList.get(position).getDate());
        holder.tvMessage.setText(pList.get(position).getMessage());
        holder.tvTimee.setText(pList.get(position).getTime());
       // holder.imImage.setImageResource(pList.get(position).getImage());
    }

    @Override
    public int getItemCount() {
        return pList.size();
    }


}
