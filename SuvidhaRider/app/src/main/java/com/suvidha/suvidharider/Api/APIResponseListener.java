package com.suvidha.suvidharider.Api;

import android.support.annotation.NonNull;

import okhttp3.Call;
import okhttp3.Response;

/**
 * Created by anuj on 23/12/17.
 */

public interface APIResponseListener {
    void onFailure(String message);
    void onResponse(Response response);
}