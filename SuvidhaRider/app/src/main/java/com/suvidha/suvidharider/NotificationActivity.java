package com.suvidha.suvidharider;

import android.app.ProgressDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;

import com.google.gson.Gson;
import com.suvidha.suvidharider.Adapters.RecyclerNotificationAdapter;
import com.suvidha.suvidharider.Api.BaseClass;
import com.suvidha.suvidharider.Interface.GetRequestResponse;
import com.suvidha.suvidharider.Models.MessageDetail;
import com.suvidha.suvidharider.Models.NotificationModel;
import com.suvidha.suvidharider.Models.RatingModel;
import com.suvidha.suvidharider.Models.SigninResponseModel;
import com.suvidha.suvidharider.Utilities.Constants;
import com.suvidha.suvidharider.Utilities.SharedPreference;

import java.util.ArrayList;
import java.util.List;

public class NotificationActivity extends BaseClass {

    private RecyclerView recycle_notifications;
    ImageButton backImageButton;

    private List<MessageDetail> notificationModelArrayList = new ArrayList<>();
    private TextView tvMessage;
    private ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notification);

        backImageButton=(ImageButton)findViewById(R.id.back);
        backImageButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        recycle_notifications = (RecyclerView) findViewById(R.id.recycle_notifications);
        tvMessage = (TextView) findViewById(R.id.tvMessage);


//        if (notificationModelArrayList.size() != 0)
//        {
//
//        }
//        else
//        {
//            recycle_notifications.setVisibility(View.GONE);
//            tvMessage.setVisibility(View.VISIBLE);
//        }

    }

    public void getNotificationDetails() {

        if (apiConnection != null) {

            progressDialog = new ProgressDialog(NotificationActivity.this);
            progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            progressDialog.setMessage("Loading...");
            progressDialog.setCanceledOnTouchOutside(false);
            progressDialog.show();
            RatingModel ratingModel = new RatingModel();
            ratingModel.setUserId(SharedPreference.getString(getBaseContext(), Constants.USER_ID));

            apiConnection.getNotification(ratingModel, new GetRequestResponse() {
                @Override
                public void onResponse(String json) {
                    progressDialog.dismiss();
                    final Gson gson=new Gson();
                    final NotificationModel notificationModel=gson.fromJson(json,NotificationModel.class);

                    notificationModelArrayList = notificationModel.getMessageDetails();

                    recycle_notifications.setVisibility(View.VISIBLE);
                    tvMessage.setVisibility(View.GONE);
                    RecyclerNotificationAdapter recyclerNotificationAdapter = new RecyclerNotificationAdapter(notificationModelArrayList);
                    RecyclerView.LayoutManager lm1 = new LinearLayoutManager(getApplicationContext(), LinearLayoutManager.VERTICAL, false);
                    recycle_notifications.setLayoutManager(lm1);
                    recycle_notifications.setAdapter(recyclerNotificationAdapter);

                }

                @Override
                public void onFailure(String json) {
                    progressDialog.dismiss();
                    recycle_notifications.setVisibility(View.GONE);
                    tvMessage.setVisibility(View.VISIBLE);
                }
            });
        }
    }

    @Override
    protected void apiConnected() {
        getNotificationDetails();
    }
}
