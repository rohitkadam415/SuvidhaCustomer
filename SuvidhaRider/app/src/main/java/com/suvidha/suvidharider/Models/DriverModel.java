package com.suvidha.suvidharider.Models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by cybertruimph on 4/2/2018.
 */

public class DriverModel {

    @SerializedName("success")
    @Expose
    private String success;

    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("message")
    @Expose
    private String message;

    @SerializedName("FromLatitude")
    @Expose
    private String fromlatitude;
    @SerializedName("FromLogitude")
    @Expose
    private String fromlongitude;

    @SerializedName("ReachTime")
    @Expose
    private String reachtime;


    public String getFromlatitude() {
        return fromlatitude;
    }

    public void setFromlatitude(String fromlatitude) {
        this.fromlatitude = fromlatitude;
    }

    public String getFromlongitude() {
        return fromlongitude;
    }

    public void setFromlongitude(String fromlongitude) {
        this.fromlongitude = fromlongitude;
    }

    public String getSuccess() {
        return success;
    }

    public void setSuccess(String success) {
        this.success = success;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getReachtime() {
        return reachtime;
    }

    public void setReachtime(String reachtime) {
        this.reachtime = reachtime;
    }
}
