package com.suvidha.suvidharider.Models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by cybertruimph on 3/21/2018.
 */

public class ConfirmBookingModel {

    @SerializedName("UserId")
    @Expose
    private String UserId;

    @SerializedName("RequestId")
    @Expose
    private String requestId;

    public String getUserId() {
        return UserId;
    }

    public void setUserId(String userId) {
        UserId = userId;
    }

    public String getRequestId() {
        return requestId;
    }

    public void setRequestId(String requestId) {
        this.requestId = requestId;
    }
}
