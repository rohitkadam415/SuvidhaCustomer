package com.suvidha.suvidharider.Models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class RatingRideDetail {

    @SerializedName("FareAmount")
    @Expose
    private String fareAmount;
    @SerializedName("TollAmount")
    @Expose
    private String tollAmount;
    @SerializedName("ParkingAmount")
    @Expose
    private String parkingAmount;
    @SerializedName("DueAmount")
    @Expose
    private String dueAmount;
    @SerializedName("DistanceInKm")
    @Expose
    private String distanceInKm;
    @SerializedName("TotalAmount")
    @Expose
    private String totalAmount;
    @SerializedName("FromLatitude")
    @Expose
    private String fromLatitude;
    @SerializedName("FromLongitude")
    @Expose
    private String fromLongitude;
    @SerializedName("ToLatitude")
    @Expose
    private String toLatitude;
    @SerializedName("ToLongitude")
    @Expose
    private String toLongitude;
    @SerializedName("DriverId")
    @Expose
    private String driverId;
    @SerializedName("Service")
    @Expose
    private String service;
    @SerializedName("VehicleNumber")
    @Expose
    private String vehicleNumber;
    @SerializedName("VehicleType")
    @Expose
    private String vehicleType;
    @SerializedName("Firstname")
    @Expose
    private String firstname;
    @SerializedName("Status")
    @Expose
    private String status;
    @SerializedName("Lastname")
    @Expose
    private String lastname;
    @SerializedName("Date")
    @Expose
    private String date;
    @SerializedName("Profile")
    @Expose
    private String profile;
    @SerializedName("RideId")
    @Expose
    private String rideId;

    public String getFareAmount() {
        return fareAmount;
    }

    public void setFareAmount(String fareAmount) {
        this.fareAmount = fareAmount;
    }

    public String getTollAmount() {
        return tollAmount;
    }

    public void setTollAmount(String tollAmount) {
        this.tollAmount = tollAmount;
    }

    public String getParkingAmount() {
        return parkingAmount;
    }

    public void setParkingAmount(String parkingAmount) {
        this.parkingAmount = parkingAmount;
    }

    public String getDueAmount() {
        return dueAmount;
    }

    public void setDueAmount(String dueAmount) {
        this.dueAmount = dueAmount;
    }

    public String getDistanceInKm() {
        return distanceInKm;
    }

    public void setDistanceInKm(String distanceInKm) {
        this.distanceInKm = distanceInKm;
    }

    public String getTotalAmount() {
        return totalAmount;
    }

    public void setTotalAmount(String totalAmount) {
        this.totalAmount = totalAmount;
    }

    public String getFromLatitude() {
        return fromLatitude;
    }

    public void setFromLatitude(String fromLatitude) {
        this.fromLatitude = fromLatitude;
    }

    public String getFromLongitude() {
        return fromLongitude;
    }

    public void setFromLongitude(String fromLongitude) {
        this.fromLongitude = fromLongitude;
    }

    public String getToLatitude() {
        return toLatitude;
    }

    public void setToLatitude(String toLatitude) {
        this.toLatitude = toLatitude;
    }

    public String getToLongitude() {
        return toLongitude;
    }

    public void setToLongitude(String toLongitude) {
        this.toLongitude = toLongitude;
    }

    public String getDriverId() {
        return driverId;
    }

    public void setDriverId(String driverId) {
        this.driverId = driverId;
    }

    public String getService() {
        return service;
    }

    public void setService(String service) {
        this.service = service;
    }

    public String getVehicleNumber() {
        return vehicleNumber;
    }

    public void setVehicleNumber(String vehicleNumber) {
        this.vehicleNumber = vehicleNumber;
    }

    public String getVehicleType() {
        return vehicleType;
    }

    public void setVehicleType(String vehicleType) {
        this.vehicleType = vehicleType;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getProfile() {
        return profile;
    }

    public void setProfile(String profile) {
        this.profile = profile;
    }

    public String getRideId() {
        return rideId;
    }

    public void setRideId(String rideId) {
        this.rideId = rideId;
    }

}


