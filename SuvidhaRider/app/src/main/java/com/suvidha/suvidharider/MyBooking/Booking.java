package com.suvidha.suvidharider.MyBooking;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageButton;

import com.google.gson.Gson;
import com.suvidha.suvidharider.Api.BaseClass;
import com.suvidha.suvidharider.Fragments.AllFragment;
import com.suvidha.suvidharider.Fragments.CompletedFragment;
import com.suvidha.suvidharider.Fragments.Upcomingfragment;
import com.suvidha.suvidharider.HomeActivity;
import com.suvidha.suvidharider.Interface.GetRequestResponse;
import com.suvidha.suvidharider.Interface.RetrofitClient;
import com.suvidha.suvidharider.Interface.RetrofitInterface;
import com.suvidha.suvidharider.Models.BookingModel;
import com.suvidha.suvidharider.Models.BookingResponseModel;
import com.suvidha.suvidharider.Models.ReasonsModel;
import com.suvidha.suvidharider.Models.RegisterResponseModel;
import com.suvidha.suvidharider.Models.SigninResponseModel;
import com.suvidha.suvidharider.Models.TripDetails;
import com.suvidha.suvidharider.PickupArrivingActivity;
import com.suvidha.suvidharider.R;
import com.suvidha.suvidharider.Utilities.Camera_Gallery;
import com.suvidha.suvidharider.Utilities.Constants;
import com.suvidha.suvidharider.Utilities.SharedPreference;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by cybertruimph on 3/19/2018.
 */

public class Booking extends BaseClass implements View.OnClickListener {

    private ViewPager viewPager;
    private TabLayout tabLayout;
    private ProgressDialog progressDialog;
    private ImageButton back;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.booking);

        viewPager = (ViewPager) findViewById(R.id.container);
        tabLayout = (TabLayout) findViewById(R.id.tabs);
        back = (ImageButton) findViewById(R.id.back);
        back.setOnClickListener(this);


    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.back:
                finish();
                break;
        }
    }


    public class SlidePagerAdapter extends FragmentPagerAdapter {
        public SlidePagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {

            if (position == 0) {
                return new AllFragment();
            } else if (position == 1) {
                return new CompletedFragment();
            } else  {
                return new Upcomingfragment();
            }
        }

        @Override
        public int getCount() {
            return 3;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            switch (position) {
                case 0:
                    return "All";
                case 1:
                    return "Completed";
                case 2:
                    return "Upcoming";
            }
            return null;
        }
    }

    @Override
    protected void apiConnected() {
        if (apiConnection != null)
        {
            getBookingDetails();
        }
    }

    private void getBookingDetails() {

        progressDialog = new ProgressDialog(Booking.this);
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDialog.setMessage("Loading...");
        progressDialog.show();
        progressDialog.setCancelable(false);

        BookingModel bookingModel = new BookingModel();
        bookingModel.setUserId(SharedPreference.getString(getBaseContext(), Constants.USER_ID));

        apiConnection.getBookingDetails(bookingModel, new GetRequestResponse() {
            @Override
            public void onResponse(String json) {
                progressDialog.dismiss();
                Gson gson = new Gson();
                TripDetails tripDetails = gson.fromJson(json, TripDetails.class);
                Constants.tripDetails = tripDetails;

                SlidePagerAdapter mSectionsPagerAdapter = new SlidePagerAdapter(getSupportFragmentManager());
                viewPager.setAdapter(mSectionsPagerAdapter);
                tabLayout.setupWithViewPager(viewPager);
            }

            @Override
            public void onFailure(String json) {
                final Gson gson=new Gson();
                final SigninResponseModel signinResponseModel=gson.fromJson(json,SigninResponseModel.class);
                Camera_Gallery.openSnackBar(viewPager, signinResponseModel.getMessage(), getApplicationContext());
                progressDialog.dismiss();
            }
        });
    }

}
