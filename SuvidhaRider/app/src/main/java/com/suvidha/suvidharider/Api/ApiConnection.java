package com.suvidha.suvidharider.Api;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.location.Location;
import android.os.Binder;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.v4.content.LocalBroadcastManager;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.animation.Animation;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.suvidha.suvidharider.Interface.GetRequestResponse;
import com.suvidha.suvidharider.Interface.RetrofitClient;
import com.suvidha.suvidharider.Interface.RetrofitInterface;
import com.suvidha.suvidharider.Models.BookingModel;
import com.suvidha.suvidharider.Models.BookingResponseModel;
import com.suvidha.suvidharider.Models.CarDetailsResponseModel;
import com.suvidha.suvidharider.Models.CarlocationModel;
import com.suvidha.suvidharider.Models.ChangeMobilePassword;
import com.suvidha.suvidharider.Models.ConfirmBookingModel;
import com.suvidha.suvidharider.Models.DistanceModel;
import com.suvidha.suvidharider.Models.DriverModel;
import com.suvidha.suvidharider.Models.Homelocation;
import com.suvidha.suvidharider.Models.LastDriverDetails;
import com.suvidha.suvidharider.Models.NotificationModel;
import com.suvidha.suvidharider.Models.RatingModel;
import com.suvidha.suvidharider.Models.ReasonsModel;
import com.suvidha.suvidharider.Models.RegisterModel;
import com.suvidha.suvidharider.Models.RegisterResponseModel;
import com.suvidha.suvidharider.Models.SigninModel;
import com.suvidha.suvidharider.Models.SigninResponseModel;
import com.suvidha.suvidharider.Models.TripDetails;
import com.suvidha.suvidharider.Models.UpdateLocationModel;
import com.suvidha.suvidharider.Models.UpdateProfileModel;
import com.suvidha.suvidharider.MyApplication;
import com.suvidha.suvidharider.POJO.Example;
import com.suvidha.suvidharider.PickupArrivingActivity;
import com.suvidha.suvidharider.R;
import com.suvidha.suvidharider.RequestRideActivity;
import com.suvidha.suvidharider.Utilities.Camera_Gallery;
import com.suvidha.suvidharider.Utilities.Constants;
import com.suvidha.suvidharider.Utilities.SharedPreference;
import com.suvidha.suvidharider.Utilities.Validation;
import com.suvidha.suvidharider.VerifyMobileActivity;


import java.io.IOException;
import java.util.ArrayList;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.FormBody;
import okhttp3.HttpUrl;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by ingsaurabh on 27/11/17.
 */

public class ApiConnection extends Binder implements SuvidaRiderApi {
    private final String TAG = ApiConnection.class.getSimpleName();
    private final OkHttpClient client;
    private final SharedPreferences prefs;
    private SuvidhaRiderService suvidhaRiderService;
    private Context context;

    public ApiConnection(SuvidhaRiderService suvidhaRiderService) {
        this.suvidhaRiderService = suvidhaRiderService;
        context = suvidhaRiderService.getApplicationContext();
        client = new OkHttpClient();
        prefs = PreferenceManager.getDefaultSharedPreferences(context);
    }

    public SuvidhaRiderService getService() {
        return suvidhaRiderService;
    }


    @Override
    public void changeLocation(String latitude, String longitude, final Context context) {
        FormBody formBody = new FormBody.Builder()
                .add("UserId", MyApplication.getMyApplicationInstance().getMyUserId())
                .add("latitude", String.valueOf(latitude))
                .add("longitude", String.valueOf(longitude))
                .build();
        Request request = new Request.Builder().url(HttpUrl.parse(RetrofitClient.BASE_URL).newBuilder().addPathSegment("get_all_car_location.php").build())
                .post(formBody)
                .build();

        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(@NonNull Call call, @NonNull IOException e) {
                Log.e(TAG + "----changeLocation", "failure");
            }

            @Override
            public void onResponse(@NonNull Call call, @NonNull final Response response) throws IOException {
                JsonObject result = new JsonParser().parse(response.body().string()).getAsJsonObject();
                if (TextUtils.equals(result.get("success").getAsString(), "true")) {
                    Log.d(TAG + "----changeLocation", result.toString());


                }
            }
        });
    }

    @Override
    public void getAllCars(Homelocation homelocation, final GetRequestResponse getRequestResponse) {

        RetrofitInterface apiInterface = RetrofitClient.getClient().create(RetrofitInterface.class);
        retrofit2.Call<CarDetailsResponseModel> call = apiInterface.getcardetails(homelocation);
        call.enqueue(new retrofit2.Callback<CarDetailsResponseModel>() {
            @Override
            public void onResponse(retrofit2.Call<CarDetailsResponseModel> call, retrofit2.Response<CarDetailsResponseModel> response) {
                CarDetailsResponseModel carDetailsResponseModel = response.body();
                Gson gson = new Gson();
                String result = gson.toJson(carDetailsResponseModel);
                if (response.body().getSuccess().equals("true")) {
                    getRequestResponse.onResponse(result);
                } else {
                    getRequestResponse.onFailure("Error");
                }

            }

            @Override
            public void onFailure(retrofit2.Call<CarDetailsResponseModel> call, Throwable t) {
                String ta = t.toString();
            }
        });
    }

    @Override
    public void checkLogin(SigninModel signinModel, final GetRequestResponse getRequestResponse) {
        RetrofitInterface apiInterface = RetrofitClient.getClient().create(RetrofitInterface.class);
        final retrofit2.Call<SigninResponseModel> call = apiInterface.checkLoginCredentials(signinModel);
        call.enqueue(new retrofit2.Callback<SigninResponseModel>() {
            @Override
            public void onResponse(retrofit2.Call<SigninResponseModel> call, retrofit2.Response<SigninResponseModel> response) {
                SigninResponseModel signinResponseModel = response.body();
                Gson gson = new Gson();
                String result = gson.toJson(signinResponseModel);
                if (response.body().getSuccess().equals("true")) {
                    getRequestResponse.onResponse(result);
                } else {
                    getRequestResponse.onFailure(result);
                }
            }

            @Override
            public void onFailure(retrofit2.Call<SigninResponseModel> call, Throwable t) {

            }
        });
    }


    @Override
    public void changepassword(ChangeMobilePassword changeMobilePassword, final GetRequestResponse getRequestResponse) {
        RetrofitInterface apiInterface = RetrofitClient.getClient().create(RetrofitInterface.class);
        final retrofit2.Call<RegisterResponseModel> call = apiInterface.change_password(Constants.changeMobilePassword);
        call.enqueue(new retrofit2.Callback<RegisterResponseModel>() {
            @Override
            public void onResponse(retrofit2.Call<RegisterResponseModel> call, retrofit2.Response<RegisterResponseModel> response) {
                RegisterResponseModel registerResponseModel = response.body();
                Gson gson = new Gson();
                String result = gson.toJson(registerResponseModel);
                if (response.body().getSuccess().equals("true")) {
                    getRequestResponse.onResponse(result);
                } else {
                    getRequestResponse.onFailure(result);
                }
            }

            @Override
            public void onFailure(retrofit2.Call<RegisterResponseModel> call, Throwable t) {

            }
        });
    }

    @Override
    public void changenumber(ChangeMobilePassword changeMobilePassword, final GetRequestResponse getRequestResponse) {
        RetrofitInterface apiInterface = RetrofitClient.getClient().create(RetrofitInterface.class);
        final retrofit2.Call<RegisterResponseModel> call = apiInterface.updatemobile(changeMobilePassword);

        call.enqueue(new retrofit2.Callback<RegisterResponseModel>() {
            @Override
            public void onResponse(retrofit2.Call<RegisterResponseModel> call, retrofit2.Response<RegisterResponseModel> response) {
                RegisterResponseModel registerResponseModel = response.body();
                Gson gson = new Gson();
                String result = gson.toJson(registerResponseModel);
                if (response.body().getSuccess().equals("true")) {

                    getRequestResponse.onResponse(result);
                } else {
                    getRequestResponse.onFailure(result);
                }
            }

            @Override
            public void onFailure(retrofit2.Call<RegisterResponseModel> call, Throwable t) {

            }
        });
    }

    @Override
    public void register(RegisterModel registerModel, final GetRequestResponse getRequestResponse) {
        RetrofitInterface apiInterface = RetrofitClient.getClient().create(RetrofitInterface.class);
        final retrofit2.Call<RegisterResponseModel> call = apiInterface.saveUserData(Constants.registerModel);

        call.enqueue(new retrofit2.Callback<RegisterResponseModel>() {
            @Override
            public void onResponse(retrofit2.Call<RegisterResponseModel> call, retrofit2.Response<RegisterResponseModel> response) {
                RegisterResponseModel registerResponseModel = response.body();
                Gson gson = new Gson();
                String result = gson.toJson(registerResponseModel);
                if (response.body().getSuccess().equals("true")) {
                    getRequestResponse.onResponse(result);
                } else {
                    getRequestResponse.onFailure(result);
                }
            }

            @Override
            public void onFailure(retrofit2.Call<RegisterResponseModel> call, Throwable t) {

            }
        });
    }

    @Override
    public void getotp(RegisterModel registerModel, final GetRequestResponse getRequestResponse) {
        RetrofitInterface apiInterface = RetrofitClient.getClient().create(RetrofitInterface.class);
        final retrofit2.Call<RegisterResponseModel> call = apiInterface.sendOtp(Constants.registerModel);
        call.enqueue(new retrofit2.Callback<RegisterResponseModel>() {
            @Override
            public void onResponse(retrofit2.Call<RegisterResponseModel> call, retrofit2.Response<RegisterResponseModel> response) {
                RegisterResponseModel registerResponseModel = response.body();
                Gson gson = new Gson();
                String result = gson.toJson(registerResponseModel);
                if (response.body().getSuccess().equals("true")) {
                    getRequestResponse.onResponse(result);
                } else {
                    getRequestResponse.onFailure(result);
                }
            }

            @Override
            public void onFailure(retrofit2.Call<RegisterResponseModel> call, Throwable t) {
                String a = t.toString();
            }
        });
    }

    @Override
    public void updateprofile(final UpdateProfileModel updateProfileModel, final GetRequestResponse getRequestResponse) {
        RetrofitInterface apiInterface = RetrofitClient.getClient().create(RetrofitInterface.class);
        final retrofit2.Call<RegisterResponseModel> call = apiInterface.updateProfile(updateProfileModel);
        call.enqueue(new retrofit2.Callback<RegisterResponseModel>() {
            @Override
            public void onResponse(retrofit2.Call<RegisterResponseModel> call, retrofit2.Response<RegisterResponseModel> response) {
                RegisterResponseModel registerResponseModel = response.body();
                Gson gson = new Gson();
                String result = gson.toJson(registerResponseModel);
                if (response.body().getSuccess().equals("true")) {
                    getRequestResponse.onResponse(result);
                } else {
                    getRequestResponse.onFailure(result);
                }
            }

            @Override
            public void onFailure(retrofit2.Call<RegisterResponseModel> call, Throwable t) {
                String a = t.toString();
            }
        });
    }

    @Override
    public void setlocation(UpdateLocationModel updateLocationModel, final GetRequestResponse getRequestResponse) {
        RetrofitInterface apiInterface = RetrofitClient.getClient().create(RetrofitInterface.class);
        final retrofit2.Call<RegisterResponseModel> call = apiInterface.updateMapLocations(updateLocationModel);
        call.enqueue(new retrofit2.Callback<RegisterResponseModel>() {
            @Override
            public void onResponse(retrofit2.Call<RegisterResponseModel> call, retrofit2.Response<RegisterResponseModel> response) {
                RegisterResponseModel registerResponseModel = response.body();
                Gson gson = new Gson();
                String result = gson.toJson(registerResponseModel);
                if (response.body().getSuccess().equals("true")) {
                    getRequestResponse.onResponse(result);
                } else {
                    getRequestResponse.onFailure(result);
                }
            }

            @Override
            public void onFailure(retrofit2.Call<RegisterResponseModel> call, Throwable t) {

            }
        });
    }

    @Override
    public void confirmride(BookingModel bookingModel, final GetRequestResponse getRequestResponse) {
        RetrofitInterface apiInterface = RetrofitClient.getClient().create(RetrofitInterface.class);
        final retrofit2.Call<BookingResponseModel> call = apiInterface.saveBookingData(bookingModel);

        call.enqueue(new retrofit2.Callback<BookingResponseModel>() {

            @Override
            public void onResponse(retrofit2.Call<BookingResponseModel> call, retrofit2.Response<BookingResponseModel> response) {

                BookingResponseModel bookingResponseModel = response.body();
                Gson gson = new Gson();
                String result = gson.toJson(bookingResponseModel);
                if (response.body().getSuccess().equals("true")) {
                    getRequestResponse.onResponse(result);
                } else {
                    getRequestResponse.onFailure(result);
                }

            }

            @Override
            public void onFailure(retrofit2.Call<BookingResponseModel> call, Throwable t) {

            }
        });
    }

    @Override
    public void confirmBooking(ConfirmBookingModel confirmBookingModel, final GetRequestResponse getRequestResponse) {
        RetrofitInterface apiInterface = RetrofitClient.getClient().create(RetrofitInterface.class);
        final retrofit2.Call<BookingResponseModel> call = apiInterface.saveConfirmBookingData(confirmBookingModel);

        call.enqueue(new retrofit2.Callback<BookingResponseModel>() {

            @Override
            public void onResponse(retrofit2.Call<BookingResponseModel> call, retrofit2.Response<BookingResponseModel> response) {

                BookingResponseModel bookingResponseModel = response.body();
                Gson gson = new Gson();
                String result = gson.toJson(bookingResponseModel);
                if (response.body().getSuccess().equals("true")) {
                    getRequestResponse.onResponse(result);
                } else {
                    getRequestResponse.onFailure(result);
                }

            }

            @Override
            public void onFailure(retrofit2.Call<BookingResponseModel> call, Throwable t) {

            }
        });
    }

    @Override
    public void checkDistance(DistanceModel distanceModel, final GetRequestResponse getRequestResponse) {

        String url = "https://maps.googleapis.com/maps/";
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(url)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        RetrofitInterface service = retrofit.create(RetrofitInterface.class);
        retrofit2.Call<Example> call = service.getDistanceDuration("metric", distanceModel.getFromlatitude() + "," + distanceModel.getFromlongitude(), distanceModel.getTolatitude() + "," + distanceModel.getTolongitude(), "driving");
        call.enqueue(new retrofit2.Callback<Example>() {
            public Animation animSildeUp;

            @Override
            public void onResponse(retrofit2.Call<Example> call, retrofit2.Response<Example> response) {
                String distance = null;
                String timeminute = null;
                String time=null;
                String timewithunit=null;
                String timehour = null;
                String distancewithunit=null;
                String distancekm=null;
                String encodedString=null;

                try {
                    // This loop will go through all the results and add marker on each location.
                    for (int i = 0; i < response.body().getRoutes().size(); i++) {
                        distance = response.body().getRoutes().get(i).getLegs().get(i).getDistance().getText();
                        time = response.body().getRoutes().get(i).getLegs().get(i).getDuration().getText();
                        timewithunit=time;
                        distancewithunit=distance;

                        encodedString = response.body().getRoutes().get(0).getOverviewPolyline().getPoints();
                    }
                    String[] times=time.split(" ");
                    int timeDouble=Integer.valueOf(times[0]);
                    if(times[1].equals("mins"))
                    {
                        timeminute=String.valueOf(timeDouble);

                    }
                    else
                    {
                        if(times.length>2)
                        {
                            timeDouble = (timeDouble * 60)+Integer.valueOf(times[3]);
                            timeminute=String.valueOf(timeDouble);

                        }
                        else {
                            timeminute = String.valueOf(timeDouble * 60);
                        }
                    }


                    String[] distanceArray=distance.split(" ");
                    Double distanceDouble=Double.valueOf(distanceArray[0]);
                    if (distanceArray[1].equals("m"))
                    {
                        distance=String.valueOf(distanceDouble);

                        Double meter=Double.valueOf(distance);

                        Double km=meter/1000;

                        distancekm=String.valueOf(km);
                    }
                    else
                    {
                        Double km = Double.valueOf(distanceArray[0]);

                        Double meter = km * 1000;

                        distance=String.valueOf(meter);

                        distancekm=String.valueOf(km);
                    }

                    JsonObject jsonObject=new JsonObject();
                    jsonObject.addProperty("distance",distancekm);
                    jsonObject.addProperty("time",timeminute);
                    jsonObject.addProperty("timewithunit",timewithunit);
                    jsonObject.addProperty("distancewithunit",distancewithunit);
                    jsonObject.addProperty("distancekm",distancekm);
                    jsonObject.addProperty("encodedString",encodedString);
                    String json = jsonObject.toString();
                    getRequestResponse.onResponse(json);

                } catch (Exception e) {
                    Log.d("onResponse", "There is an error");
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(retrofit2.Call<Example> call, Throwable t) {

            }
        });
    }

    @Override
    public void changepaymentMode(DistanceModel distanceModel, final GetRequestResponse getRequestResponse) {

        RetrofitInterface apiInterface = RetrofitClient.getClient().create(RetrofitInterface.class);
        final retrofit2.Call<RegisterResponseModel> call = apiInterface.savePaymentType(distanceModel);

        call.enqueue(new retrofit2.Callback<RegisterResponseModel>() {
            @Override
            public void onResponse(retrofit2.Call<RegisterResponseModel> call, retrofit2.Response<RegisterResponseModel> response) {
                RegisterResponseModel registerResponseModel = response.body();
                Gson gson = new Gson();
                String result = gson.toJson(registerResponseModel);
                if (response.body().getSuccess().equals("true")) {
                    getRequestResponse.onResponse(result);
                } else {
                    getRequestResponse.onFailure(result);
                }
            }

            @Override
            public void onFailure(retrofit2.Call<RegisterResponseModel> call, Throwable t) {

            }
        });

    }

    @Override
    public void getBookingDetails(BookingModel bookingModel, final GetRequestResponse getRequestResponse) {

        RetrofitInterface apiInterface = RetrofitClient.getClient().create(RetrofitInterface.class);
        final retrofit2.Call<TripDetails> call = apiInterface.getBooking(bookingModel);

        call.enqueue(new retrofit2.Callback<TripDetails>() {
            @Override
            public void onResponse(retrofit2.Call<TripDetails> call, retrofit2.Response<TripDetails> response) {
                TripDetails tripDetails = response.body();
                Gson gson = new Gson();
                String result = gson.toJson(tripDetails);
                if (response.body().getSuccess().equals("true")) {
                    getRequestResponse.onResponse(result);
                } else {
                    getRequestResponse.onFailure(result);
                }
            }

            @Override
            public void onFailure(retrofit2.Call<TripDetails> call, Throwable t) {
                String a = t.toString();
            }
        });
    }

    @Override
    public void getDriverLatLang(ReasonsModel reasonsModel, final GetRequestResponse getRequestResponse) {

        RetrofitInterface apiInterface = RetrofitClient.getClient().create(RetrofitInterface.class);
        final retrofit2.Call<DriverModel> call = apiInterface.getDriverPosition(reasonsModel);

        call.enqueue(new retrofit2.Callback<DriverModel>() {
            @Override
            public void onResponse(retrofit2.Call<DriverModel> call, retrofit2.Response<DriverModel> response) {
                DriverModel driverModel = response.body();
                Gson gson = new Gson();
                String result = gson.toJson(driverModel);

                if (response.body().getSuccess().equals("true")) {
                    getRequestResponse.onResponse(result);
                }
                else
                {
                    getRequestResponse.onFailure(result);
                }
            }

            @Override
            public void onFailure(retrofit2.Call<DriverModel> call, Throwable t) {
                String a = t.toString();
            }
        });
    }

    @Override
    public void saveRating(RatingModel ratingModel, final GetRequestResponse getRequestResponse) {

        RetrofitInterface apiInterface = RetrofitClient.getClient().create(RetrofitInterface.class);
        final retrofit2.Call<DriverModel> call = apiInterface.saveRatingDetails(ratingModel);

        call.enqueue(new retrofit2.Callback<DriverModel>() {
            @Override
            public void onResponse(retrofit2.Call<DriverModel> call, retrofit2.Response<DriverModel> response) {
                DriverModel driverModel = response.body();

                Gson gson = new Gson();
                String result = gson.toJson(driverModel);

                if (response.body().getSuccess().equals("true")) {
                    getRequestResponse.onResponse(result);
                }
                else
                {
                    getRequestResponse.onFailure(result);
                }
            }

            @Override
            public void onFailure(retrofit2.Call<DriverModel> call, Throwable t) {
                String a = t.toString();
            }
        });

    }

    @Override
    public void checkLastDriver(RatingModel ratingModel, final GetRequestResponse getRequestResponse) {

        RetrofitInterface apiInterface = RetrofitClient.getClient().create(RetrofitInterface.class);
        final retrofit2.Call<LastDriverDetails> call = apiInterface.checkRatingDetails(ratingModel);

        call.enqueue(new retrofit2.Callback<LastDriverDetails>() {
            @Override
            public void onResponse(retrofit2.Call<LastDriverDetails> call, retrofit2.Response<LastDriverDetails> response) {
                LastDriverDetails lastDriverDetails = response.body();

                Gson gson = new Gson();
                String result = gson.toJson(lastDriverDetails);

                if (response.body().getSuccess().equals("true")) {
                    getRequestResponse.onResponse(result);
                }
                else
                {
                    getRequestResponse.onFailure(result);
                }
            }

            @Override
            public void onFailure(retrofit2.Call<LastDriverDetails> call, Throwable t) {
                String a = t.toString();
            }
        });
    }

    @Override
    public void getInvoiceDetails(RatingModel ratingModel, final GetRequestResponse getRequestResponse) {

        RetrofitInterface apiInterface = RetrofitClient.getClient().create(RetrofitInterface.class);
        final retrofit2.Call<TripDetails> call = apiInterface.getInvoice(ratingModel);

        call.enqueue(new retrofit2.Callback<TripDetails>() {
            @Override
            public void onResponse(retrofit2.Call<TripDetails> call, retrofit2.Response<TripDetails> response) {
                TripDetails tripDetails = response.body();
                Gson gson = new Gson();
                String result = gson.toJson(tripDetails);
                if (response.body().getSuccess().equals("true")) {
                    getRequestResponse.onResponse(result);
                } else {
                    getRequestResponse.onFailure(result);
                }
            }

            @Override
            public void onFailure(retrofit2.Call<TripDetails> call, Throwable t) {
                String a = t.toString();
            }
        });
    }

    @Override
    public void getNotification(RatingModel ratingModel, final GetRequestResponse getRequestResponse) {

        RetrofitInterface apiInterface = RetrofitClient.getClient().create(RetrofitInterface.class);
        final retrofit2.Call<NotificationModel> call = apiInterface.getNotifications(ratingModel);

        call.enqueue(new retrofit2.Callback<NotificationModel>() {
            @Override
            public void onResponse(retrofit2.Call<NotificationModel> call, retrofit2.Response<NotificationModel> response) {
                NotificationModel notificationModel = response.body();
                Gson gson = new Gson();
                String result = gson.toJson(notificationModel);
                if (response.body().getSuccess().equals("true")) {
                    getRequestResponse.onResponse(result);
                } else {
                    getRequestResponse.onFailure(result);
                }
            }

            @Override
            public void onFailure(retrofit2.Call<NotificationModel> call, Throwable t) {
                String a = t.toString();
            }
        });

    }


}