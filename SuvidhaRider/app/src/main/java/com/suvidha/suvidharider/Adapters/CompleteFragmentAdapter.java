package com.suvidha.suvidharider.Adapters;

import android.content.Intent;
import android.location.Address;
import android.location.Geocoder;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.makeramen.roundedimageview.RoundedImageView;
import com.suvidha.suvidharider.InvoiceActivity;
import com.suvidha.suvidharider.Models.CompleteRideDetail;
import com.suvidha.suvidharider.Models.InvoiceModel;
import com.suvidha.suvidharider.R;
import com.suvidha.suvidharider.Utilities.Camera_Gallery;
import com.suvidha.suvidharider.Utilities.Constants;

import java.io.IOException;
import java.util.List;
import java.util.Locale;

/**
 * Created by cybertruimph on 3/28/2018.
 */

public class CompleteFragmentAdapter extends RecyclerView.Adapter<CompleteFragmentAdapter.MyCredits> {

    private List<CompleteRideDetail> pList;
    private View itemView;

    public CompleteFragmentAdapter(List<CompleteRideDetail> pList) {
        this.pList = pList;
    }

    public class MyCredits extends RecyclerView.ViewHolder {

        private final TextView tvName, tvCarName, tvLocation, tvResult, tvTime, tvAmount, tvPaymentType;
        private final ImageView imMap;
        private final Button btnInvoice;
        private final RoundedImageView imProfile;

        public MyCredits(View itemView) {
            super(itemView);
            tvName = (TextView) itemView.findViewById(R.id.tvName);
            tvCarName = (TextView) itemView.findViewById(R.id.tvCarName);
            tvLocation = (TextView) itemView.findViewById(R.id.tvLocation);
            tvResult = (TextView) itemView.findViewById(R.id.tvResult);
            tvTime = (TextView) itemView.findViewById(R.id.tvTime);
            tvAmount = (TextView) itemView.findViewById(R.id.tvAmount);
            tvPaymentType = (TextView) itemView.findViewById(R.id.tvPaymentType);
            imMap = (ImageView) itemView.findViewById(R.id.imMap);
            imProfile = (RoundedImageView) itemView.findViewById(R.id.imProfile);
            btnInvoice = (Button) itemView.findViewById(R.id.btnInvoice);
        }

    }

    @Override
    public CompleteFragmentAdapter.MyCredits onCreateViewHolder(ViewGroup parent, int viewType) {
        itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.custom_recycler_all_booking, parent, false);
        return new CompleteFragmentAdapter.MyCredits(itemView);
    }

    @Override
    public void onBindViewHolder(CompleteFragmentAdapter.MyCredits holder, final int position) {
        holder.tvName.setText(pList.get(position).getFirstname()+" "+pList.get(position).getLastname());
        holder.tvCarName.setText(pList.get(position).getVehicleType());
        holder.tvLocation.setText(Camera_Gallery.getAddressFromLocation(itemView.getContext(), Double.valueOf(pList.get(position).getToLatitude()), Double.valueOf(pList.get(position).getToLongitude())));
        holder.tvResult.setText(pList.get(position).getStatus());
        holder.tvAmount.setText(itemView.getResources().getString(R.string.Rs)+" "+pList.get(position).getTotalAmount());
        holder.tvTime.setText(Camera_Gallery.getFormattedDate(itemView.getContext(), pList.get(position).getDate()));

        Glide.with(itemView.getContext())
                .load(pList.get(position).getProfile())
                .into(holder.imProfile);

        Glide.with(itemView.getContext())
                .load(pList.get(position).getMapurl())
                .into(holder.imMap);

        holder.btnInvoice.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Constants.invoiceModel.setTotalFare(pList.get(position).getTotalAmount());
                Constants.invoiceModel.setTotalDistance(pList.get(position).getDistance());
                Constants.invoiceModel.setFromLatitude(pList.get(position).getFromLatitude());
                Constants.invoiceModel.setFromLongitude(pList.get(position).getFromLongitude());
                Constants.invoiceModel.setToLatitude(pList.get(position).getToLatitude());
                Constants.invoiceModel.setToLongitude(pList.get(position).getToLongitude());
                Constants.invoiceModel.setTripFare(pList.get(position).getFareAmount());
                Constants.invoiceModel.setTolls(pList.get(position).getTollAmount());
                Constants.invoiceModel.setRiderDiscounts(pList.get(position).getTotalAmount());
                Constants.invoiceModel.setOutstanding(pList.get(position).getTotalAmount());
                Constants.invoiceModel.setTotal(pList.get(position).getTotalAmount());
                Constants.invoiceModel.setParking(pList.get(position).getParkingAmount());
                Constants.invoiceModel.setDue(pList.get(position).getDueAmount());

                itemView.getContext().startActivity(new Intent(itemView.getContext(), InvoiceActivity.class));
            }
        });
    }

    @Override
    public int getItemCount() {
        return pList.size();
    }
}