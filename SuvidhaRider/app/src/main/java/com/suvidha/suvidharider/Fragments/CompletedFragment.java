package com.suvidha.suvidharider.Fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.suvidha.suvidharider.Adapters.AllFragmentAdapter;
import com.suvidha.suvidharider.Adapters.CompleteFragmentAdapter;
import com.suvidha.suvidharider.Models.CompleteRideDetail;
import com.suvidha.suvidharider.Models.MyBookingModel;
import com.suvidha.suvidharider.R;
import com.suvidha.suvidharider.Utilities.Constants;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by cybertruimph on 3/19/2018.
 */

public class CompletedFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private RecyclerView recycle_all_booking;

    private List<CompleteRideDetail> completeRideDetailArrayList = new ArrayList<>();
    private View view;

    public CompletedFragment() {
        // Required empty public constructor
    }

    // TODO: Rename and change types and number of parameters


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        view = inflater.inflate(R.layout.fragment_completed, container, false);

        completeRideDetailArrayList = Constants.tripDetails.getCompleteRideDetails();

        recycle_all_booking = (RecyclerView) view.findViewById(R.id.recycle_completed);

        if (completeRideDetailArrayList != null)
        {
            CompleteFragmentAdapter completeFragmentAdapter = new CompleteFragmentAdapter(completeRideDetailArrayList);
            RecyclerView.LayoutManager lm1 = new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false);
            recycle_all_booking.setLayoutManager(lm1);
            recycle_all_booking.setAdapter(completeFragmentAdapter);
        }

        return view;

    }
}
