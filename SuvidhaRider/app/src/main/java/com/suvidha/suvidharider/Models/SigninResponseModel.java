package com.suvidha.suvidharider.Models;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class SigninResponseModel {

    @SerializedName("success")
    @Expose
    private String success;
    @SerializedName("UserDetails")
    @Expose
    private List<UserDetailsModel> userDetails = null;
    @SerializedName("Homelocation")
    @Expose
    private List<Homelocation> homelocation = null;
    @SerializedName("Worklocation")
    @Expose
    private List<Worklocation> worklocation = null;
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("message")
    @Expose
    private String message;

    public String getSuccess() {
        return success;
    }

    public void setSuccess(String success) {
        this.success = success;
    }

    public List<UserDetailsModel> getUserDetails() {
        return userDetails;
    }

    public void setUserDetails(List<UserDetailsModel> userDetails) {
        this.userDetails = userDetails;
    }

    public List<Homelocation> getHomelocation() {
        return homelocation;
    }

    public void setHomelocation(List<Homelocation> homelocation) {
        this.homelocation = homelocation;
    }

    public List<Worklocation> getWorklocation() {
        return worklocation;
    }

    public void setWorklocation(List<Worklocation> worklocation) {
        this.worklocation = worklocation;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

}
