package com.suvidha.suvidharider;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.suvidha.suvidharider.Utilities.Constants;

public class FareEstimateActivity extends AppCompatActivity implements View.OnClickListener {

    ImageButton backImageButton;
    private Button btnDone;
    private TextView tvFromname, tvToName, tvApproxTime, tvFareAmt, tvFare, tvDue;
    private LinearLayout llGross;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fair_estimate);

        initialize();

        backImageButton.setOnClickListener(this);
        btnDone.setOnClickListener(this);
        tvApproxTime.setText("Approx. Travel Time :"+getIntent().getStringExtra(Constants.APPROX_TIME));
        tvFromname.setText(getIntent().getStringExtra(Constants.FROM_LOCATION));
        tvToName.setText(getIntent().getStringExtra(Constants.TO_LOCATION));

        String fare = getIntent().getStringExtra(Constants.FARE_AMT);
        String due = getIntent().getStringExtra(Constants.DUE_AMT);

        if (!due.equals("0"))
        {
            llGross.setVisibility(View.VISIBLE);
            tvFare.setText(fare);
            tvDue.setText(due);

            double totalamount = Double.valueOf(fare)+Double.valueOf(due);

            double maxfare = Double.valueOf(fare) +Double.valueOf(due)+ 10;
            tvFareAmt.setText(getResources().getString(R.string.Rs)+" "+Math.round(totalamount)+" - "+getResources().getString(R.string.Rs)+" "+String.valueOf(Math.round(maxfare)));
        }
        else
        {
            llGross.setVisibility(View.GONE);
            double maxfare = Double.valueOf(fare) + 10;
            tvFareAmt.setText(getResources().getString(R.string.Rs)+" "+fare+" - "+getResources().getString(R.string.Rs)+" "+String.valueOf(Math.round(maxfare)));
        }


    }

    private void initialize() {
        btnDone = (Button) findViewById(R.id.btnDone);
        backImageButton=(ImageButton)findViewById(R.id.back);
        tvFromname = (TextView) findViewById(R.id.tvFromName);
        tvToName = (TextView) findViewById(R.id.tvToName);
        tvApproxTime = (TextView) findViewById(R.id.tvApproxTime);
        tvFareAmt = (TextView) findViewById(R.id.tvFareAmt);
        tvFare = (TextView) findViewById(R.id.tvFare);
        tvDue = (TextView) findViewById(R.id.tvDue);
        llGross = (LinearLayout) findViewById(R.id.llGross);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId())
        {
            case R.id.btnDone:

                Intent i = new Intent(FareEstimateActivity.this, RequestRideActivity.class);
                i.putExtra(Constants.FROM_LOCATION, tvFromname.getText().toString());
                i.putExtra(Constants.TO_LOCATION, tvToName.getText().toString());
                startActivity(i);

                break;

            case R.id.back:
                finish();
                break;
        }
    }
}
