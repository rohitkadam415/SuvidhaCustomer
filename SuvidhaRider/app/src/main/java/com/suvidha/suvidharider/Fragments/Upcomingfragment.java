package com.suvidha.suvidharider.Fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.suvidha.suvidharider.Adapters.AllFragmentAdapter;
import com.suvidha.suvidharider.Adapters.UpcomingFragmentAdapter;
import com.suvidha.suvidharider.Models.MyBookingModel;
import com.suvidha.suvidharider.R;

import java.util.ArrayList;

/**
 * Created by cybertruimph on 3/19/2018.
 */

public class Upcomingfragment extends Fragment {

    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private RecyclerView recycle_all_booking;

    private ArrayList<MyBookingModel> myBookingModelArrayList = new ArrayList<>();
    private View view;

    public Upcomingfragment() {
        // Required empty public constructor
    }

    // TODO: Rename and change types and number of parameters


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        view = inflater.inflate(R.layout.fragment_upcoming, container, false);

        MyBookingModel myBookingModel = new MyBookingModel("Amol Magdum", "BMW", "8753 Maricio Walks", "Cancel");
        myBookingModelArrayList.add(myBookingModel);
        myBookingModel = new MyBookingModel("Sameer Thakar", "Audi", "8753 Maricio Walks", "Success");
        myBookingModelArrayList.add(myBookingModel);

        recycle_all_booking = (RecyclerView) view.findViewById(R.id.recycle_upcoming);

        UpcomingFragmentAdapter upcomingFragmentAdapter = new UpcomingFragmentAdapter(myBookingModelArrayList);
        RecyclerView.LayoutManager lm1 = new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false);
        recycle_all_booking.setLayoutManager(lm1);
        recycle_all_booking.setAdapter(upcomingFragmentAdapter);

        return view;

    }
}
