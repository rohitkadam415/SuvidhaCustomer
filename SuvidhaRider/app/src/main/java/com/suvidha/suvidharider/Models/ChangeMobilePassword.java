package com.suvidha.suvidharider.Models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by cybertruimph on 3/5/2018.
 */

public class ChangeMobilePassword {

    @SerializedName("UserId")
    @Expose
    private String UserId;

    @SerializedName("Mobile")
    @Expose
    private String mobile;

    @SerializedName("CountryCode")
    @Expose
    private String countryCode;

    @SerializedName("OldPassword")
    @Expose
    private String oldpassword;

    @SerializedName("NewPassword")
    @Expose
    private String newpassword;


    public String getUserId() {
        return UserId;
    }

    public void setUserId(String userId) {
        UserId = userId;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getCountryCode() {
        return countryCode;
    }

    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
    }

    public String getOldpassword() {
        return oldpassword;
    }

    public void setOldpassword(String oldpassword) {
        this.oldpassword = oldpassword;
    }

    public String getNewpassword() {
        return newpassword;
    }

    public void setNewpassword(String newpassword) {
        this.newpassword = newpassword;
    }
}
