package com.suvidha.suvidharider;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ListView;

import com.rengwuxian.materialedittext.MaterialEditText;
import com.suvidha.suvidharider.Models.ChangeMobilePassword;
import com.suvidha.suvidharider.Utilities.Camera_Gallery;
import com.suvidha.suvidharider.Utilities.Constants;
import com.suvidha.suvidharider.Utilities.InternetCheck;
import com.suvidha.suvidharider.Utilities.SharedPreference;
import com.suvidha.suvidharider.Utilities.Validation;

public class UpdateMobileNumberActivity extends AppCompatActivity implements View.OnClickListener {

    private MaterialEditText input_mobile, input_country_code;
    private Button btnSubmit;
    private ImageButton back;
    private ListView lv;
    ArrayAdapter<String> adapter;
    EditText inputSearch;
    private String[] countrynames;
    private String[] countycodes;
    private String[] blank;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_update_mobile_number);

        initialize();

        input_country_code.setOnClickListener(this);
        btnSubmit.setOnClickListener(this);
        back.setOnClickListener(this);

        countrynames = new String[]{"Afghanistan", "Albania", "Algeria", "American Samoa", "Andorra", "Antarctica", "Argentina", "Australia", "Bangladesh", "Bermuda", "Brazil", "Belgium", "Canada", "China", "Colombia", "Costa Rica", "Egypt", "Finland", "France", "Germany", "Greece", "Greenland", "Hong Kong", "Iceland", "India", "Indonesia", "Iran", "Ireland", "Italy", "Japan", "Kenya", "Korea (North)", "Korea (South)", "Kuwait", "Malaysia", "Maldives", "Mauritius", "Mexico", "Myanmar", "Nepal", "Netherlands", "New Zealand", "Nigeria", "Pakistan", "Philippines", "Poland", "Portugal", "Russia", "Singapore", "South Africa", "Spain", "Sri Lanka", "Sweden", "Taiwan", "Thailand", "United Kingdom", "USA", "Zimbabwe"};
        countycodes = new String[]{"+93", "+355", "+213", "+1-684", "+376", "+672", "+54", "+61", "+880", "+1-441", "+55", "+32", "+1", "+86", "+57", "+506", "+20", "+358", "+33", "+49", "+30", "+299", "+852", "+354", "+91", "+62", "+98", "+353", "+39", "+81", "+254", "+850", "+82", "+965", "+60", "+960", "+230", "+52", "+95", "+977", "+31", "+64", "+234", "+92", "+63", "+48", "+351", "+7", "+65", "+27", "+34", "+94", "+46", "+886", "+66", "+44", "+1", "+263"};

        final String products[] = getResources().getStringArray(R.array.DialingCountryCode);
        blank = countrynames;
    }

    private void initialize() {

        input_mobile = (MaterialEditText) findViewById(R.id.input_mobile);
        input_country_code = (MaterialEditText) findViewById(R.id.input_country_code);
        btnSubmit = (Button) findViewById(R.id.btnSubmit);
        back = (ImageButton) findViewById(R.id.back);
    }

    @Override
    public void onClick(View view) {

        switch (view.getId()) {
            case R.id.back:
                finish();
                break;

            case R.id.btnSubmit:

                if (InternetCheck.getConnectivityStatus(getApplicationContext()))
                {
                    if (validation()) {
                        String mobile = input_mobile.getText().toString();
                        String cc = input_country_code.getText().toString();
                        String user_id = SharedPreference.getString(getApplicationContext(), Constants.USER_ID);

                        Constants.changeMobilePassword.setUserId(user_id);
                        Constants.changeMobilePassword.setCountryCode(cc);
                        Constants.changeMobilePassword.setMobile(mobile);

                        Constants.registerModel.setMobile(mobile);
                        Constants.registerModel.setCountryCode(cc);
                        startActivity(new Intent(UpdateMobileNumberActivity.this, VerifyMobileActivity.class).putExtra( Constants.UPDATEMOBILE, "updatemobile"));
                    }

                }
                else {
                    Camera_Gallery.openSnackBar(btnSubmit, getResources().getString(R.string.nointernet), getApplicationContext());
                }
                break;

            case R.id.input_country_code:
                final Dialog dialog = new Dialog(UpdateMobileNumberActivity.this);
                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                dialog.setContentView(R.layout.list_countries);

                lv = (ListView) dialog.findViewById(R.id.list_view);
                inputSearch = (EditText) dialog.findViewById(R.id.inputSearch);
                adapter = new ArrayAdapter<>(getBaseContext(), R.layout.list_item, R.id.product_name, countrynames);
                lv.setAdapter(adapter);

                inputSearch.addTextChangedListener(new TextWatcher() {
                    @Override
                    public void onTextChanged(CharSequence cs, int arg1, int arg2, int arg3) {
                        // When user changed the Text
                        adapter.getFilter().filter(cs);

                    }

                    @Override
                    public void afterTextChanged(Editable editable) {

                    }

                    @Override
                    public void beforeTextChanged(CharSequence arg0, int arg1, int arg2,
                                                  int arg3) {
                        // TODO Auto-generated method stub
                    }
                });

                lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                        Log.e("item", parent.getItemAtPosition(position).toString());
                        dialog.dismiss();
                        Log.e("item1", parent.getItemAtPosition(position) + "");

                        String carName = parent.getItemAtPosition(position).toString();
                        int index = -1;
                        for (int i = 0; i < blank.length; i++) {
                            if (blank[i].equals(carName)) {
                                index = i;
                                break;
                            }
                        }
                        String cs = countycodes[index];
                        input_country_code.setText(cs);
                        // tvCountrySelect.setText(parent.getItemAtPosition(position).toString());
                    }
                });

                dialog.show();

                break;
        }
    }

    public boolean validation() {
        if (Validation.isValidContact(input_mobile, "Please enter mobile")) {
            return true;
        }
        return false;
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (Constants.finishhFlag)
        {
            Constants.finishhFlag = false;
            finish();
        }
    }
}
