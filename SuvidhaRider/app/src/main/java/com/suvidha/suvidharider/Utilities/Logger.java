package com.suvidha.suvidharider.Utilities;


import android.util.Log;

public class Logger {
    static final String BASETAG = "-----Logger----------";
    public static boolean DEBUG_MODE = true;

    public static void v(String TAG, String msg) {
        if (DEBUG_MODE && msg != null)
            Log.v(BASETAG + TAG, msg);
    }

    public static void e(String TAG, String msg) {
        if (DEBUG_MODE && msg != null)
            Log.e(BASETAG + TAG, msg);
    }

    public static void d(String TAG, String msg) {
        if (DEBUG_MODE && msg != null)
            Log.d(BASETAG + TAG, msg);
    }

    public static void i(String TAG, String msg) {
        if (DEBUG_MODE && msg != null)
            Log.i(BASETAG + TAG, msg);
    }

    public static void w(String TAG, String msg) {
        if (DEBUG_MODE && msg != null)
            Log.w(BASETAG + TAG, msg);

    }

    public static void e(String TAG, String msg, boolean hasLOG) {
        if (DEBUG_MODE && msg != null)
            Log.e(BASETAG + TAG, msg);

    }

    public static void d(String TAG, String msg, boolean hasLOG) {
        if (DEBUG_MODE && msg != null)
            Log.d(BASETAG + TAG, msg);

    }

    public static void i(String TAG, String msg, boolean hasLOG) {
        if (DEBUG_MODE && msg != null)
            Log.i(BASETAG + TAG, msg);

    }

    public static void w(String TAG, String msg, boolean hasLOG) {
        if (DEBUG_MODE && msg != null)
            Logger.w(BASETAG + TAG, msg);

    }
}