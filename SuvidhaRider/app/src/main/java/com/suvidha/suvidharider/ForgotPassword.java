package com.suvidha.suvidharider;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;


import com.rengwuxian.materialedittext.MaterialEditText;
import com.suvidha.suvidharider.Interface.RetrofitClient;
import com.suvidha.suvidharider.Interface.RetrofitInterface;
import com.suvidha.suvidharider.Models.RegisterResponseModel;
import com.suvidha.suvidharider.Utilities.Camera_Gallery;
import com.suvidha.suvidharider.Utilities.InternetCheck;
import com.suvidha.suvidharider.Utilities.Validation;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ForgotPassword extends AppCompatActivity implements View.OnClickListener {

    MaterialEditText edtForgotEmail;
    private Button btnSubmit;
    private ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forgot_password);

        btnSubmit = (Button) findViewById(R.id.btnSubmit);
        edtForgotEmail = (MaterialEditText) findViewById(R.id.edtForgotEmail);
        ImageButton back = (ImageButton) findViewById(R.id.back);

        back.setOnClickListener(this);
        btnSubmit.setOnClickListener(this);

    }

    @Override
    public void onClick(View view) {
        switch (view.getId())
        {
            case R.id.back:
                finish();
                break;

            case R.id.btnSubmit:
                // startActivity(new Intent(ForgotPassword.this, HomeActivity.class));
                if (InternetCheck.getConnectivityStatus(getApplicationContext())) {
                    if (Validation.isValidateEmailAddress(edtForgotEmail, "Please enter valid address")) {
                        forgetMail();
                    }
                }
                else
                {
                    Camera_Gallery.openSnackBar(btnSubmit, getResources().getString(R.string.nointernet), getApplicationContext());
                }
                break;
        }
    }

    private void forgetMail() {

        progressDialog = new ProgressDialog(ForgotPassword.this);
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDialog.setMessage("Please wait...");
        progressDialog.show();

        RetrofitInterface apiInterface = RetrofitClient.getClient().create(RetrofitInterface.class);
        final Call<RegisterResponseModel> call = apiInterface.forgetpassword(edtForgotEmail.getText().toString());
        call.enqueue(new Callback<RegisterResponseModel>() {
            @Override
            public void onResponse(Call<RegisterResponseModel> call, Response<RegisterResponseModel> response) {

                progressDialog.dismiss();

                if (response.body().getSuccess().equals("true")) {
                    Snackbar snak = Snackbar.make(btnSubmit, response.body().getMessage(), Snackbar.LENGTH_LONG);
                    View snackBarView = snak.getView();
                    TextView textView = (TextView) snackBarView.findViewById(android.support.design.R.id.snackbar_text);
                    textView.setTextSize(14.0f);
                    snackBarView.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
                    snak.show();
                    edtForgotEmail.setText("");
                } else {
                    Snackbar snak = Snackbar.make(btnSubmit, response.body().getMessage(), Snackbar.LENGTH_LONG);
                    View snackBarView = snak.getView();
                    TextView textView = (TextView) snackBarView.findViewById(android.support.design.R.id.snackbar_text);
                    textView.setTextSize(14.0f);
                    snackBarView.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
                    snak.show();
                }
            }

            @Override
            public void onFailure(Call<RegisterResponseModel> call, Throwable t) {

            }
        });

    }
}
