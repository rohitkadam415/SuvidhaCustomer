package com.suvidha.suvidharider;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.location.Address;
import android.location.Geocoder;
import android.support.v4.app.FragmentActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlaceAutocomplete;
import com.google.android.gms.location.places.ui.PlaceAutocompleteFragment;
import com.google.android.gms.location.places.ui.PlacePicker;
import com.google.android.gms.location.places.ui.PlaceSelectionListener;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.suvidha.suvidharider.Api.BaseClass;
import com.suvidha.suvidharider.Interface.GetRequestResponse;
import com.suvidha.suvidharider.Interface.RetrofitClient;
import com.suvidha.suvidharider.Interface.RetrofitInterface;
import com.suvidha.suvidharider.Models.RegisterResponseModel;
import com.suvidha.suvidharider.Models.UpdateLocationModel;
import com.suvidha.suvidharider.Utilities.Camera_Gallery;
import com.suvidha.suvidharider.Utilities.Constants;
import com.suvidha.suvidharider.Utilities.InternetCheck;
import com.suvidha.suvidharider.Utilities.SharedPreference;

import org.w3c.dom.Text;

import java.io.IOException;
import java.util.List;
import java.util.Locale;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MapsActivity extends BaseClass implements OnMapReadyCallback, GoogleMap.OnMarkerDragListener, View.OnClickListener {

    private static final String TAG = "map";
    private static final int PLACE_AUTOCOMPLETE_REQUEST_CODE = 1;
    private GoogleMap mMap;
    private TextView tvLocation;
    private ImageButton back;
    private LinearLayout llPick;
    private GoogleMap.OnCameraIdleListener onCameraIdleListener;
    LinearLayout locationdetailsLinearLayout;
    Button picLocationButton;
    TextView locationTextView, tvPlace;
    String locationString, placeName;
    String latitudeString = null;
    String longitudeString;
    private UpdateLocationModel updateLocationModel;
    private ProgressDialog progressDialog;
    private static LatLngBounds BOUNDS_MOUNTAIN_VIEW =null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        initialize();

        back.setOnClickListener(this);
        llPick.setOnClickListener(this);
        picLocationButton.setOnClickListener(this);

        LatLng currentLocation = new LatLng(Double.valueOf(SharedPreference.getString(getBaseContext(), Constants.LATITUDE)), Double.valueOf(SharedPreference.getString(getBaseContext(), Constants.LONGITUDE)));
        BOUNDS_MOUNTAIN_VIEW = new LatLngBounds(currentLocation, currentLocation);


        if(latitudeString == null)
        {
            picLocationButton.setVisibility(View.GONE);
        }

    }


    private void initialize() {

        back = (ImageButton) findViewById(R.id.back);
        tvLocation = (TextView) findViewById(R.id.tvLocation);

        llPick = (LinearLayout) findViewById(R.id.llPick);

        locationdetailsLinearLayout = (LinearLayout) findViewById(R.id.locationdetails);
        picLocationButton = (Button) findViewById(R.id.btnPick);
        locationTextView = (TextView) findViewById(R.id.tvPickLocation);
        tvPlace = (TextView) findViewById(R.id.tvPlace);


    }

    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        mMap.setOnMarkerDragListener(this);

        LatLng myloc;
        String title;

        if (getIntent().getStringExtra(Constants.LOCATIONTYPE).equals(Constants.HOMELOCATION) && SharedPreference.getString(getBaseContext(), Constants.HOMELOCATION) != null) {
            title = Constants.HOMELOCATION;
            locationdetailsLinearLayout.setVisibility(View.VISIBLE);
            myloc = new LatLng(Double.valueOf(SharedPreference.getString(getBaseContext(), Constants.HOMELATITUDE)), Double.valueOf(SharedPreference.getString(getBaseContext(), Constants.HOMELONGITUDE)));

            mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(myloc, 16.0f));
            tvPlace.setText(SharedPreference.getString(getApplicationContext(), Constants.HOMEPLACENAME));
            locationTextView.setText(SharedPreference.getString(getApplicationContext(), Constants.HOMELOCATION));
        } else if (getIntent().getStringExtra(Constants.LOCATIONTYPE).equals(Constants.WORKLOCATION) && SharedPreference.getString(getBaseContext(), Constants.WORKLOCATION) != null) {
            locationdetailsLinearLayout.setVisibility(View.VISIBLE);
            myloc = new LatLng(Double.valueOf(SharedPreference.getString(getBaseContext(), Constants.WORKLATITUDE)), Double.valueOf(SharedPreference.getString(getBaseContext(), Constants.WORKLONGITUDE)));
            title = Constants.WORKLOCATION;
            mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(myloc, 16.0f));
            tvPlace.setText(SharedPreference.getString(getApplicationContext(), Constants.WORKPLACENAME));
            locationTextView.setText(SharedPreference.getString(getApplicationContext(), Constants.WORKLOCATION));
        } else {
            myloc = new LatLng(Double.valueOf(SharedPreference.getString(getBaseContext(), Constants.LATITUDE)), Double.valueOf(SharedPreference.getString(getBaseContext(), Constants.LONGITUDE)));
            mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(myloc, 16.0f));
            title = "Current Location";
        }

        mMap.addMarker(new MarkerOptions().position(myloc).icon(BitmapDescriptorFactory.fromResource(R.drawable.markernew)).title(title));
        mMap.moveCamera(CameraUpdateFactory.newLatLng(myloc));
    }

    @Override
    public void onMarkerDragStart(Marker marker) {
//        tvLocation.setText("Marker " + marker.getId() + " DragStart");
    }

    @Override
    public void onMarkerDrag(Marker marker) {
//        tvLocation.setText("Marker " + marker.getId() + " Drag@" + marker.getPosition());
    }

    @Override
    public void onMarkerDragEnd(Marker marker) {
        String selectedPlace = Camera_Gallery.getAddressFromLocation(getApplicationContext(), marker.getPosition().latitude, marker.getPosition().longitude);
        tvLocation.setText(selectedPlace);
        locationTextView.setText(selectedPlace);
        tvPlace.setText(getFeatureAddress(marker.getPosition().latitude, marker.getPosition().longitude));

        locationString = selectedPlace;
        latitudeString = String.valueOf(marker.getPosition().latitude);
        longitudeString = String.valueOf(marker.getPosition().longitude);
        placeName = getFeatureAddress(marker.getPosition().latitude, marker.getPosition().longitude);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == PLACE_AUTOCOMPLETE_REQUEST_CODE) {
            if (resultCode == RESULT_OK) {
                Place place = PlaceAutocomplete.getPlace(this, data);
//                Place place = PlacePicker.getPlace(data, this);
                Log.i(TAG, "Place: " + place.getName());

                mMap.clear();
                mMap.addMarker(new MarkerOptions()
                        .position(place.getLatLng())
                        .icon(BitmapDescriptorFactory.fromResource(R.drawable.markernew))
                        .draggable(true));
                mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(place.getLatLng(), 16.0f));

                picLocationButton.setVisibility(View.VISIBLE);

                locationString = place.getAddress().toString();
                latitudeString = String.valueOf(place.getLatLng().latitude);
                longitudeString = String.valueOf(place.getLatLng().longitude);
                placeName = place.getName().toString();
                locationdetailsLinearLayout.setVisibility(View.VISIBLE);
                locationTextView.setText(place.getAddress().toString());
                tvPlace.setText(place.getName().toString());
                tvLocation.setText(place.getAddress().toString());


            } else if (resultCode == PlaceAutocomplete.RESULT_ERROR) {
                Status status = PlaceAutocomplete.getStatus(this, data);
                // TODO: Handle the error.
                Log.i(TAG, status.getStatusMessage());

            } else if (resultCode == RESULT_CANCELED) {
                // The user canceled the operation.
            }
        }

    }

    @Override
    public void onClick(View view) {

        switch (view.getId()) {
            case R.id.back:
                // updateLocations();
                finish();
                break;

            case R.id.llPick:
                try {
                    Intent intent = new PlaceAutocomplete.IntentBuilder(PlaceAutocomplete.MODE_FULLSCREEN)
                            .setBoundsBias(BOUNDS_MOUNTAIN_VIEW)
                                    .build(this);
                    startActivityForResult(intent, PLACE_AUTOCOMPLETE_REQUEST_CODE);
                } catch (GooglePlayServicesRepairableException e) {
                    // TODO: Handle the error.
                } catch (GooglePlayServicesNotAvailableException e) {
                    // TODO: Handle the error.
                }
                break;

            case R.id.btnPick:

                if (InternetCheck.getConnectivityStatus(getApplicationContext()))
                {
                    progressDialog = new ProgressDialog(MapsActivity.this);
                    progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
                    progressDialog.setMessage("Loading...");
                    progressDialog.show();

                    updateLocationModel = new UpdateLocationModel();
                    updateLocationModel.setUserId(SharedPreference.getString(getApplicationContext(), Constants.USER_ID));

                    if (getIntent().getStringExtra(Constants.LOCATIONTYPE).equals(Constants.HOMELOCATION)) {
                        SharedPreference.saveString(getBaseContext(), Constants.HOMEPLACENAME, placeName);
                        SharedPreference.saveString(getBaseContext(), Constants.HOMELOCATION, locationString);
                        SharedPreference.saveString(getBaseContext(), Constants.HOMELATITUDE, latitudeString);
                        SharedPreference.saveString(getBaseContext(), Constants.HOMELONGITUDE, longitudeString);

                        updateLocationModel.setLocationType(getIntent().getStringExtra(Constants.LOCATIONTYPE));
                        updateLocationModel.setLocation(SharedPreference.getString(getApplicationContext(), Constants.HOMEPLACENAME));
                        updateLocationModel.setSubLocation(SharedPreference.getString(getApplicationContext(), Constants.HOMELOCATION));
                        updateLocationModel.setLatitude(SharedPreference.getString(getApplicationContext(), Constants.HOMELATITUDE));
                        updateLocationModel.setLangitude(SharedPreference.getString(getApplicationContext(), Constants.HOMELONGITUDE));


                    } else {
                        SharedPreference.saveString(getBaseContext(), Constants.WORKPLACENAME, placeName);
                        SharedPreference.saveString(getBaseContext(), Constants.WORKLOCATION, locationString.toString());
                        SharedPreference.saveString(getBaseContext(), Constants.WORKLATITUDE, latitudeString);
                        SharedPreference.saveString(getBaseContext(), Constants.WORKLONGITUDE, longitudeString);

                        updateLocationModel.setLocationType(getIntent().getStringExtra(Constants.LOCATIONTYPE));
                        updateLocationModel.setLocation(SharedPreference.getString(getApplicationContext(), Constants.WORKPLACENAME));
                        updateLocationModel.setSubLocation(SharedPreference.getString(getApplicationContext(), Constants.WORKLOCATION));
                        updateLocationModel.setLatitude(SharedPreference.getString(getApplicationContext(), Constants.WORKLATITUDE));
                        updateLocationModel.setLangitude(SharedPreference.getString(getApplicationContext(), Constants.WORKLONGITUDE));

                    }
                    updateLocations();
                }
                else
                {
                    Camera_Gallery.openSnackBar(llPick, getResources().getString(R.string.nointernet), getApplicationContext());
                }
                break;

        }
    }

    private void updateLocations() {

        if(apiConnection!=null)
        {
                apiConnection.setlocation(updateLocationModel, new GetRequestResponse() {
                    @Override
                    public void onResponse(String json) {
                        progressDialog.dismiss();
                        finish();
                    }

                    @Override
                    public void onFailure(String json) {

                    }
                });


        }
    }


    public String getFeatureAddress(double toLatitude, double toLongitude) {

        Geocoder geocoder = new Geocoder(this, Locale.ENGLISH);
        try {
            List<Address> addresses = geocoder.getFromLocation(toLatitude, toLongitude, 1);

            if (addresses.size() > 0) {

                 return addresses.get(0).getFeatureName()+", "+addresses.get(0).getSubLocality();

            } else {
                return "Searching Current Address";
            }

        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }


}
