package com.suvidha.suvidharider;

import android.content.Intent;
import android.graphics.Bitmap;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.suvidha.suvidharider.Utilities.Constants;
import com.suvidha.suvidharider.Utilities.SharedPreference;

public class ReferEarnActivity extends AppCompatActivity implements View.OnClickListener {

    private ImageButton backImageButton;
    private LinearLayout llShare;
    private ImageView img;
    private TextView tvReferCode;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_refer_earn);

        initialize();

        tvReferCode.setText(SharedPreference.getString(getBaseContext(), Constants.USER_ID));
    }

    private void initialize() {

        backImageButton=(ImageButton)findViewById(R.id.back);
        backImageButton.setOnClickListener(this);

        llShare = (LinearLayout) findViewById(R.id.llShare);
        llShare.setOnClickListener(this);

        img = (ImageView) findViewById(R.id.img);
        tvReferCode = (TextView) findViewById(R.id.tvReferCode);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId())
        {
            case R.id.back:
                finish();
                break;

            case R.id.llShare:
                Intent sharingIntent = new Intent(android.content.Intent.ACTION_SEND);
                sharingIntent.setType("text/plain");
                String shareBody = "Download the app from this link: https://drive.google.com/open?id=1CawDOSQx-_ek-UT8ucg9kwux5QQurtl6 "+" Use refer code at the time of register : "+ SharedPreference.getString(getBaseContext(), Constants.USER_ID);
                sharingIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, "Subject Here");
                sharingIntent.putExtra(android.content.Intent.EXTRA_TEXT, shareBody);
                startActivity(Intent.createChooser(sharingIntent, "Share via"));

                break;
        }
    }


}
