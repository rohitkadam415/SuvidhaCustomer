package com.suvidha.suvidharider;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;

import com.google.gson.Gson;
import com.rengwuxian.materialedittext.MaterialEditText;
import com.suvidha.suvidharider.Api.BaseClass;
import com.suvidha.suvidharider.Interface.GetRequestResponse;
import com.suvidha.suvidharider.Interface.RetrofitClient;
import com.suvidha.suvidharider.Interface.RetrofitInterface;
import com.suvidha.suvidharider.Models.RegisterResponseModel;
import com.suvidha.suvidharider.Utilities.Camera_Gallery;
import com.suvidha.suvidharider.Utilities.Constants;
import com.suvidha.suvidharider.Utilities.InternetCheck;
import com.suvidha.suvidharider.Utilities.SharedPreference;
import com.suvidha.suvidharider.Utilities.Validation;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ChangePasswordActivity extends BaseClass implements View.OnClickListener {

    ImageButton backImageButton;
    private MaterialEditText old_password, new_password, confirm_password;
    private TextView btnChangePassword;
    private ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_change_password);

        intialize();
        backImageButton.setOnClickListener(this);
        btnChangePassword.setOnClickListener(this);

    }

    private void intialize() {

        backImageButton = (ImageButton) findViewById(R.id.back);
        old_password = (MaterialEditText) findViewById(R.id.old_password);
        new_password = (MaterialEditText) findViewById(R.id.new_password);
        confirm_password = (MaterialEditText) findViewById(R.id.confirm_password);

        btnChangePassword = (TextView) findViewById(R.id.btnChangePassword);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.back:
                finish();
                break;

            case R.id.btnChangePassword:
                String oldPass = old_password.getText().toString();
                String newPass = new_password.getText().toString();
                String confirmPass = confirm_password.getText().toString();

                String user_id = SharedPreference.getString(getApplicationContext(), Constants.USER_ID);
                String b = SharedPreference.getString(getApplicationContext(), Constants.PASSWORD);

                if (InternetCheck.getConnectivityStatus(getApplicationContext())) {

                    if (validation()) {

                        if ((SharedPreference.getString(getApplicationContext(), Constants.PASSWORD)).equals(oldPass)) {
                            progressDialog = new ProgressDialog(ChangePasswordActivity.this);
                            progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
                            progressDialog.setMessage("Loading...");
                            progressDialog.show();

                            Constants.changeMobilePassword.setUserId(user_id);
                            Constants.changeMobilePassword.setOldpassword(oldPass);
                            Constants.changeMobilePassword.setNewpassword(confirmPass);

                            apiConnection.changepassword(Constants.changeMobilePassword, new GetRequestResponse() {
                                @Override
                                public void onResponse(String json) {
                                    final Gson gson = new Gson();
                                    final RegisterResponseModel registerResponseModel = gson.fromJson(json, RegisterResponseModel.class);
                                    SharedPreference.saveString(getApplicationContext(), Constants.PASSWORD, Constants.changeMobilePassword.getNewpassword());

                                    final Dialog dialog = new Dialog(ChangePasswordActivity.this);
                                    dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                                    dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                                    dialog.setContentView(R.layout.dialog_update_mobile);
                                    dialog.setCancelable(false);

                                    TextView tvMessege = (TextView) dialog.findViewById(R.id.tvMessege);
                                    tvMessege.setText("Your password has been changed");

                                    TextView done = (TextView) dialog.findViewById(R.id.done);
                                    done.setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View view) {
                                            dialog.dismiss();
                                            finish();
                                        }
                                    });
                                    progressDialog.cancel();
                                    dialog.show();
                                }

                                @Override
                                public void onFailure(String json) {
                                    progressDialog.cancel();
                                    final Gson gson = new Gson();
                                    final RegisterResponseModel registerResponseModel = gson.fromJson(json, RegisterResponseModel.class);

                                    Camera_Gallery.openSnackBar(confirm_password, registerResponseModel.getMessage(), getApplicationContext());

                                }
                            });

                        } else {
                            Camera_Gallery.openSnackBar(confirm_password, "Old password is wrong", getApplicationContext());
                        }

                    }
                } else {
                    Camera_Gallery.openSnackBar(confirm_password, getResources().getString(R.string.nointernet), getApplicationContext());
                }

                break;
        }
    }


    public boolean validation() {

        if (Validation.isPassword(old_password, "Password must be more than 6 digit") && Validation.isPassword(new_password, "Password must be more than 6 digit") && Validation.isPassword(confirm_password, "Password must be more than 6 digit") && Validation.isValidateConfirm(new_password, confirm_password, "Enter same password")) {
            return true;
        }
        return false;
    }

}
