package com.suvidha.suvidharider.Api;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.support.annotation.Nullable;

/**
 * Created by ingsaurabh on 27/11/17.
 */

public class SuvidhaRiderService extends Service {
    private IBinder binder;

    @Override
    public void onCreate() {
        super.onCreate();
        binder = new ApiConnection(this);
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return binder;
    }
}
