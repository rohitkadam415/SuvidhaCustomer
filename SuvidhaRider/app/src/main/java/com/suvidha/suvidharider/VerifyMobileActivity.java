package com.suvidha.suvidharider;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;


import com.google.gson.Gson;
import com.suvidha.suvidharider.Api.BaseClass;
import com.suvidha.suvidharider.Interface.GetRequestResponse;
import com.suvidha.suvidharider.Interface.RetrofitClient;
import com.suvidha.suvidharider.Interface.RetrofitInterface;
import com.suvidha.suvidharider.Interface.SmsListener;
import com.suvidha.suvidharider.Models.RegisterResponseModel;
import com.suvidha.suvidharider.Utilities.Camera_Gallery;
import com.suvidha.suvidharider.Utilities.Constants;
import com.suvidha.suvidharider.Utilities.InternetCheck;
import com.suvidha.suvidharider.Utilities.SharedPreference;
import com.suvidha.suvidharider.Utilities.SmsReceiver;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class VerifyMobileActivity extends BaseClass implements View.OnClickListener {

    private EditText etOtp;
    private ProgressDialog progressDialog;
    private Button btnSubmit;
    TextView resendTextView, timerTextView;
    private CountDownTimer countDownTimer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_verify_mobile);

        ImageButton back = (ImageButton) findViewById(R.id.back);
        TextView tvChangeNumber = (TextView) findViewById(R.id.tvChangeNumber);
        btnSubmit = (Button) findViewById(R.id.btnSubmit);
        resendTextView = (TextView) findViewById(R.id.resend);
        timerTextView = (TextView) findViewById(R.id.timer);
        etOtp = (EditText) findViewById(R.id.etOtp);

        back.setOnClickListener(this);
        tvChangeNumber.setOnClickListener(this);
        btnSubmit.setOnClickListener(this);
        resendTextView.setOnClickListener(this);

        startTimer();
        SmsReceiver.bindListener(new SmsListener() {
            @Override
            public void messageReceived(String messageText) {

                String detectedOtp = messageText.substring(messageText.length()-6);
               // Toast.makeText(VerifyMobileActivity.this,"Message: "+messageText,Toast.LENGTH_LONG).show();

                etOtp.setText(detectedOtp);
            }
        });
    }

    private void startTimer() {

        countDownTimer = new CountDownTimer(30000, 1000) {

            public void onTick(long millisUntilFinished) {

                timerTextView.setText("00:" + millisUntilFinished / 1000);
                btnSubmit.setEnabled(false);
                resendTextView.setEnabled(false);
                resendTextView.setTextColor(getResources().getColor(R.color.colorLightGreyText));
            }

            public void onFinish() {
                timerTextView.setText("");
                btnSubmit.setEnabled(true);
                resendTextView.setEnabled(true);
                resendTextView.setTextColor(getResources().getColor(R.color.colorPrimary));
            }
        };
        countDownTimer.start();

    }

    private void getOtpCode() {


        if (InternetCheck.getConnectivityStatus(getApplicationContext()))
        {
            apiConnection.getotp(Constants.registerModel, new GetRequestResponse() {
                @Override
                public void onResponse(String json) {
                    Gson gson = new Gson();
                    RegisterResponseModel registerResponseModel = gson.fromJson(json, RegisterResponseModel.class);

                    if (registerResponseModel.getSuccess().equals("true")) {
                        String otp_code = registerResponseModel.getOtp_code();
                        Constants.OtpCode = otp_code;
                   //     Toast.makeText(getBaseContext(), otp_code.toString(), Toast.LENGTH_LONG).show();
                        btnSubmit.setEnabled(true);
                        resendTextView.setEnabled(true);
                        countDownTimer.cancel();
                        timerTextView.setText("");
                        resendTextView.setTextColor(getResources().getColor(R.color.colorPrimary));
                    }
                }

                @Override
                public void onFailure(String json) {

                }
            });
        }
        else
        {
            Camera_Gallery.openSnackBar(btnSubmit, getResources().getString(R.string.nointernet), getApplicationContext());
        }
    }

    @Override
    protected void apiConnected() {
        if(apiConnection!=null)
        {
                getOtpCode();
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.resend:
                startTimer();
                getOtpCode();
                break;

            case R.id.back:
                finish();
                break;

            case R.id.tvChangeNumber:
                finish();
                break;

            case R.id.btnSubmit:

                progressDialog = new ProgressDialog(VerifyMobileActivity.this);
                progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
                progressDialog.setMessage("Loading...");
                progressDialog.show();


                if (getIntent().getStringExtra(Constants.UPDATEMOBILE) != null)
                {
                    if(apiConnection!=null)
                    {
                        apiConnection.changenumber(Constants.changeMobilePassword, new GetRequestResponse() {
                            @Override
                            public void onResponse(String json) {

                                SharedPreference.saveString(getApplicationContext(), Constants.MOBILE, Constants.registerModel.getMobile());

                                final Dialog dialog = new Dialog(VerifyMobileActivity.this);
                                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                                dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                                dialog.setContentView(R.layout.dialog_update_mobile);
                                dialog.setCancelable(false);

                                TextView done = (TextView) dialog.findViewById(R.id.done);
                                done.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View view) {
                                        dialog.dismiss();
                                        Constants.finishhFlag = true;
                                        finish();
                                    }
                                });

                                dialog.show();
                                progressDialog.cancel();
                            }

                            @Override
                            public void onFailure(String json) {
                                progressDialog.cancel();
                                final Gson gson = new Gson();
                                final RegisterResponseModel registerResponseModel = gson.fromJson(json, RegisterResponseModel.class);

                                Camera_Gallery.openSnackBar(btnSubmit, registerResponseModel.getMessage(), getApplicationContext());

                            }

                        });
                    }
                } else {

                    if (Constants.OtpCode.equals(etOtp.getText().toString()))  {

                        Constants.registerModel.setFcm_token(SharedPreference.getString(getBaseContext(), Constants.FCM_TOKEN));
                        apiConnection.register(Constants.registerModel, new GetRequestResponse() {
                            @Override
                            public void onResponse(String json) {

                                final Gson gson = new Gson();
                                final RegisterResponseModel registerResponseModel = gson.fromJson(json, RegisterResponseModel.class);

                                if (registerResponseModel.getSuccess().equals("true")) {

                                    SharedPreference.saveString(getApplicationContext(), Constants.FIRSTNAME, Constants.registerModel.getFirstname());
                                    SharedPreference.saveString(getApplicationContext(), Constants.LASTNAME, Constants.registerModel.getLastname());
                                    SharedPreference.saveString(getApplicationContext(), Constants.EMAIL, Constants.registerModel.getEmail());
                                    SharedPreference.saveString(getApplicationContext(), Constants.USER_ID, registerResponseModel.getUserId());
                                    SharedPreference.saveString(getApplicationContext(), Constants.COUNTRY_CODE, Constants.registerModel.getCountryCode());
                                    SharedPreference.saveString(getApplicationContext(), Constants.MOBILE, Constants.registerModel.getMobile());
                                    SharedPreference.saveString(getApplicationContext(), Constants.PASSWORD, Constants.registerModel.getPassword());
                                    SharedPreference.saveString(getApplicationContext(), Constants.REFER_CODE, Constants.registerModel.getReferCode());

                                    final Dialog dialog = new Dialog(VerifyMobileActivity.this);
                                    dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                                    dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                                    dialog.setContentView(R.layout.dialog_update_mobile);
                                    dialog.setCancelable(false);

                                    TextView tvMessege = (TextView) dialog.findViewById(R.id.tvMessege);
                                    tvMessege.setText("Registration Successfully\nWelcome To Suvidha");

                                    TextView done = (TextView) dialog.findViewById(R.id.done);
                                    done.setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View view) {
                                            MyApplication.getMyApplicationInstance().setMyUserId();
                                            Intent intent = new Intent(VerifyMobileActivity.this, HomeActivity.class);
                                            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                            startActivity(intent);
                                            dialog.dismiss();
                                        }
                                    });

                                    dialog.show();
                                    progressDialog.cancel();
                                }
                            }

                            @Override
                            public void onFailure(String json) {
                                progressDialog.cancel();
                                final Gson gson = new Gson();
                                final RegisterResponseModel registerResponseModel = gson.fromJson(json, RegisterResponseModel.class);

                                Camera_Gallery.openSnackBar(btnSubmit, registerResponseModel.getMessage(), getApplicationContext());

                            }

                        });

                    } else {
                        etOtp.setError("Invalid OTP");
                    }
                }
                break;
        }
    }

}
