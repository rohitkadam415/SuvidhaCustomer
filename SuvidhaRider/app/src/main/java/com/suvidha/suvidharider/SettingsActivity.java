package com.suvidha.suvidharider;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;

import com.suvidha.suvidharider.Utilities.Constants;

public class SettingsActivity extends AppCompatActivity {

    ImageButton backImageButton;
    TextView changePassTextView, tvUpdatePhone;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);

        backImageButton=(ImageButton)findViewById(R.id.back);
        changePassTextView=(TextView)findViewById(R.id.changePass);
        tvUpdatePhone=(TextView)findViewById(R.id.tvUpdatePhone);

        backImageButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        changePassTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                startActivity(new Intent(getBaseContext(),ChangePasswordActivity.class));
            }
        });

        tvUpdatePhone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Constants.finishhFlag = false;
                startActivity(new Intent(getBaseContext(),UpdateMobileNumberActivity.class));
            }
        });
    }
}
