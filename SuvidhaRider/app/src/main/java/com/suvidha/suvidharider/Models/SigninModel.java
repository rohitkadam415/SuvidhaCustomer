package com.suvidha.suvidharider.Models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class SigninModel {

    @SerializedName("Mobile")
    @Expose
    private String mobile;
    @SerializedName("Password")
    @Expose
    private String password;

    @SerializedName("Fcm_token")
    @Expose
    private String Fcm_token;

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getFcm_token() {
        return Fcm_token;
    }

    public void setFcm_token(String fcm_token) {
        Fcm_token = fcm_token;
    }
}