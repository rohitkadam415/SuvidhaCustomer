package com.suvidha.suvidharider;

import android.app.ProgressDialog;
import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Base64;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.signature.StringSignature;
import com.google.gson.Gson;
import com.makeramen.roundedimageview.RoundedImageView;
import com.soundcloud.android.crop.Crop;
import com.suvidha.suvidharider.Api.BaseClass;
import com.suvidha.suvidharider.Interface.GetRequestResponse;
import com.suvidha.suvidharider.Interface.RetrofitClient;
import com.suvidha.suvidharider.Interface.RetrofitInterface;
import com.suvidha.suvidharider.Models.RegisterResponseModel;
import com.suvidha.suvidharider.Models.UpdateProfileModel;
import com.suvidha.suvidharider.Utilities.Camera_Gallery;
import com.suvidha.suvidharider.Utilities.Constants;
import com.suvidha.suvidharider.Utilities.ImageCompress;
import com.suvidha.suvidharider.Utilities.InternetCheck;
import com.suvidha.suvidharider.Utilities.SharedPreference;
import com.suvidha.suvidharider.Utilities.Validation;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class EditProfileActivity extends BaseClass implements View.OnClickListener {

    private RoundedImageView profileImage;
    private EditText edtFirstName, edtLastName, edtEmail, edtMobile, input_country_code;
    String profileBase64String = null;
    private ImageView imgEditHome, imgEditWork, imEditIcon;
    private TextView tvPlaceHome, tvPlaceWork;
    String locationType = "";
    private String prof;
    private ProgressDialog dialog;
    private ImageButton back;

    private static byte[] loadFile(File file) throws IOException {
        InputStream is = new FileInputStream(file);

        long length = file.length();
        if (length > Integer.MAX_VALUE) {
            // File is too large
        }
        byte[] bytes = new byte[(int) length];

        int offset = 0;
        int numRead = 0;
        while (offset < bytes.length
                && (numRead = is.read(bytes, offset, bytes.length - offset)) >= 0) {
            offset += numRead;
        }

        if (offset < bytes.length) {
            throw new IOException("Could not completely read file " + file.getName());
        }

        is.close();
        return bytes;
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_profile);

        initialize();

        back = (ImageButton) findViewById(R.id.back);

        back.setOnClickListener(this);
        profileImage.setOnClickListener(this);
        imEditIcon.setOnClickListener(this);
        imgEditHome.setOnClickListener(this);
        imgEditWork.setOnClickListener(this);


        edtFirstName.setText(SharedPreference.getString(getApplicationContext(), Constants.FIRSTNAME));
        edtLastName.setText(SharedPreference.getString(getApplicationContext(), Constants.LASTNAME));
        edtEmail.setText(SharedPreference.getString(getApplicationContext(), Constants.EMAIL));
        edtMobile.setText(SharedPreference.getString(getApplicationContext(), Constants.MOBILE));
        input_country_code.setText("+"+SharedPreference.getString(getApplicationContext(), Constants.COUNTRY_CODE));

        tvPlaceHome.setText(SharedPreference.getString(getBaseContext(), Constants.HOMEPLACENAME));
        tvPlaceWork.setText(SharedPreference.getString(getBaseContext(), Constants.WORKPLACENAME));
        prof = SharedPreference.getString(getBaseContext(), Constants.PROFILE);

        if (prof != null && prof != "") {
            Glide.with(getApplicationContext())
                    .load(prof)
                    .into(profileImage);
        }

    }

    private void initialize() {

        profileImage = (RoundedImageView) findViewById(R.id.profileImage);
        imEditIcon = (ImageView) findViewById(R.id.imEditIcon);
        edtFirstName = (EditText) findViewById(R.id.input_name_first);
        edtLastName = (EditText) findViewById(R.id.input_name_last);
        edtEmail = (EditText) findViewById(R.id.input_email);
        edtMobile = (EditText) findViewById(R.id.input_mobile);
        input_country_code = (EditText) findViewById(R.id.input_country_code);

        imgEditHome = (ImageView) findViewById(R.id.imgEditHome);
        imgEditWork = (ImageView) findViewById(R.id.imgEditWork);

        tvPlaceHome = (TextView) findViewById(R.id.tvPlaceHome);
        tvPlaceWork = (TextView) findViewById(R.id.tvPlaceWork);


    }

    @Override

    public void onClick(View view) {

        switch (view.getId()) {
            case R.id.back:

                updateProfile();
                break;

            case R.id.profileImage:
                Camera_Gallery.selectImage(EditProfileActivity.this, profileImage);
                break;

            case R.id.imEditIcon:
                Camera_Gallery.selectImage(EditProfileActivity.this, profileImage);
                break;

            case R.id.imgEditHome:
                locationType = Constants.HOMELOCATION;
                startActivity(new Intent(EditProfileActivity.this, MapsActivity.class).putExtra(Constants.LOCATIONTYPE, locationType));

                break;

            case R.id.imgEditWork:
                locationType = Constants.WORKLOCATION;
                startActivity(new Intent(EditProfileActivity.this, MapsActivity.class).putExtra(Constants.LOCATIONTYPE, locationType));
                break;

        }


    }


    @Override
    public void onBackPressed() {
        updateProfile();
    }

    private void updateProfile() {

        String fn = SharedPreference.getString(getBaseContext(), Constants.FIRSTNAME);
        String ln = SharedPreference.getString(getBaseContext(), Constants.LASTNAME);
        String email = SharedPreference.getString(getBaseContext(), Constants.EMAIL);
        String mob = SharedPreference.getString(getBaseContext(), Constants.MOBILE);
        String cc = SharedPreference.getString(getBaseContext(), Constants.COUNTRY_CODE);
        String pro = SharedPreference.getString(getBaseContext(), Constants.PROFILE);
        if (fn.equals(edtFirstName.getText().toString()) && ln.equals(edtLastName.getText().toString()) && email.equals(edtEmail.getText().toString()) && mob.equals(edtMobile.getText().toString()) && cc.equals(input_country_code.getText().toString().substring(1, input_country_code.getText().length())) && profileBase64String == null) {
            finish();
        } else {
            if (Validation.isNotEmpty(edtFirstName, "Please enter firstname") && Validation.isNotEmpty(edtLastName, "Please enter lastname") && Validation.isValidateEmailAddress(edtEmail, "Please enter email") && Validation.isValidContact(edtMobile, "Please enter mobile")) {

                if(InternetCheck.getConnectivityStatus(getBaseContext()))
                {
                    dialog = new ProgressDialog(EditProfileActivity.this);
                    dialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
                    dialog.setMessage("Loading...");
                    dialog.show();

                    final UpdateProfileModel updateProfileModel = new UpdateProfileModel();

                    updateProfileModel.setFirstname(edtFirstName.getText().toString());
                    updateProfileModel.setLastname(edtLastName.getText().toString());
                    updateProfileModel.setEmail(edtEmail.getText().toString());
                    updateProfileModel.setMobile(edtMobile.getText().toString());
                    updateProfileModel.setCountryCode(cc);
                    updateProfileModel.setProfile(profileBase64String);
                    updateProfileModel.setUserId(SharedPreference.getString(getApplicationContext(), Constants.USER_ID));


                    if (apiConnection != null) {
                        apiConnection.updateprofile(updateProfileModel, new GetRequestResponse() {
                            @Override
                            public void onResponse(String json) {
                                Gson gson = new Gson();
                                RegisterResponseModel registerResponseModel = gson.fromJson(json, RegisterResponseModel.class);

                                if (registerResponseModel.getSuccess().equals("true")) {
                                    SharedPreference.saveString(getApplicationContext(), Constants.FIRSTNAME, updateProfileModel.getFirstname());
                                    SharedPreference.saveString(getApplicationContext(), Constants.LASTNAME, updateProfileModel.getLastname());
                                    SharedPreference.saveString(getApplicationContext(), Constants.EMAIL, updateProfileModel.getEmail());
                                    if (registerResponseModel.getProfileurl() != null) {
                                        SharedPreference.saveString(getApplicationContext(), Constants.PROFILE, registerResponseModel.getProfileurl());
                                    }
                                    SharedPreference.saveString(getApplicationContext(), Constants.MOBILE, updateProfileModel.getMobile());
                                    SharedPreference.saveString(getApplicationContext(), Constants.COUNTRY_CODE, updateProfileModel.getCountryCode());
                                    dialog.dismiss();
                                    finish();
                                }
                                dialog.dismiss();
                            }

                            @Override
                            public void onFailure(String json) {
                                dialog.dismiss();
                            }
                        });
                    }

                }
                else
                {
                    Camera_Gallery.openSnackBar(back, getResources().getString(R.string.nointernet), getApplicationContext());

                }
            }
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent result) {
        if (requestCode == Crop.REQUEST_PICK && resultCode == RESULT_OK) {
            profileImage.setImageDrawable(null);
            beginCrop(result.getData());
        } else if (requestCode == Constants.REQUEST_CAMERA && resultCode == RESULT_OK) {
            profileImage.setImageDrawable(null);
            beginCrop(Constants.profileImageUri);
        } else if (requestCode == Crop.REQUEST_CROP) {
            handleCrop(resultCode, result);
        }
    }

    private void beginCrop(Uri source) {
        Uri destination = Uri.fromFile(new File(getCacheDir(), "cropped"));
        Crop.of(source, destination).asSquare().start(this);
    }

    private void handleCrop(int resultCode, Intent result) {
        if (resultCode == RESULT_OK) {
            profileImage.setImageURI(Crop.getOutput(result));

            File profileFile = null;
            try {
                profileFile = ImageCompress.from(this, Crop.getOutput(result));

            } catch (IOException e) {
                e.printStackTrace();
            }
            byte[] bytes1 = new byte[0];
            try {
                bytes1 = loadFile(profileFile);
            } catch (IOException e) {
                e.printStackTrace();
            }

            profileBase64String = Base64.encodeToString(bytes1, Base64.NO_WRAP);

        } else if (resultCode == Crop.RESULT_ERROR) {
            Toast.makeText(this, Crop.getError(result).getMessage(), Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (locationType.equals(Constants.HOMELOCATION)) {
            tvPlaceHome.setText(SharedPreference.getString(getBaseContext(), Constants.HOMEPLACENAME));
        } else if (locationType.equals(Constants.WORKLOCATION)) {
            tvPlaceWork.setText(SharedPreference.getString(getBaseContext(), Constants.WORKPLACENAME));
        }

    }
}
