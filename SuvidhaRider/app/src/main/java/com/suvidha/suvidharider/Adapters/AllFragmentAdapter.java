package com.suvidha.suvidharider.Adapters;

import android.app.Activity;
import android.content.Intent;
import android.location.Address;
import android.location.Geocoder;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.makeramen.roundedimageview.RoundedImageView;
import com.suvidha.suvidharider.InvoiceActivity;
import com.suvidha.suvidharider.Models.CompleteRideDetail;
import com.suvidha.suvidharider.Models.InCompleteRideDetail;
import com.suvidha.suvidharider.Models.MyBookingModel;
import com.suvidha.suvidharider.R;
import com.suvidha.suvidharider.Utilities.Camera_Gallery;
import com.suvidha.suvidharider.Utilities.Constants;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

/**
 * Created by cybertruimph on 2/13/2018.
 */

public class AllFragmentAdapter extends RecyclerView.Adapter<AllFragmentAdapter.MyCredits> {

    private List<InCompleteRideDetail> pList;
    private View itemView;

    public AllFragmentAdapter(List<InCompleteRideDetail> pList) {
        this.pList = pList;
    }

    public class MyCredits extends RecyclerView.ViewHolder {

        private final TextView tvName, tvCarName, tvLocation, tvResult, tvTime, tvAmount, tvPaymentType;
        private final ImageView imMap;
        private final RoundedImageView  imProfile;
        private final RelativeLayout llPay;
        private final View view;
        private final Button btnInvoice;


        public MyCredits(View itemView) {
            super(itemView);
            tvName = (TextView) itemView.findViewById(R.id.tvName);
            tvCarName = (TextView) itemView.findViewById(R.id.tvCarName);
            tvLocation = (TextView) itemView.findViewById(R.id.tvLocation);
            tvResult = (TextView) itemView.findViewById(R.id.tvResult);
            tvTime = (TextView) itemView.findViewById(R.id.tvTime);
            tvAmount = (TextView) itemView.findViewById(R.id.tvAmount);
            tvPaymentType = (TextView) itemView.findViewById(R.id.tvPaymentType);
            imMap = (ImageView) itemView.findViewById(R.id.imMap);
            imProfile = (RoundedImageView) itemView.findViewById(R.id.imProfile);
            llPay = (RelativeLayout) itemView.findViewById(R.id.llPay);
            view = (View) itemView.findViewById(R.id.view);
            btnInvoice = (Button) itemView.findViewById(R.id.btnInvoice);
        }

    }

    @Override
    public AllFragmentAdapter.MyCredits onCreateViewHolder(ViewGroup parent, int viewType) {
        itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.custom_recycler_all_booking, parent, false);
        return new AllFragmentAdapter.MyCredits(itemView);
    }

    @Override
    public void onBindViewHolder(AllFragmentAdapter.MyCredits holder, final int position) {
        holder.tvName.setText(pList.get(position).getFirstname()+" "+pList.get(position).getLastname());
        holder.tvCarName.setText(pList.get(position).getVehicleType());
        holder.tvLocation.setText(Camera_Gallery.getAddressFromLocation(itemView.getContext(), Double.valueOf(pList.get(position).getToLatitude()), Double.valueOf(pList.get(position).getToLongitude())));
        holder.tvResult.setText(pList.get(position).getStatus());
        holder.tvAmount.setText(itemView.getResources().getString(R.string.Rs)+" "+pList.get(position).getTotalAmount());
        holder.tvTime.setText(Camera_Gallery.getFormattedDate(itemView.getContext(), pList.get(position).getDate()));

        Glide.with(itemView.getContext())
                .load(pList.get(position).getProfile())
                .into(holder.imProfile);

        if (pList.get(position).getStatus().equals("Cancelled"))
        {
            holder.btnInvoice.setVisibility(View.GONE);
            holder.view.setVisibility(View.GONE);
            holder.llPay.setVisibility(View.GONE);
            holder.tvResult.setTextColor(itemView.getResources().getColor(R.color.colorFaintGreen));
        }
        else
        {
            holder.btnInvoice.setVisibility(View.VISIBLE);
            holder.tvResult.setTextColor(itemView.getResources().getColor(R.color.colorOrange));
            holder.view.setVisibility(View.VISIBLE);
            holder.llPay.setVisibility(View.VISIBLE);
        }

        Glide.with(itemView.getContext())
                .load(pList.get(position).getMapurl())
                .into(holder.imMap);

        holder.btnInvoice.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Constants.invoiceModel.setTotalFare(pList.get(position).getTotalAmount());
                Constants.invoiceModel.setTotalDistance(pList.get(position).getDistance());
                Constants.invoiceModel.setFromLatitude(pList.get(position).getFromLatitude());
                Constants.invoiceModel.setFromLongitude(pList.get(position).getFromLongitude());
                Constants.invoiceModel.setToLatitude(pList.get(position).getToLatitude());
                Constants.invoiceModel.setToLongitude(pList.get(position).getToLongitude());
                Constants.invoiceModel.setTripFare(pList.get(position).getFareAmount());
                Constants.invoiceModel.setTolls(pList.get(position).getTollAmount());
                Constants.invoiceModel.setRiderDiscounts(pList.get(position).getTotalAmount());
                Constants.invoiceModel.setOutstanding(pList.get(position).getTotalAmount());
                Constants.invoiceModel.setTotal(pList.get(position).getTotalAmount());
                Constants.invoiceModel.setParking(pList.get(position).getParkingAmount());
                Constants.invoiceModel.setDue(pList.get(position).getDueAmount());

                itemView.getContext().startActivity(new Intent(itemView.getContext(), InvoiceActivity.class));
            }
        });
    }

    @Override
    public int getItemCount() {
        return pList.size();
    }

}