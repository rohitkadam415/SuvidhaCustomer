package com.suvidha.suvidharider.Models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by cybertruimph on 3/27/2018.
 */

public class DueDetail {

    @SerializedName("Amount")
    @Expose
    private String amount;
    @SerializedName("Date")
    @Expose
    private String date;
    @SerializedName("DriverId")
    @Expose
    private String driverId;

    @SerializedName("DueId")
    @Expose
    private String dueId;


    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getDriverId() {
        return driverId;
    }

    public void setDriverId(String driverId) {
        this.driverId = driverId;
    }

    public String getDueId() {
        return dueId;
    }

    public void setDueId(String dueId) {
        this.dueId = dueId;
    }
}
