package com.suvidha.suvidharider.Api;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;

/**
 * Created by cybertruimph on 3/16/2018.
 */

public abstract class BaseClass extends AppCompatActivity {

    protected ApiConnection apiConnection;
    private ServiceConnection connection = new ServiceConnection() {

        public void onServiceConnected(ComponentName className,
                                       IBinder service) {
            apiConnection = (ApiConnection) service;
            apiConnected();
        }

        public void onServiceDisconnected(ComponentName arg0) {
        }

    };

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        bindService(new Intent(this, SuvidhaRiderService.class), connection, Context.BIND_AUTO_CREATE);

    }

    protected void apiConnected() {
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if(apiConnection!=null)
        {
            unbindService(connection);
        }
    }
}
