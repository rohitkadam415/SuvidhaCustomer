package com.suvidha.suvidharider.Interface;

import com.google.gson.JsonObject;
import com.suvidha.suvidharider.Models.CarDetailsResponseModel;
import com.suvidha.suvidharider.Models.SigninResponseModel;

/**
 * Created by cybertruimph on 3/16/2018.
 */

public interface GetRequestResponse {

    void onResponse(String json);

    void  onFailure(String json);

}
