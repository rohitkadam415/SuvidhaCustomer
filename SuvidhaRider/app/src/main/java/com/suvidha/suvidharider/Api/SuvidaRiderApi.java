package com.suvidha.suvidharider.Api;

import android.content.Context;
import android.location.Location;

import com.google.android.gms.maps.model.LatLng;
import com.suvidha.suvidharider.ChangePasswordActivity;
import com.suvidha.suvidharider.Interface.GetRequestResponse;
import com.suvidha.suvidharider.Models.BookingModel;
import com.suvidha.suvidharider.Models.ChangeMobilePassword;
import com.suvidha.suvidharider.Models.ConfirmBookingModel;
import com.suvidha.suvidharider.Models.DistanceModel;
import com.suvidha.suvidharider.Models.Homelocation;
import com.suvidha.suvidharider.Models.RatingModel;
import com.suvidha.suvidharider.Models.ReasonsModel;
import com.suvidha.suvidharider.Models.RegisterModel;
import com.suvidha.suvidharider.Models.SigninModel;
import com.suvidha.suvidharider.Models.UpdateLocationModel;
import com.suvidha.suvidharider.Models.UpdateProfileModel;


/**
 * Created by ingsaurabh on 27/11/17.
 */

public interface SuvidaRiderApi {


    void changeLocation(String latitude, String longitude, Context context);

    void getAllCars(Homelocation homelocation, GetRequestResponse getRequestResponse);

    void checkLogin(SigninModel signinModel,GetRequestResponse getRequestResponse);

    void changepassword(ChangeMobilePassword changeMobilePassword, GetRequestResponse getRequestResponse);

    void changenumber(ChangeMobilePassword changeMobilePassword, GetRequestResponse getRequestResponse);

    void register(RegisterModel registerModel,GetRequestResponse getRequestResponse);

    void getotp(RegisterModel registerModel,GetRequestResponse getRequestResponse);

    void updateprofile(UpdateProfileModel updateProfileModel,GetRequestResponse getRequestResponse);


    void setlocation(UpdateLocationModel updateLocationModel,GetRequestResponse getRequestResponse);

    void confirmride(BookingModel bookingModel,GetRequestResponse getRequestResponse);

    void confirmBooking(ConfirmBookingModel confirmBookingModel, GetRequestResponse getRequestResponse);

    void checkDistance(DistanceModel distanceModel, GetRequestResponse getRequestResponse);

    void changepaymentMode(DistanceModel distanceModel, GetRequestResponse getRequestResponse);

    void getBookingDetails(BookingModel bookingModel, GetRequestResponse getRequestResponse);

    void getDriverLatLang(ReasonsModel reasonsModel, GetRequestResponse getRequestResponse);

    void saveRating(RatingModel ratingModel, GetRequestResponse getRequestResponse);

    void checkLastDriver(RatingModel ratingModel, GetRequestResponse getRequestResponse);

    void getInvoiceDetails(RatingModel ratingModel, GetRequestResponse getRequestResponse);

    void getNotification(RatingModel ratingModel, GetRequestResponse getRequestResponse);
}