package com.suvidha.suvidharider.Utilities;


import android.net.Uri;

import com.google.gson.JsonObject;
import com.suvidha.suvidharider.Models.BookingResponseModel;
import com.suvidha.suvidharider.Models.CarlocationModel;
import com.suvidha.suvidharider.Models.ChangeMobilePassword;
import com.suvidha.suvidharider.Models.DriverDetailsModel;
import com.suvidha.suvidharider.Models.InvoiceModel;
import com.suvidha.suvidharider.Models.LastDriverDetails;
import com.suvidha.suvidharider.Models.RegisterModel;
import com.suvidha.suvidharider.Models.TripDetails;

/**
 * Created by cybertruimph on 2/24/2018.
 */

public class Constants {

    public static final String FIRSTNAME = "firstname";
    public static final String LASTNAME = "lastname";
    public static final String VEHICLE_TYPE = "vehicle_type";
    public static final String VEHICLE_NUMBER = "vehicle_number";
    public static final String ADDRESS = "adress";
    public static final String EMAIL = "email";
    public static final String COUNTRY_CODE = "country_code";
    public static final String MOBILE = "mobile";
    public static final String PASSWORD = "password";
    public static final String USER_ID = "user_id";
    public static final String PROFILE = "profile";
    // global topic to receive app wide push notifications
    public static final String TOPIC_GLOBAL = "global";
    // broadcast receiver intent filters
    public static final String REGISTRATION_COMPLETE = "registrationComplete";
    public static final String PUSH_NOTIFICATION = "pushNotification";
    // id to handle the notification in the notification tray
    public static final int NOTIFICATION_ID = 100;
    public static final int NOTIFICATION_ID_BIG_IMAGE = 101;
    public static final String FCM_TOKEN = "fcm_token";
    public static final String LOCATIONTYPE = "locationtype";
    public static final String HOMEPLACENAME = "homeplacename";
    public static final String WORKPLACENAME = "workplacename";
    public static final String UPDATEMOBILE = "updatemobile";
    public static final String APPROX_TIME = "approx_time";
    public static final String FROM_LOCATION = "from_loc";
    public static final String TO_LOCATION = "to_loc";
    public static final String FARE_AMT = "fare_amt";
    public static final String DUE_AMT = "due_amt";
    public static final String DRIVER_ID = "driver_id";
    public static final String PREVIOUS_FROM_LOCATION = "prefrom";
    public static final String PREVIOUS_TO_LOCATION = "preto";
    public static final String PREVIOUS_PROFILE = "prepprofile";
    public static final String PREVIOUS_TIME = "pretime";
    public static final String REFER_CODE = "refercode";
    public static String REASON = "";
    public static String SERVICE = "";
    public static String TO_LONGITUDE = "";
    public static String TO_LATTITUDE = "";
    public static String FROM_LONGITUDE = "";
    public static String FROM_LATTITUDE = "";
    public static String ENCODED_STRING = "";
    public static String PICKUPLOCATION = "";
    public static String HOMELOCATION = "Homelocation";
    public static String HOMELATITUDE = "homelatitude";
    public static String HOMELONGITUDE = "homelongitude";

    public static String WORKLOCATION = "Worklocation";
    public static String WORKLATITUDE= "worklatitude";
    public static String WORKLONGITUDE= "worklongitude";
    public static String OtpCode = "otp";
    public static String LATITUDE="latitude";
    public static String LONGITUDE="longitude";

    public static RegisterModel registerModel = new RegisterModel();

    public static ChangeMobilePassword changeMobilePassword = new ChangeMobilePassword();

    public static BookingResponseModel bookingResponseModel =new BookingResponseModel();
    public static DriverDetailsModel driverDetailsModel =new DriverDetailsModel();
    public static TripDetails tripDetails =new TripDetails();

    public static final int REQUEST_CAMERA = 1234;
    public static Uri profileImageUri = null;
    public static String Regiseter="register";
    public static Boolean finishhFlag=false;

    public static InvoiceModel invoiceModel = new InvoiceModel();

    public static LastDriverDetails lastDriverDetails = new LastDriverDetails();

}
