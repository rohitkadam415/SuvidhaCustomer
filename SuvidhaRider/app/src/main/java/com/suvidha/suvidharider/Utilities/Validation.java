package com.suvidha.suvidharider.Utilities;

import android.widget.EditText;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by kedar on 1/25/2018.
 */

public class Validation {

    private static final String PASSWORD_PATTERN = "((?=.*\\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[@#$%]).{6,20})";
    private static final String EMAIL_PATTERN =
            "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
                    + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
    private static Pattern pattern;
    private static Matcher matcher;

    public Validation() {
        pattern = Pattern.compile(PASSWORD_PATTERN);
    }


    public static boolean isValidate(EditText editText, String error) {

        matcher = pattern.matcher(editText.getText());
        if (matcher.matches() == false) {
            editText.setError(error);
            editText.requestFocus();
        }
        return matcher.matches();

    }

    public static boolean isValidateConfirm(EditText editText, EditText editText1, String error) {
        if (editText.getText().toString().trim().equals(editText1.getText().toString())) {
            return true;
        }
        editText1.setError(error);
        editText1.requestFocus();
        return false;
    }

    public static boolean isNotEmpty(EditText editText, String error) {
        if (editText.getText().toString().trim().length() > 0) {
            return true;
        }
        editText.setError(error);
        editText.requestFocus();
        return false;
    }

    public static boolean isValidContact(EditText editText, String error) {
        if (editText.getText().length() == 10) {
            return true;
        }
        editText.setError(error);
        editText.requestFocus();
        return false;
    }

    public static boolean isPassword(EditText editText, String error) {
        if (editText.getText().length() >= 6 && editText.getText().length() <= 11) {
            return true;
        }
        editText.setError(error);
        editText.requestFocus();
        return false;
    }

    public static boolean isValidateEmailAddress(EditText editText, String error) {
        Matcher matcherObj = Pattern.compile(EMAIL_PATTERN).matcher(editText.getText());

        if (matcherObj.matches()) {
            return true;
        }
        editText.setError(error);
        editText.requestFocus();
        return false;
    }

    public static String setCapitalizeletter(String s)
    {
        return s.substring(0,1).toUpperCase()+s.substring(1,s.length()).toLowerCase();
    }

    public static String spliString(String s)
    {
        String a[] = s.split(" ");
        return a[0];
    }
}
