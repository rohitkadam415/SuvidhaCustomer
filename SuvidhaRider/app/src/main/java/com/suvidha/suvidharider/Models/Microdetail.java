package com.suvidha.suvidharider.Models;

/**
 * Created by cybertruimph on 3/8/2018.
 */


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Microdetail {

    @SerializedName("basic_rate")
    @Expose
    private String basicRate;
    @SerializedName("km_rate")
    @Expose
    private String kmRate;
    @SerializedName("min_rate")
    @Expose
    private String minRate;
    @SerializedName("timetoreach")
    @Expose
    private String timetoreach;
    @SerializedName("maxpersons")
    @Expose
    private String maxpersons;

    public String getBasicRate() {
        return basicRate;
    }

    public void setBasicRate(String basicRate) {
        this.basicRate = basicRate;
    }

    public String getKmRate() {
        return kmRate;
    }

    public void setKmRate(String kmRate) {
        this.kmRate = kmRate;
    }

    public String getMinRate() {
        return minRate;
    }

    public void setMinRate(String minRate) {
        this.minRate = minRate;
    }

    public String getTimetoreach() {
        return timetoreach;
    }

    public void setTimetoreach(String timetoreach) {
        this.timetoreach = timetoreach;
    }

    public String getMaxpersons() {
        return maxpersons;
    }

    public void setMaxpersons(String maxpersons) {
        this.maxpersons = maxpersons;
    }

}
