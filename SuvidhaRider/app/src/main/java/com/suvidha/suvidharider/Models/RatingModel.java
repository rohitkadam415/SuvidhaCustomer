package com.suvidha.suvidharider.Models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by cybertruimph on 4/5/2018.
 */

public class RatingModel {

    @SerializedName("UserId")
    @Expose
    private String UserId;

    @SerializedName("DriverId")
    @Expose
    private String driverId;

    @SerializedName("Rating")
    @Expose
    private String rating;

    @SerializedName("Comment")
    @Expose
    private String comment;

    @SerializedName("ReviewTime")
    @Expose
    private String reviewTime;

    @SerializedName("RideId")
    @Expose
    private String rideId;

    @SerializedName("RequestId")
    @Expose
    private String requestId;

    public String getUserId() {
        return UserId;
    }

    public void setUserId(String userId) {
        UserId = userId;
    }

    public String getDriverId() {
        return driverId;
    }

    public void setDriverId(String driverId) {
        this.driverId = driverId;
    }

    public String getRating() {
        return rating;
    }

    public void setRating(String rating) {
        this.rating = rating;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public String getReviewTime() {
        return reviewTime;
    }

    public void setReviewTime(String reviewTime) {
        this.reviewTime = reviewTime;
    }

    public String getRideId() {
        return rideId;
    }

    public void setRideId(String rideId) {
        this.rideId = rideId;
    }

    public String getRequestId() {
        return requestId;
    }

    public void setRequestId(String requestId) {
        this.requestId = requestId;
    }
}
