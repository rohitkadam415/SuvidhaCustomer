package com.suvidha.suvidharider.Utilities;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.location.Address;
import android.location.Geocoder;
import android.location.LocationManager;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.design.widget.Snackbar;
import android.support.v4.content.FileProvider;
import android.support.v7.app.AlertDialog;
import android.text.Html;
import android.text.format.DateFormat;
import android.text.format.Time;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;


import com.makeramen.roundedimageview.RoundedImageView;
import com.soundcloud.android.crop.Crop;
import com.suvidha.suvidharider.BuildConfig;
import com.suvidha.suvidharider.R;

import java.io.File;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

/**
 * Created by cybertruimph on 2/24/2018.
 */

public class Camera_Gallery {

    private static Activity activity;

    private static String userChoosenTask;

    public static void selectImage(final Activity context, final RoundedImageView profileImage) {

        activity = context;

        final CharSequence[] items = {context.getString(R.string.takephoto), context.getString(R.string.choosefromlibrary),
                context.getString(R.string.cancel)};

        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle(Html.fromHtml("<b>"+context.getString(R.string.Addphoto)+"<b>"));

        builder.setItems(items, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {
                // boolean result = Utility.checkPermission(RegisterActivity.this);

                if (items[item].equals(context.getString(R.string.takephoto))) {
                    userChoosenTask = context.getString(R.string.takephoto);

                    Constants.profileImageUri = generateTimeStampPhotoFileUri(context);
                    Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                    intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
                    intent.putExtra(MediaStore.EXTRA_OUTPUT, Constants.profileImageUri);
                    context.startActivityForResult(intent, Constants.REQUEST_CAMERA);
                } else if (items[item].equals(context.getString(R.string.choosefromlibrary))) {
                    userChoosenTask = context.getString(R.string.choosefromlibrary);
                    //  if (result)

                    Crop.pickImage(context);

                } else if (items[item].equals(context.getString(R.string.cancel))) {
                    dialog.dismiss();
                }
            }
        });
        builder.show();
    }


    private static Uri generateTimeStampPhotoFileUri(Context context) {

        Uri photoFileUri = null;


        File direct = getPhotoDirectory();
        if (direct != null) {
            Time t = new Time();
            t.setToNow();
            File photoFile = new File(direct, System.currentTimeMillis()
                    + ".jpg");
            //  photoFileUri = Uri.fromFile(photoFile);

            if (Build.VERSION.SDK_INT >= 24) {
                photoFileUri = FileProvider.getUriForFile(context,
                        BuildConfig.APPLICATION_ID + ".provider",
                        photoFile);
            } else {
                photoFileUri = Uri.fromFile(photoFile);
            }
        }
        return photoFileUri;
    }



    public static boolean checkLocation(Context context) {
        LocationManager mLocationManager = (LocationManager)context. getSystemService(Context.LOCATION_SERVICE);
        boolean gpsProvider = mLocationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
        boolean networkProvider = mLocationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
        return gpsProvider && networkProvider;
    }

    public static void showSettingsAlert(final Context context) {


        android.app.AlertDialog.Builder alertDialog = new android.app.AlertDialog.Builder(context);
        // Setting Dialog Title
        alertDialog.setTitle("GPS Settings");

        alertDialog.setCancelable(false);
        // Setting Dialog Message
        alertDialog.setMessage("GPS is not enabled. Do you want to go to settings menu?");
        // On pressing Settings button
        alertDialog.setPositiveButton("Settings", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {

                Intent intent = new Intent(android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                context.startActivity(intent);

            }
        });
        // on pressing cancel button
        alertDialog.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });
        // Showing Alert Message
        alertDialog.show();

    }

    private static File getPhotoDirectory() {
        File outputDir = null;
        String externalStorageStagte = Environment.getExternalStorageState();
        if (externalStorageStagte.equals(Environment.MEDIA_MOUNTED)) {
            outputDir = new File(Environment.getExternalStorageDirectory() + "/SuvidhaRider/");
            if (!outputDir.exists()) {
                outputDir = new File(Environment.getExternalStorageDirectory().getPath() + "/SuvidhaRider/");
                outputDir.mkdirs();
            }
        }
        return outputDir;
    }

    public static void openSnackBar(View view, String message, Context context)
    {
        Snackbar snak = Snackbar.make(view, message, Snackbar.LENGTH_LONG);
        View snackBarView = snak.getView();
        TextView textView = (TextView) snackBarView.findViewById(android.support.design.R.id.snackbar_text);
        textView.setTextSize(14.0f);
        snackBarView.setBackgroundColor(context.getResources().getColor(R.color.colorPrimary));
        snak.show();
    }

    public static String getFormattedDate(Context context, String date) {

        long timeInMilliseconds = 0;
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        try {
            Date mDate = sdf.parse(date);
            timeInMilliseconds = mDate.getTime();
            System.out.println("Date in milli :: " + timeInMilliseconds);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        Calendar smsTime = Calendar.getInstance();
        smsTime.setTimeInMillis(timeInMilliseconds);

        Calendar now = Calendar.getInstance();

        final String timeFormatString = "h:mm aa";
        final String dateTimeFormatString = "EEEE, MMMM d, h:mm aa";
        final long HOURS = 60 * 60 * 60;
        if (now.get(Calendar.DATE) == smsTime.get(Calendar.DATE) ) {
            return "Today " + DateFormat.format(timeFormatString, smsTime);
        } else if (now.get(Calendar.DATE) - smsTime.get(Calendar.DATE) == 1  ){
            return "Yesterday " + DateFormat.format(timeFormatString, smsTime);
        }
//        else if (now.get(Calendar.YEAR) == smsTime.get(Calendar.YEAR)) {
//            return DateFormat.format(dateTimeFormatString, smsTime).toString();
//        }
        else {
            //return DateFormat.format("MMMM dd yyyy, h:mm aa", smsTime).toString();
            return DateFormat.format("yyyy-MM-dd HH:mm:ss", smsTime).toString();
        }
    }


    public static String getAddressFromLocation(Context context, double toLatitude, double toLongitude) {

        Geocoder geocoder = new Geocoder(context, Locale.ENGLISH);
        try {
            List<Address> addresses = geocoder.getFromLocation(toLatitude, toLongitude, 1);

            if (addresses.size() > 0) {
                Address fetchedAddress = addresses.get(0);
                StringBuilder strAddress = new StringBuilder();
                for (int i = 0; i <= fetchedAddress.getMaxAddressLineIndex(); i++) {
                    strAddress.append(fetchedAddress.getAddressLine(i)).append(" ");
                }
                return strAddress.toString();

            } else {
                return "Searching Current Address";
            }

        } catch (IOException e) {
            e.printStackTrace();
            //  printToast("Could not get address..!");
        }
        return null;
    }

}
