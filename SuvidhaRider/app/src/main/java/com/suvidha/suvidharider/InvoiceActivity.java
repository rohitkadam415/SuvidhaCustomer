package com.suvidha.suvidharider;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ScrollView;
import android.widget.TextView;

import com.suvidha.suvidharider.Utilities.Camera_Gallery;
import com.suvidha.suvidharider.Utilities.Constants;
import com.suvidha.suvidharider.Utilities.InternetCheck;
import com.suvidha.suvidharider.Utilities.Validation;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.DecimalFormat;

/**
 * Created by cybertruimph on 4/2/2018.
 */

public class InvoiceActivity extends AppCompatActivity implements View.OnClickListener {

    private ImageButton back;
    private TextView tvTotalFare, tvtotalDistance, tvFrom, tvTo, tvTripFare, tvTolls, tvRiderDiscount, tvOutstanding, tvTotal, tvParking, tvDueAmt;
    private Button btnShare;
    private ScrollView scroll;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_invoice);
        initialize();

        back.setOnClickListener(this);
        btnShare.setOnClickListener(this);

        bindData();

    }

    private void bindData() {

        tvTotalFare.setText(getString(R.string.Rs)+" "+formatFigureTwoPlaces(Constants.invoiceModel.getTotalFare()));
        tvtotalDistance.setText(Constants.invoiceModel.getTotalDistance());
        tvFrom.setText(Camera_Gallery.getAddressFromLocation(getBaseContext(), Double.valueOf(Constants.invoiceModel.getFromLatitude()), Double.valueOf(Constants.invoiceModel.getFromLongitude())));
        tvTo.setText(Camera_Gallery.getAddressFromLocation(getBaseContext(), Double.valueOf(Constants.invoiceModel.getToLatitude()), Double.valueOf(Constants.invoiceModel.getToLongitude())));
        tvTripFare.setText(formatFigureTwoPlaces(Constants.invoiceModel.getTripFare()));
        tvTolls.setText(formatFigureTwoPlaces(Constants.invoiceModel.getTolls()));
        tvTotal.setText(formatFigureTwoPlaces(Constants.invoiceModel.getTotalFare()));
        tvParking.setText(formatFigureTwoPlaces(Constants.invoiceModel.getParking()));
        tvDueAmt.setText(formatFigureTwoPlaces(Constants.invoiceModel.getDue()));

    }

    public String formatFigureTwoPlaces(String value) {

        float val = Float.valueOf(value);
        DecimalFormat myFormatter = new DecimalFormat("##0.00");
        return myFormatter.format(val);
    }

    private void initialize() {

        back = (ImageButton) findViewById(R.id.back);
        tvTotalFare = (TextView) findViewById(R.id.tvTotalFare);
        tvtotalDistance = (TextView) findViewById(R.id.tvtotalDistance);
        tvFrom = (TextView) findViewById(R.id.tvFrom);
        tvTo = (TextView) findViewById(R.id.tvTo);

        tvTripFare = (TextView) findViewById(R.id.tvTripFare);
        tvTolls = (TextView) findViewById(R.id.tvTolls);
        tvRiderDiscount = (TextView) findViewById(R.id.tvRiderDiscount);
        tvOutstanding = (TextView) findViewById(R.id.tvOutstanding);
        tvParking = (TextView) findViewById(R.id.tvParking);
        tvDueAmt = (TextView) findViewById(R.id.tvDueAmt);
        tvTotal = (TextView) findViewById(R.id.tvTotal);

        btnShare = (Button) findViewById(R.id.btnShare);

        scroll = (ScrollView) findViewById(R.id.scroll);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId())
        {
            case R.id.back:
                finish();
                break;

            case R.id.btnShare:

                View view1 = getWindow().getDecorView().findViewById(android.R.id.content);

                Bitmap icon = getBitmapFromView(scroll,scroll.getChildAt(0).getHeight(),scroll.getChildAt(0).getWidth());

                Intent sharingIntent = new Intent(android.content.Intent.ACTION_SEND);
                sharingIntent.setType("image/*");

               // Bitmap icon = getScreenshot(view1);
                ByteArrayOutputStream bytes = new ByteArrayOutputStream();
                icon.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
                File f = new File(Environment.getExternalStorageDirectory() + File.separator + "temporary_file.jpg");

                try {
                    f.createNewFile();
                    FileOutputStream fo = new FileOutputStream(f);
                    fo.write(bytes.toByteArray());
                } catch (IOException e) {

                }

                sharingIntent.putExtra(Intent.EXTRA_STREAM, Uri.parse("file:///sdcard/temporary_file.jpg"));
                sharingIntent.putExtra(Intent.EXTRA_TEXT, "Invoice");
                startActivity(Intent.createChooser(sharingIntent, "Share via"));

                break;
        }
    }

    private Bitmap getScreenshot(View view) {
        view.setDrawingCacheEnabled(true);
        Bitmap bitmap = Bitmap.createBitmap(view.getDrawingCache());
        view.setDrawingCacheEnabled(false);
        return bitmap;
    }

    //create bitmap from the view
    private Bitmap getBitmapFromView(View view,int height,int width) {
        Bitmap bitmap = Bitmap.createBitmap(width, height,Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(bitmap);
        Drawable bgDrawable =view.getBackground();
        if (bgDrawable!=null)
            bgDrawable.draw(canvas);
        else
            canvas.drawColor(Color.WHITE);
        view.draw(canvas);
        return bitmap;
    }
}
