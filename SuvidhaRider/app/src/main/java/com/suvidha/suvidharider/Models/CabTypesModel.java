package com.suvidha.suvidharider.Models;

/**
 * Created by cybertruimph on 3/17/2018.
 */

public class CabTypesModel {

    int cabimage;
    String type, timetoreach;

    public CabTypesModel(String type, String timetoreach) {
        this.type = type;
        this.timetoreach = timetoreach;
    }

    public int getCabimage() {
        return cabimage;
    }

    public void setCabimage(int cabimage) {
        this.cabimage = cabimage;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getTimetoreach() {
        return timetoreach;
    }

    public void setTimetoreach(String timetoreach) {
        this.timetoreach = timetoreach;
    }
}
