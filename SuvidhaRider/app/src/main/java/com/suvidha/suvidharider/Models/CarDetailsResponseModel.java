package com.suvidha.suvidharider.Models;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class CarDetailsResponseModel {

    @SerializedName("success")
    @Expose
    private String success;
    @SerializedName("carlocation")
    @Expose
    private List<CarlocationModel> carlocation = null;
    @SerializedName("RateDetails")
    @Expose
    private List<RateDetail> rateDetails = null;
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("message")
    @Expose
    private String message;

    @SerializedName("DueDetails")
    @Expose
    private List<DueDetail> dueDetails = null;

    public String getSuccess() {
        return success;
    }

    public void setSuccess(String success) {
        this.success = success;
    }

    public List<CarlocationModel> getCarlocation() {
        return carlocation;
    }

    public void setCarlocation(List<CarlocationModel> carlocation) {
        this.carlocation = carlocation;
    }

    public List<RateDetail> getRateDetails() {
        return rateDetails;
    }

    public void setRateDetails(List<RateDetail> rateDetails) {
        this.rateDetails = rateDetails;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<DueDetail> getDueDetails() {
        return dueDetails;
    }

    public void setDueDetails(List<DueDetail> dueDetails) {
        this.dueDetails = dueDetails;
    }
}