package com.suvidha.suvidharider;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlaceAutocomplete;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.model.MarkerOptions;
import com.seatgeek.placesautocomplete.OnPlaceSelectedListener;
import com.seatgeek.placesautocomplete.PlacesAutocompleteTextView;
import com.suvidha.suvidharider.Utilities.Camera_Gallery;
import com.suvidha.suvidharider.Utilities.Constants;
import com.suvidha.suvidharider.Utilities.InternetCheck;
import com.suvidha.suvidharider.Utilities.Validation;

public class FromToActivity extends AppCompatActivity  {

    private static final String TAG = "latlong";
    private static final int PLACE_AUTOCOMPLETE_FROM = 1;
    private static final int PLACE_AUTOCOMPLETE_TO = 2;
    //private TextView tvFromLocation, tvToLocation;
    public static String picloc="";

    PlacesAutocompleteTextView tvFromLocation, tvToLocation;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_from_to);

        initialize();

        tvFromLocation.setOnPlaceSelectedListener(new OnPlaceSelectedListener() {
            @Override
            public void onPlaceSelected(@NonNull com.seatgeek.placesautocomplete.model.Place place) {
                tvFromLocation.setText(place.description.toString());

            }
        });

        tvToLocation.setOnPlaceSelectedListener(new OnPlaceSelectedListener() {
            @Override
            public void onPlaceSelected(@NonNull com.seatgeek.placesautocomplete.model.Place place) {
                tvToLocation.setText(place.description.toString());

                Constants.PICKUPLOCATION = place.description.toString();
                Intent intent = new Intent();
                intent.putExtra("FromLocation", place.description.toString());
                setResult(2, intent);
                finish();
            }
        });

    }

    private void initialize() {

        tvFromLocation = (PlacesAutocompleteTextView) findViewById(R.id.tvFromLocation);
        tvToLocation = (PlacesAutocompleteTextView) findViewById(R.id.tvToLocation);
    }

}
