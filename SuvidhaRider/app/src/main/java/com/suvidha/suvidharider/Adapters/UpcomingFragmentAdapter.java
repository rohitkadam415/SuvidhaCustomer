package com.suvidha.suvidharider.Adapters;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.suvidha.suvidharider.Models.MyBookingModel;
import com.suvidha.suvidharider.R;

import java.util.ArrayList;

/**
 * Created by cybertruimph on 3/19/2018.
 */

public class UpcomingFragmentAdapter extends RecyclerView.Adapter<UpcomingFragmentAdapter.MyCredits> {

    private ArrayList<MyBookingModel> pList;
    private View itemView;

    public UpcomingFragmentAdapter(ArrayList<MyBookingModel> pList) {
        this.pList = pList;
    }

    public class MyCredits extends RecyclerView.ViewHolder {

      //  private final TextView tvName, tvCarName, tvLocation, tvResult;

        public MyCredits(View itemView) {
            super(itemView);
//            tvName = (TextView) itemView.findViewById(R.id.tvName);
//            tvCarName = (TextView) itemView.findViewById(R.id.tvCarName);
//            tvLocation = (TextView) itemView.findViewById(R.id.tvLocation);
//            tvResult = (TextView) itemView.findViewById(R.id.tvResult);
        }
    }

    @Override
    public UpcomingFragmentAdapter.MyCredits onCreateViewHolder(ViewGroup parent, int viewType) {
        itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.custom_recycler_upcoming, parent, false);
        return new UpcomingFragmentAdapter.MyCredits(itemView);
    }

    @Override
    public void onBindViewHolder(UpcomingFragmentAdapter.MyCredits holder, int position) {
//        holder.tvName.setText(pList.get(position).getName());
//        holder.tvCarName.setText(pList.get(position).getCarname());
//        holder.tvLocation.setText(pList.get(position).getLoction());
//        holder.tvResult.setText(pList.get(position).getResult());

    }

    @Override
    public int getItemCount() {
        return pList.size();
    }


}