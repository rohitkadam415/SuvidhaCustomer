package com.suvidha.suvidharider.Models;
import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class TripDetails {

    @SerializedName("success")
    @Expose
    private String success;
    @SerializedName("CompleteRideDetails")
    @Expose
    private List<CompleteRideDetail> completeRideDetails = null;
    @SerializedName("InCompleteRideDetails")
    @Expose
    private List<InCompleteRideDetail> inCompleteRideDetails = null;
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("message")
    @Expose
    private String message;

    public String getSuccess() {
        return success;
    }

    public void setSuccess(String success) {
        this.success = success;
    }

    public List<CompleteRideDetail> getCompleteRideDetails() {
        return completeRideDetails;
    }

    public void setCompleteRideDetails(List<CompleteRideDetail> completeRideDetails) {
        this.completeRideDetails = completeRideDetails;
    }

    public List<InCompleteRideDetail> getInCompleteRideDetails() {
        return inCompleteRideDetails;
    }

    public void setInCompleteRideDetails(List<InCompleteRideDetail> inCompleteRideDetails) {
        this.inCompleteRideDetails = inCompleteRideDetails;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

}