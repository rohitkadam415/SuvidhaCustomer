package com.suvidha.suvidharider;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.widget.RecyclerView;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.gson.Gson;
import com.makeramen.roundedimageview.RoundedImageView;
import com.suvidha.suvidharider.Api.BaseClass;
import com.suvidha.suvidharider.Interface.GetRequestResponse;
import com.suvidha.suvidharider.Models.BookingResponseModel;
import com.suvidha.suvidharider.Models.ConfirmBookingModel;
import com.suvidha.suvidharider.Models.DriverModel;
import com.suvidha.suvidharider.Models.ReasonsModel;
import com.suvidha.suvidharider.Utilities.Camera_Gallery;
import com.suvidha.suvidharider.Utilities.Constants;
import com.suvidha.suvidharider.Utilities.SharedPreference;

public class AppClosingActivity extends BaseClass implements View.OnClickListener, OnMapReadyCallback {

    ImageButton backImageButton;
    private RoundedImageView imDriverPhoto;
    private TextView tvDriverName, tvVehicleType, tvDriverLocation, tvRide;
    private RatingBar ratingBar;
    private FloatingActionButton fabCall, fabMyLocation;
    private GoogleMap mMap;
    private CameraPosition cameraPosition;
    private ProgressDialog dialog, progressDialog;
    private LinearLayout ll;
    private ConfirmBookingModel confirmBookingModel;
    private LatLng newDriverLoc;
    private Boolean isRide = false;
    private LatLng to;
    private LatLng driverLoc;
    private LatLng from;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_app_closing);

        initialize();

        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentByTag("mapFragment");
        if (mapFragment == null) {
            mapFragment = new SupportMapFragment();
            FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
            ft.add(R.id.mapFragmentContainer, mapFragment, "mapFragment");
            ft.commit();
            getSupportFragmentManager().executePendingTransactions();
        }
        mapFragment.getMapAsync(this);

        backImageButton.setOnClickListener(this);
        fabCall.setOnClickListener(this);
        fabMyLocation.setOnClickListener(this);

        to = new LatLng(Double.valueOf(Constants.TO_LATTITUDE), Double.valueOf(Constants.TO_LONGITUDE));

        IntentFilter intentFilter = new IntentFilter("completeRide");
        LocalBroadcastManager.getInstance(getBaseContext()).registerReceiver(onNotice, intentFilter);

    }

    private void bindDriverData() {

        if (Constants.bookingResponseModel.getDriverDetailsModels().get(0).getProfile() != null) {
            Glide.with(getApplicationContext())
                    .load(Constants.bookingResponseModel.getDriverDetailsModels().get(0).getProfile())
                    .into(imDriverPhoto);
        }
        String drivername = Constants.bookingResponseModel.getDriverDetailsModels().get(0).getFirstname() + " " + Constants.bookingResponseModel.getDriverDetailsModels().get(0).getLastname();
        tvDriverName.setText(drivername);
        tvVehicleType.setText(Constants.bookingResponseModel.getDriverDetailsModels().get(0).getVehicleName());
        ratingBar.setRating(Float.valueOf(Constants.bookingResponseModel.getDriverDetailsModels().get(0).getRating()));
        tvDriverLocation.setText(Camera_Gallery.getAddressFromLocation(getApplicationContext(), Double.valueOf(Constants.bookingResponseModel.getDriverDetailsModels().get(0).getLatitude()), Double.valueOf(Constants.bookingResponseModel.getDriverDetailsModels().get(0).getLongitude())));
    }

    private void initialize() {
        backImageButton = (ImageButton) findViewById(R.id.back);
        imDriverPhoto = (RoundedImageView) findViewById(R.id.imDriverPhoto);
        tvDriverName = (TextView) findViewById(R.id.tvDriverName);
        tvVehicleType = (TextView) findViewById(R.id.tvVehicleType);
        tvDriverLocation = (TextView) findViewById(R.id.tvDriverLocation);
        tvRide = (TextView) findViewById(R.id.tvRide);
        ratingBar = (RatingBar) findViewById(R.id.ratingBar);
        fabCall = (FloatingActionButton) findViewById(R.id.fabCall);
        fabMyLocation = (FloatingActionButton) findViewById(R.id.fabMyLocation);
    }

    @SuppressLint("MissingPermission")
    @Override
    public void onClick(View view) {
        switch (view.getId()) {

            case R.id.back:
                finish();
                break;

            case R.id.fabCall:
                Intent intent = new Intent(Intent.ACTION_CALL);
                intent.setData(Uri.parse("tel:" + Constants.bookingResponseModel.getDriverDetailsModels().get(0).getMobile()));
                startActivity(intent);
                break;

            case R.id.fabMyLocation:
                String currentLatitude = SharedPreference.getString(getApplicationContext(), Constants.LATITUDE);
                String currentLongitude = SharedPreference.getString(getApplicationContext(), Constants.LONGITUDE);
                cameraPosition = new CameraPosition.Builder().target(new LatLng(Double.valueOf(currentLatitude), Double.valueOf(currentLongitude))).zoom(17.0f).build();
                mMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));

                break;
        }
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
    }

    //Convert a marker view to bitmap
    public Bitmap createDrawableFromView(Context context, View view) {
        DisplayMetrics displayMetrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        view.setLayoutParams(new WindowManager.LayoutParams(WindowManager.LayoutParams.WRAP_CONTENT, WindowManager.LayoutParams.WRAP_CONTENT));
        view.measure(displayMetrics.widthPixels, displayMetrics.heightPixels);
        view.layout(0, 0, displayMetrics.widthPixels, displayMetrics.heightPixels);
        view.buildDrawingCache();
        Bitmap bitmap = Bitmap.createBitmap(view.getMeasuredWidth(), view.getMeasuredHeight(), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(bitmap);
        view.draw(canvas);
        return bitmap;
    }


    @Override
    protected void apiConnected() {
        progressDialog = new ProgressDialog(AppClosingActivity.this);
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDialog.setMessage("Loading...");
        progressDialog.setCancelable(false);
        progressDialog.show();

        confirmBookingModel = new ConfirmBookingModel();
        confirmBookingModel.setRequestId(Constants.bookingResponseModel.getDriverDetailsModels().get(0).getRequestId());
        confirmBookingModel.setUserId(SharedPreference.getString(getApplicationContext(), Constants.USER_ID));

        getDriverDetails();
    }

    public void getDriverDetails() {
        if (apiConnection != null) {
            apiConnection.confirmBooking(confirmBookingModel, new GetRequestResponse() {

                @Override
                public void onResponse(String json) {
                    Gson gson = new Gson();
                    final BookingResponseModel bookingResponseModel = gson.fromJson(json, BookingResponseModel.class);

                    if (bookingResponseModel.getSuccess().equals("true")) {
                        progressDialog.dismiss();
                        Constants.bookingResponseModel = bookingResponseModel;
                        bindDriverData();

                        final View marker = ((LayoutInflater) getBaseContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.map_marker, null);
                        final TextView tvMinutes = (TextView) marker.findViewById(R.id.tvMinutes);

                        String minute = getIntent().getStringExtra("reachtime");
                        String[] digitMinute = minute.split(" ");
                        tvMinutes.setText(digitMinute[0] + "\n" + "MIN");

                        from = new LatLng(Double.valueOf(Constants.FROM_LATTITUDE), Double.valueOf(Constants.FROM_LONGITUDE));
                        driverLoc = new LatLng(Double.valueOf(Constants.bookingResponseModel.getDriverDetailsModels().get(0).getLatitude()), Double.valueOf(Constants.bookingResponseModel.getDriverDetailsModels().get(0).getLongitude()));

                        driverVehicleMarker(driverLoc);

                        mMap.addMarker(new MarkerOptions().position(from).icon(BitmapDescriptorFactory.fromBitmap(createDrawableFromView(getBaseContext(), marker))));
                        movecamera(from, driverLoc);

                        ReasonsModel reasonsModel = new ReasonsModel();
                        reasonsModel.setDriverId(bookingResponseModel.getDriverDetailsModels().get(0).getDriverId());
                        reasonsModel.setUserLatitude(SharedPreference.getString(getBaseContext(), Constants.LATITUDE));
                        reasonsModel.setUserLongitude(SharedPreference.getString(getBaseContext(), Constants.LONGITUDE));
                        apiConnection.getDriverLatLang(reasonsModel, new GetRequestResponse() {
                            @Override
                            public void onResponse(String json) {
                                Gson gson = new Gson();
                                final DriverModel driverModel = gson.fromJson(json, DriverModel.class);
                                mMap.clear();

                                String minute = driverModel.getReachtime();
                                String[] digitMinute = minute.split(" ");
                                tvMinutes.setText(digitMinute[0] + "\n" + "MIN");

                                newDriverLoc = new LatLng(Double.valueOf(driverModel.getFromlatitude()), Double.valueOf(driverModel.getFromlongitude()));
                                driverVehicleMarker(newDriverLoc);

                                if (!isRide) {

                                    mMap.addMarker(new MarkerOptions().position(from).icon(BitmapDescriptorFactory.fromBitmap(createDrawableFromView(getBaseContext(), marker))));
                                    movecamera(newDriverLoc, from);
                                } else {

                                    mMap.addMarker(new MarkerOptions().position(to).icon(BitmapDescriptorFactory.fromResource(R.drawable.marker_40)));
                                    movecamera(newDriverLoc, to);
                                }
                            }

                            @Override
                            public void onFailure(String json) {

                            }
                        });
                    }
                }

                @Override
                public void onFailure(String json) {
                }
            });
        }
    }

    private void driverVehicleMarker(LatLng latLng) {

        if (Constants.SERVICE.equals("Cars")) {
            mMap.addMarker(new MarkerOptions().position(latLng).icon(BitmapDescriptorFactory.fromResource(R.drawable.mini)));
        }
        if (Constants.SERVICE.equals("ERikshaw")) {
            mMap.addMarker(new MarkerOptions().position(latLng).icon(BitmapDescriptorFactory.fromResource(R.drawable.micro)));
        }
        if (Constants.SERVICE.equals("Auto")) {
            mMap.addMarker(new MarkerOptions().position(latLng).icon(BitmapDescriptorFactory.fromResource(R.drawable.sedan)));
        }
    }

    private BroadcastReceiver onNotice = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {

            String driver_id = intent.getStringExtra("DriverId");
            String request_id = intent.getStringExtra("RequestId");
            openDialog(driver_id, request_id);
        }
    };


    public void movecamera(LatLng latLng1, LatLng latLng2) {
        LatLngBounds.Builder builder = new LatLngBounds.Builder();
        builder.include(latLng1);
        builder.include(latLng2);
        final CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngBounds(builder.build(), 100);

        mMap.setOnMapLoadedCallback(new GoogleMap.OnMapLoadedCallback() {
            @Override
            public void onMapLoaded() {
                mMap.moveCamera(cameraUpdate);
            }
        });
    }

    private void openDialog(final String driverid, final String requestid) {

        final Dialog dialogRequestCancel = new Dialog(AppClosingActivity.this);
        dialogRequestCancel.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialogRequestCancel.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialogRequestCancel.setContentView(R.layout.dialog_booking_cancel);
        dialogRequestCancel.setCancelable(false);

        TextView DoneTextView = (TextView) dialogRequestCancel.findViewById(R.id.done);
        DoneTextView.setText("OK");
        TextView cancel = (TextView) dialogRequestCancel.findViewById(R.id.cancel);
        cancel.setVisibility(View.GONE);
        TextView tvMessage = (TextView) dialogRequestCancel.findViewById(R.id.tvMessage);
        tvMessage.setText("Your ride is completed.\nThank you");

        TextView tvTitle = (TextView) dialogRequestCancel.findViewById(R.id.tvTitle);
        tvTitle.setText("Ride Completed");

        ImageView imOk = (ImageView) dialogRequestCancel.findViewById(R.id.imOk);
        imOk.setImageResource(R.drawable.booking_successful_70);

        DoneTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent intent = new Intent(AppClosingActivity.this, FareBreakdownActivity.class);
                intent.putExtra("DriverId", driverid);
                intent.putExtra("RequestId", requestid);
                startActivity(intent);
            }
        });

        dialogRequestCancel.show();
    }


    @Override
    protected void onResume() {
        super.onResume();
        MyApplication.setStart();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        MyApplication.setStop();
    }
}
