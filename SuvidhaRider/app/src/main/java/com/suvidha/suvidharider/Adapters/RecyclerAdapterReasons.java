package com.suvidha.suvidharider.Adapters;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.suvidha.suvidharider.Models.ReasonsModel;
import com.suvidha.suvidharider.R;
import com.suvidha.suvidharider.Utilities.Constants;

import java.util.ArrayList;

/**
 * Created by cybertruimph on 3/14/2018.
 */

public class RecyclerAdapterReasons extends RecyclerView.Adapter<RecyclerAdapterReasons.MyCredits> {

    private ArrayList<ReasonsModel> pList;
    private  int selectedPosition = -1;
    public RecyclerAdapterReasons(ArrayList<ReasonsModel> pList) {
        this.pList = pList;
    }

    public class MyCredits extends RecyclerView.ViewHolder {

        private final TextView product_name;
        private final ImageView im;

        public MyCredits(View itemView) {
            super(itemView);
            product_name = (TextView) itemView.findViewById(R.id.product_name);
            im = (ImageView) itemView.findViewById(R.id.im);
            im.setVisibility(View.INVISIBLE);
        }
    }

    @Override
    public RecyclerAdapterReasons.MyCredits onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.custom_reason_list, parent, false);
        return new RecyclerAdapterReasons.MyCredits(itemView);
    }

    @Override
    public void onBindViewHolder(final RecyclerAdapterReasons.MyCredits holder, int position) {
        if (position == selectedPosition)
        {
            holder.im.setVisibility(View.VISIBLE);
        }
        else
        {
            holder.im.setVisibility(View.GONE);
        }
        holder.product_name.setText(pList.get(position).getReason());

        holder.product_name.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                selectedPosition = holder.getAdapterPosition();
                holder.im.setVisibility(View.VISIBLE);

                Constants.REASON = pList.get(selectedPosition).getReason().toString();

                notifyDataSetChanged();
            }
        });

    }

    @Override
    public int getItemCount() {
        return pList.size();
    }


}
