package com.suvidha.suvidharider.Models;

/**
 * Created by kedar on 3/6/2018.
 */


import com.google.gson.JsonObject;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class CarlocationModel {

    @SerializedName("Latitude")
    @Expose
    private String latitude;
    @SerializedName("Longitude")
    @Expose
    private String longitude;
    @SerializedName("Distance")
    @Expose
    private Float distance;
    @SerializedName("UserId")
    @Expose
    private String userId;
    @SerializedName("Service")
    @Expose
    private String service;
    @SerializedName("TimeToreach")
    @Expose
    private String timeToreach;

//    @SerializedName("ServiceId")
//    @Expose
//    private String serviceId;


    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public Float getDistance() {
        return distance;
    }

    public void setDistance(Float distance) {
        this.distance = distance;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getService() {
        return service;
    }

    public void setService(String service) {
        this.service = service;
    }

    public String getTimeToreach() {
        return timeToreach;
    }

    public void setTimeToreach(String timeToreach) {
        this.timeToreach = timeToreach;
    }

//    public String getServiceId() {
//        return serviceId;
//    }
//
//    public void setServiceId(String serviceId) {
//        this.serviceId = serviceId;
//    }
}
