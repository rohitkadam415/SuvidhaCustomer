package com.suvidha.suvidharider;

import android.app.ProgressDialog;
import android.content.Intent;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.rengwuxian.materialedittext.MaterialEditText;
import com.suvidha.suvidharider.Api.BaseClass;
import com.suvidha.suvidharider.Interface.GetRequestResponse;
import com.suvidha.suvidharider.Interface.RetrofitClient;
import com.suvidha.suvidharider.Interface.RetrofitInterface;
import com.suvidha.suvidharider.Models.CarDetailsResponseModel;
import com.suvidha.suvidharider.Models.SigninModel;
import com.suvidha.suvidharider.Models.SigninResponseModel;
import com.suvidha.suvidharider.Utilities.Camera_Gallery;
import com.suvidha.suvidharider.Utilities.Constants;
import com.suvidha.suvidharider.Utilities.InternetCheck;
import com.suvidha.suvidharider.Utilities.SharedPreference;
import com.suvidha.suvidharider.Utilities.Validation;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoginActivity extends BaseClass implements View.OnClickListener {

    private Button btnForgotPassword, btnSignin;
    private ImageButton back;
    private MaterialEditText input_mobile, input_password;
    private String mobile;
    private String password;
    private SigninModel signinModel;
    private ProgressDialog dialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        initialize();
    }

    private void initialize() {

        btnForgotPassword = (Button) findViewById(R.id.btnForgot);
        btnSignin = (Button) findViewById(R.id.btnSignin);
        back = (ImageButton) findViewById(R.id.back);

        input_mobile = (MaterialEditText) findViewById(R.id.input_mobile);
        input_password = (MaterialEditText) findViewById(R.id.input_password);

        back.setOnClickListener(this);
        btnSignin.setOnClickListener(this);
        btnForgotPassword.setOnClickListener(this);

    }

    @Override
    public void onClick(View view) {
        switch (view.getId())
        {
            case R.id.back:
                finish();
                break;

            case R.id.btnSignin:

                if ( InternetCheck.getConnectivityStatus(getApplicationContext())) {

                    if (validation()) {
                        mobile = input_mobile.getText().toString();
                        password = input_password.getText().toString();
                        signinModel = new SigninModel();
                        signinModel.setMobile(mobile);
                        signinModel.setPassword(password);
                        signinModel.setFcm_token(SharedPreference.getString(getBaseContext(), Constants.FCM_TOKEN));
                        signin();
                    }
                }
                else
                {
                    Camera_Gallery.openSnackBar(btnSignin, getResources().getString(R.string.nointernet), getApplicationContext());
                }
                break;

            case R.id.btnForgot:
                startActivity(new Intent(LoginActivity.this, ForgotPassword.class));
                break;
        }
    }

    private void signin() {

        dialog = new ProgressDialog(LoginActivity.this);
        dialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        dialog.setMessage("Loading...");
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();

        if(apiConnection!=null) {
            apiConnection.checkLogin(signinModel, new GetRequestResponse() {
                @Override
                public void onResponse(String json) {
                    final Gson gson=new Gson();
                    final SigninResponseModel signinResponseModel=gson.fromJson(json,SigninResponseModel.class);

                    SharedPreference.saveString(getApplicationContext(), Constants.FIRSTNAME,  signinResponseModel.getUserDetails().get(0).getFirstname());
                    SharedPreference.saveString(getApplicationContext(), Constants.LASTNAME,  signinResponseModel.getUserDetails().get(0).getLastname());
                    SharedPreference.saveString(getApplicationContext(), Constants.EMAIL,  signinResponseModel.getUserDetails().get(0).getEmail());
                    SharedPreference.saveString(getApplicationContext(), Constants.USER_ID,  signinResponseModel.getUserDetails().get(0).getUserId());
                    SharedPreference.saveString(getApplicationContext(), Constants.PROFILE,  signinResponseModel.getUserDetails().get(0).getProfile());
                    SharedPreference.saveString(getApplicationContext(), Constants.COUNTRY_CODE,  signinResponseModel.getUserDetails().get(0).getCountryCode());
                    SharedPreference.saveString(getApplicationContext(), Constants.MOBILE,  signinResponseModel.getUserDetails().get(0).getMobile());
                    SharedPreference.saveString(getApplicationContext(), Constants.PASSWORD,  input_password.getText().toString());
                    SharedPreference.saveString(getApplicationContext(), Constants.REFER_CODE, signinResponseModel.getUserDetails().get(0).getUserId());

                    MyApplication.getMyApplicationInstance().setMyUserId();
                    if (signinResponseModel.getHomelocation() != null)
                    {
                        SharedPreference.saveString(getApplicationContext(), Constants.HOMEPLACENAME,  signinResponseModel.getHomelocation().get(0).getLocation());
                        SharedPreference.saveString(getApplicationContext(), Constants.HOMELOCATION, signinResponseModel.getHomelocation().get(0).getSubLocation());
                        SharedPreference.saveString(getApplicationContext(), Constants.HOMELATITUDE,  signinResponseModel.getHomelocation().get(0).getLatitude());
                        SharedPreference.saveString(getApplicationContext(), Constants.HOMELONGITUDE,  signinResponseModel.getHomelocation().get(0).getLongitude());

                    }
                    if (signinResponseModel.getWorklocation() != null)
                    {
                        SharedPreference.saveString(getApplicationContext(), Constants.WORKPLACENAME,  signinResponseModel.getWorklocation().get(0).getLocation());
                        SharedPreference.saveString(getApplicationContext(), Constants.WORKLOCATION,  signinResponseModel.getWorklocation().get(0).getSubLocation());
                        SharedPreference.saveString(getApplicationContext(), Constants.WORKLATITUDE,  signinResponseModel.getWorklocation().get(0).getLatitude());
                        SharedPreference.saveString(getApplicationContext(), Constants.WORKLONGITUDE,  signinResponseModel.getWorklocation().get(0).getLongitude());
                    }

                    Intent intent=new Intent(LoginActivity.this, HomeActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(intent);
                    dialog.cancel();
                }

                @Override
                public void onFailure(String msg) {
                    final Gson gson=new Gson();
                    final SigninResponseModel signinResponseModel=gson.fromJson(msg,SigninResponseModel.class);

                    Camera_Gallery.openSnackBar(btnSignin, signinResponseModel.getMessage(), getApplicationContext());
                    dialog.cancel();
                }
            });
        }

    }

    public boolean validation() {

        if (Validation.isValidContact(input_mobile, "Please enter mobile") && Validation.isPassword(input_password, "Password must be more than 6 digit")) {
            return true;
        }
        return false;

    }


    @Override
    protected void apiConnected() {
        super.apiConnected();

    }
}
