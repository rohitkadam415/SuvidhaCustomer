package com.suvidha.suvidharider.Models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by cybertruimph on 3/14/2018.
 */

public class ReasonsModel {

    @SerializedName("UserId")
    @Expose
    private String UserId;

    @SerializedName("DriverId")
    @Expose
    private String driverId;

    @SerializedName("RequestId")
    @Expose
    private String requestId;

    @SerializedName("Reason")
    @Expose
    private String reason;

    @SerializedName("UserLatitude")
    @Expose
    private String userLatitude;
    @SerializedName("UserLongitude")
    @Expose
    private String userLongitude;

    public ReasonsModel() {
    }

    public ReasonsModel(String reason) {
        this.reason = reason;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    public String getUserId() {
        return UserId;
    }

    public void setUserId(String userId) {
        UserId = userId;
    }

    public String getDriverId() {
        return driverId;
    }

    public void setDriverId(String driverId) {
        this.driverId = driverId;
    }

    public String getRequestId() {
        return requestId;
    }

    public void setRequestId(String requestId) {
        this.requestId = requestId;
    }

    public String getUserLatitude() {
        return userLatitude;
    }

    public void setUserLatitude(String userLatitude) {
        this.userLatitude = userLatitude;
    }

    public String getUserLongitude() {
        return userLongitude;
    }

    public void setUserLongitude(String userLongitude) {
        this.userLongitude = userLongitude;
    }
}
