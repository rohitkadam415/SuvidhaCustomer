package com.suvidha.suvidharider.Models;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class LastDriverDetails {

    @SerializedName("success")
    @Expose
    private String success;
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("RatingRideDetails")
    @Expose
    private List<RatingRideDetail> ratingRideDetails = null;
    @SerializedName("message")
    @Expose
    private String message;

    public String getSuccess() {
        return success;
    }

    public void setSuccess(String success) {
        this.success = success;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public List<RatingRideDetail> getRatingRideDetails() {
        return ratingRideDetails;
    }

    public void setRatingRideDetails(List<RatingRideDetail> ratingRideDetails) {
        this.ratingRideDetails = ratingRideDetails;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

}