package com.suvidha.suvidharider.Interface;


import com.suvidha.suvidharider.Models.BookingModel;
import com.suvidha.suvidharider.Models.BookingResponseModel;
import com.suvidha.suvidharider.Models.CarDetailsResponseModel;
import com.suvidha.suvidharider.Models.ChangeMobilePassword;
import com.suvidha.suvidharider.Models.ConfirmBookingModel;
import com.suvidha.suvidharider.Models.DistanceModel;
import com.suvidha.suvidharider.Models.DriverModel;
import com.suvidha.suvidharider.Models.Homelocation;
import com.suvidha.suvidharider.Models.LastDriverDetails;
import com.suvidha.suvidharider.Models.NotificationModel;
import com.suvidha.suvidharider.Models.RatingModel;
import com.suvidha.suvidharider.Models.ReasonsModel;
import com.suvidha.suvidharider.Models.RegisterModel;
import com.suvidha.suvidharider.Models.RegisterResponseModel;
import com.suvidha.suvidharider.Models.SigninModel;
import com.suvidha.suvidharider.Models.SigninResponseModel;
import com.suvidha.suvidharider.Models.TripDetails;
import com.suvidha.suvidharider.Models.UpdateLocationModel;
import com.suvidha.suvidharider.Models.UpdateProfileModel;
import com.suvidha.suvidharider.POJO.Example;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Query;


/**
 * Created by kedar on 12/25/2017.
 */

public interface RetrofitInterface {


    @POST("send_otp.php")
    Call<RegisterResponseModel> sendOtp(@Body RegisterModel registerModel);

    @POST("customer_signup.php")
    Call<RegisterResponseModel> saveUserData(@Body RegisterModel registerModel);

    @POST("customer_signin.php")
    Call<SigninResponseModel> checkLoginCredentials(@Body SigninModel signinModel);

    @FormUrlEncoded
    @POST("forget_password.php")
    Call<RegisterResponseModel> forgetpassword(@Field("Email") String Email);

    @POST("customer_update_profile.php")
    Call<RegisterResponseModel> updateProfile(@Body UpdateProfileModel updateProfileModel);

    @POST("customer_update_location.php")
    Call<RegisterResponseModel> updateMapLocations(@Body UpdateLocationModel updateLocationModel);

    @POST("change_number.php")
    Call<RegisterResponseModel> updatemobile(@Body ChangeMobilePassword changeMobilePassword);

    @POST("change_password.php")
    Call<RegisterResponseModel> change_password(@Body ChangeMobilePassword changeMobilePassword);


    @POST("get_all_car_location1.php")
    Call<CarDetailsResponseModel> getcardetails(@Body Homelocation homelocation);

    @GET("api/directions/json?key=AIzaSyD5Ssb9-3EKkNzOYvv9BCiShCyo0EcgqDk")
    Call<Example> getDistanceDuration(@Query("units") String units, @Query("origin") String origin, @Query("destination") String destination, @Query("mode") String mode);

    @POST("confirm_booking.php")
    Call<BookingResponseModel> saveBookingData(@Body BookingModel bookingModel);

    @POST("cancelrequest.php")
    Call<RegisterResponseModel> cancelBokking(@Body ReasonsModel reasonsModel);

    @POST("Get_Driver_Details.php")
    Call<BookingResponseModel> saveConfirmBookingData(@Body ConfirmBookingModel confirmBookingModel);

    @POST("Get_Driver_Detailsss.php")
    Call<RegisterResponseModel> savePaymentType(@Body DistanceModel distanceModel);

    @POST("completedtrips.php")
    Call<TripDetails> getBooking(@Body BookingModel bookingModel);

    @POST("get_driver_location.php")
    Call<DriverModel> getDriverPosition(@Body ReasonsModel reasonsModel);

    @POST("InsertRating.php")
    Call<DriverModel> saveRatingDetails(@Body RatingModel ratingModel);

    @POST("DriverRating.php")
    Call<LastDriverDetails> checkRatingDetails(@Body RatingModel ratingModel);

    @POST("InvoiceGenerated.php")
    Call<TripDetails> getInvoice(@Body RatingModel ratingModel);

    @POST("GetNotification.php")
    Call<NotificationModel> getNotifications(@Body RatingModel ratingModel);
}
