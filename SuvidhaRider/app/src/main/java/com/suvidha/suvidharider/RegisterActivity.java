package com.suvidha.suvidharider;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.telephony.TelephonyManager;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.rengwuxian.materialedittext.MaterialEditText;
import com.suvidha.suvidharider.Utilities.Camera_Gallery;
import com.suvidha.suvidharider.Utilities.Constants;
import com.suvidha.suvidharider.Utilities.InternetCheck;
import com.suvidha.suvidharider.Utilities.SharedPreference;
import com.suvidha.suvidharider.Utilities.Validation;

import java.io.File;

public class RegisterActivity extends AppCompatActivity implements View.OnClickListener {

    public static final int RequestPermissionCode = 1;
    SharedPreference sharedPreference;
    String profileBase64String = "";
    private TextView continueReg;
    private MaterialEditText input_name_first, input_name_last, input_email, input_country_code, input_mobile, input_password, input_referCode;
    private ImageView imageProfile;
    private String userChoosenTask;
    private File file;
    private ListView lv;
    ArrayAdapter<String> adapter;
    EditText inputSearch;
    private String[] countrynames;
    private String[] countycodes;
    private String[] blank;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        initialize();

        countrynames = new String[]{"Afghanistan", "Albania", "Algeria", "American Samoa", "Andorra", "Antarctica", "Argentina", "Australia", "Bangladesh", "Bermuda", "Brazil", "Belgium", "Canada", "China", "Colombia", "Costa Rica", "Egypt", "Finland", "France", "Germany", "Greece", "Greenland", "Hong Kong", "Iceland", "India", "Indonesia", "Iran", "Ireland", "Italy", "Japan", "Kenya", "Korea (North)", "Korea (South)", "Kuwait", "Malaysia", "Maldives", "Mauritius", "Mexico", "Myanmar", "Nepal", "Netherlands", "New Zealand", "Nigeria", "Pakistan", "Philippines", "Poland", "Portugal", "Russia", "Singapore", "South Africa", "Spain", "Sri Lanka", "Sweden", "Taiwan", "Thailand", "United Kingdom", "USA", "Zimbabwe"};
        countycodes = new String[]{"+93", "+355", "+213", "+1-684", "+376", "+672", "+54", "+61", "+880", "+1-441", "+55", "+32", "+1", "+86", "+57", "+506", "+20", "+358", "+33", "+49", "+30", "+299", "+852", "+354", "+91", "+62", "+98", "+353", "+39", "+81", "+254", "+850", "+82", "+965", "+60", "+960", "+230", "+52", "+95", "+977", "+31", "+64", "+234", "+92", "+63", "+48", "+351", "+7", "+65", "+27", "+34", "+94", "+46", "+886", "+66", "+44", "+1", "+263"};

        String countrycode = getCountryDialCode();
        if (countrycode.equals("") && countrycode == null) {
            input_country_code.setText("--");
        } else {
            input_country_code.setText("+"+countrycode);
        }

        final String products[] = getResources().getStringArray(R.array.DialingCountryCode);
        blank = countrynames;
    }

    public String getCountryDialCode() {
        String contryId = null;
        String contryDialCode = null;

        TelephonyManager telephonyMngr = (TelephonyManager) this.getSystemService(Context.TELEPHONY_SERVICE);
        contryId = telephonyMngr.getSimCountryIso().toUpperCase();

        String[] arrContryCode = RegisterActivity.this.getResources().getStringArray(R.array.DialingCountryCode);
        for (int i = 0; i < arrContryCode.length; i++) {
            String[] arrDial = arrContryCode[i].split(",");
            if (arrDial[1].trim().equals(contryId.trim())) {
                contryDialCode = arrDial[0];
                // country = arrDial[1];
                break;
            }
        }
        return contryDialCode;

    }

    private void initialize() {

        continueReg = (TextView) findViewById(R.id.continueReg);
        ImageButton back = (ImageButton) findViewById(R.id.back);
        input_name_first = (MaterialEditText) findViewById(R.id.input_name_first);
        input_name_last = (MaterialEditText) findViewById(R.id.input_name_last);
        input_email = (MaterialEditText) findViewById(R.id.input_email);
        input_country_code = (MaterialEditText) findViewById(R.id.input_country_code);
        input_mobile = (MaterialEditText) findViewById(R.id.input_mobile);
        input_password = (MaterialEditText) findViewById(R.id.input_password);
        input_referCode = (MaterialEditText) findViewById(R.id.input_referCode);

        back.setOnClickListener(this);
        continueReg.setOnClickListener(this);
        input_country_code.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.back:
                finish();
                break;

            case R.id.input_country_code:

                final Dialog dialog = new Dialog(RegisterActivity.this);
                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                dialog.setContentView(R.layout.list_countries);

                lv = (ListView) dialog.findViewById(R.id.list_view);
                inputSearch = (EditText) dialog.findViewById(R.id.inputSearch);
                adapter = new ArrayAdapter<>(getBaseContext(), R.layout.list_item, R.id.product_name, countrynames);
                lv.setAdapter(adapter);

                inputSearch.addTextChangedListener(new TextWatcher() {
                    @Override
                    public void onTextChanged(CharSequence cs, int arg1, int arg2, int arg3) {
                        // When user changed the Text
                        adapter.getFilter().filter(cs);

                    }

                    @Override
                    public void afterTextChanged(Editable editable) {

                    }

                    @Override
                    public void beforeTextChanged(CharSequence arg0, int arg1, int arg2,
                                                  int arg3) {
                        // TODO Auto-generated method stub
                    }
                });

                lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                        Log.e("item", parent.getItemAtPosition(position).toString());
                        dialog.dismiss();
                        Log.e("item1", parent.getItemAtPosition(position) + "");

                        String carName = parent.getItemAtPosition(position).toString();
                        int index = -1;
                        for (int i = 0; i < blank.length; i++) {
                            if (blank[i].equals(carName)) {
                                index = i;
                                break;
                            }
                        }
                        String cs = countycodes[index];
                        input_country_code.setText(cs);
                        // tvCountrySelect.setText(parent.getItemAtPosition(position).toString());
                    }
                });

                dialog.show();

                break;

            case R.id.continueReg:

                if (InternetCheck.getConnectivityStatus(getApplicationContext()))
                {
                    if (validation())
                    {
                        String firstname = input_name_first.getText().toString();
                        String lastname = input_name_last.getText().toString();
                        String email = input_email.getText().toString();
                        String country_code =  input_country_code.getText().toString().substring(1, input_country_code.getText().length());
                        String mobile = input_mobile.getText().toString();
                        String password = input_password.getText().toString();
                        String referCode = input_referCode.getText().toString();

                        Constants.registerModel.setFirstname(firstname);
                        Constants.registerModel.setLastname(lastname);
                        Constants.registerModel.setEmail(email);
                        Constants.registerModel.setMobile(mobile);
                        Constants.registerModel.setPassword(password);
                        Constants.registerModel.setCountryCode(country_code);
                        Constants.registerModel.setReferCode(referCode);

                        startActivity(new Intent(RegisterActivity.this, VerifyMobileActivity.class));
                    }
                }
                else {
                    Camera_Gallery.openSnackBar(continueReg, getResources().getString(R.string.nointernet), getApplicationContext());
                }
                break;

        }
    }

    public boolean validation() {

        if (Validation.isNotEmpty(input_name_first, "Please enter firstname") && Validation.isNotEmpty(input_name_last, "Please enter lastname") && Validation.isValidateEmailAddress(input_email, "Please enter email") && Validation.isValidContact(input_mobile, "Please enter mobile") && Validation.isPassword(input_password, "Password must be more than 6 digit")) {
            return true;
        }
        return false;

    }

}
