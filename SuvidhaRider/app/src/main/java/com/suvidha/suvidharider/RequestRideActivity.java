package com.suvidha.suvidharider;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.app.TimePickerDialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.TimePicker;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;
import com.google.gson.Gson;
import com.suvidha.suvidharider.Api.BaseClass;
import com.suvidha.suvidharider.Interface.GetRequestResponse;
import com.suvidha.suvidharider.Interface.RetrofitClient;
import com.suvidha.suvidharider.Interface.RetrofitInterface;
import com.suvidha.suvidharider.Models.BookingModel;
import com.suvidha.suvidharider.Models.BookingResponseModel;
import com.suvidha.suvidharider.Models.RegisterResponseModel;
import com.suvidha.suvidharider.Utilities.Camera_Gallery;
import com.suvidha.suvidharider.Utilities.Constants;
import com.suvidha.suvidharider.Utilities.InternetCheck;
import com.suvidha.suvidharider.Utilities.SharedPreference;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RequestRideActivity extends BaseClass implements OnMapReadyCallback, View.OnClickListener {

    private TextView tvPromocode, tvConfirm, tvBookLater, tvFromname, tvToName, tvChange;
    private ImageButton backImageButton;
    Polyline line;
    private GoogleMap mMap;
    private LinearLayout llFareEsti;
    private int mYear, mMonth, mDay, mHour, mMinute;
    ProgressDialog dialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_request_ride);

        initialize();

        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentByTag("mapFragment");
        if (mapFragment == null) {
            mapFragment = new SupportMapFragment();
            FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
            ft.add(R.id.mapFragmentContainer, mapFragment, "mapFragment");
            ft.commit();
            getSupportFragmentManager().executePendingTransactions();
        }
        mapFragment.getMapAsync(this);

        tvFromname.setText(getIntent().getStringExtra(Constants.FROM_LOCATION));
        tvToName.setText(getIntent().getStringExtra(Constants.TO_LOCATION));

        tvPromocode.setOnClickListener(this);
        tvConfirm.setOnClickListener(this);
        tvBookLater.setOnClickListener(this);
        backImageButton.setOnClickListener(this);
        llFareEsti.setOnClickListener(this);
        tvChange.setOnClickListener(this);


    }

    private void initialize() {

        tvPromocode = (TextView) findViewById(R.id.tvPromocode);
        tvConfirm=(TextView) findViewById(R.id.tvConfirm);
        tvBookLater =(TextView) findViewById(R.id.tvBookLater);
        backImageButton = (ImageButton)findViewById(R.id.back);
        tvFromname = (TextView) findViewById(R.id.tvFromName);
        tvToName = (TextView) findViewById(R.id.tvToName);
        tvChange = (TextView) findViewById(R.id.tvChange);
        llFareEsti = (LinearLayout) findViewById(R.id.llFareEsti);
    }

    private List<LatLng> decodePoly(String encoded) {
        List<LatLng> poly = new ArrayList<LatLng>();
        int index = 0, len = encoded.length();
        int lat = 0, lng = 0;

        while (index < len) {
            int b, shift = 0, result = 0;
            do {
                b = encoded.charAt(index++) - 63;
                result |= (b & 0x1f) << shift;
                shift += 5;
            } while (b >= 0x20);
            int dlat = ((result & 1) != 0 ? ~(result >> 1) : (result >> 1));
            lat += dlat;

            shift = 0;
            result = 0;
            do {
                b = encoded.charAt(index++) - 63;
                result |= (b & 0x1f) << shift;
                shift += 5;
            } while (b >= 0x20);
            int dlng = ((result & 1) != 0 ? ~(result >> 1) : (result >> 1));
            lng += dlng;

            LatLng p = new LatLng((((double) lat / 1E5)),
                    (((double) lng / 1E5)));
            poly.add(p);
        }

        return poly;
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        mMap.clear();
        List<LatLng> list = decodePoly(Constants.ENCODED_STRING);
        line = mMap.addPolyline(new PolylineOptions()
                .addAll(list)
                .width(10)
                .color(getResources().getColor(R.color.colorPrimary))
                .geodesic(true)
        );

       LatLng from = new LatLng(Double.valueOf(Constants.FROM_LATTITUDE), Double.valueOf(Constants.FROM_LONGITUDE));
        LatLng to = new LatLng(Double.valueOf(Constants.TO_LATTITUDE), Double.valueOf(Constants.TO_LONGITUDE));

        LatLngBounds.Builder builder = new LatLngBounds.Builder();
        builder.include(from).include(to);
        CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngBounds(builder.build(), 100);
        mMap.moveCamera(cameraUpdate);

        mMap.addMarker(new MarkerOptions().position(from).icon(BitmapDescriptorFactory.fromResource(R.drawable.markernew)));
        mMap.addMarker(new MarkerOptions().position(to).icon(BitmapDescriptorFactory.fromResource(R.drawable.markernew)));
    }

    @Override
    public void onClick(View view) {
        switch (view.getId())
        {
            case R.id.tvPromocode:
                final Dialog dialogPromocode = new Dialog(RequestRideActivity.this);
                dialogPromocode.requestWindowFeature(Window.FEATURE_NO_TITLE);
                dialogPromocode.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                dialogPromocode.setContentView(R.layout.dialog_promocode);

                TextView tvCancel = (TextView) dialogPromocode.findViewById(R.id.tvCancel);

                tvCancel.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        dialogPromocode.dismiss();
                    }
                });

                dialogPromocode.show();

                break;

            case R.id.tvConfirm:

                saveBookingDetails();
                break;

            case R.id.tvBookLater:
                final Dialog dialogLater = new Dialog(RequestRideActivity.this);
                dialogLater.requestWindowFeature(Window.FEATURE_NO_TITLE);
                dialogLater.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                dialogLater.setContentView(R.layout.dialog_booking_later);
                TextView DoneTextView1=(TextView)dialogLater.findViewById(R.id.done);
                TextView CancelTextView1=(TextView)dialogLater.findViewById(R.id.cancel);
                final EditText etDate=(EditText)dialogLater.findViewById(R.id.etDate);
                final EditText etTime=(EditText)dialogLater.findViewById(R.id.etTime);

                etDate.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        final Calendar c = Calendar.getInstance();
                        mYear = c.get(Calendar.YEAR);
                        mMonth = c.get(Calendar.MONTH);
                        mDay = c.get(Calendar.DAY_OF_MONTH);

                        DatePickerDialog datePickerDialog = new DatePickerDialog(RequestRideActivity.this,
                                new DatePickerDialog.OnDateSetListener() {

                                    @Override
                                    public void onDateSet(DatePicker view, int year,
                                                          int monthOfYear, int dayOfMonth) {

                                        etDate.setText(dayOfMonth + "-" + (monthOfYear + 1) + "-" + year);

                                    }
                                }, mYear, mMonth, mDay);
                        datePickerDialog.getDatePicker().setMinDate(System.currentTimeMillis());
                        datePickerDialog.show();
                    }
                });

                etTime.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        final Calendar c = Calendar.getInstance();
                        mHour = c.get(Calendar.HOUR_OF_DAY);
                        mMinute = c.get(Calendar.MINUTE);

                        // Launch Time Picker Dialog
                        TimePickerDialog timePickerDialog = new TimePickerDialog(RequestRideActivity.this,
                                new TimePickerDialog.OnTimeSetListener() {

                                    @Override
                                    public void onTimeSet(TimePicker view, int hourOfDay,
                                                          int minute) {

                                        String status = "AM";

                                        if(hourOfDay > 11)
                                        {
                                            // If the hour is greater than or equal to 12
                                            // Then the current AM PM status is PM
                                            status = "PM";
                                        }

                                        // Initialize a new variable to hold 12 hour format hour value
                                        int hour_of_12_hour_format;

                                        if(hourOfDay > 11){

                                            // If the hour is greater than or equal to 12
                                            // Then we subtract 12 from the hour to make it 12 hour format time
                                            hour_of_12_hour_format = hourOfDay - 12;
                                        }
                                        else {
                                            hour_of_12_hour_format = hourOfDay;
                                        }

//                                        etTime.setText(hourOfDay + ":" + minute);
                                        etTime.setText(hour_of_12_hour_format + " : " + minute + " : " + status);
                                    }
                                }, mHour, mMinute, false);
                        timePickerDialog.show();
                    }
                });


                DoneTextView1.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        dialogLater.cancel();
                    }
                });
                CancelTextView1.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        dialogLater.cancel();
                    }
                });

                dialogLater.show();

                break;

            case R.id.llFareEsti:
                finish();
                break;

            case R.id.back:
                finish();
                break;

            case R.id.tvChange:
                startActivity(new Intent(RequestRideActivity.this,PaymentActivity.class));
                break;
        }
    }

    private void saveBookingDetails() {
        if(InternetCheck.getConnectivityStatus(getBaseContext()))
        {

            dialog = new ProgressDialog(RequestRideActivity.this);
            dialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            dialog.setMessage("Loading...");
            dialog.show();
            BookingModel bookingModel = new BookingModel();

            bookingModel.setFirstname(SharedPreference.getString(getApplicationContext(), Constants.FIRSTNAME));
            bookingModel.setLastname(SharedPreference.getString(getApplicationContext(), Constants.LASTNAME));
            bookingModel.setLatitude(Constants.FROM_LATTITUDE);
            bookingModel.setLongitude(Constants.FROM_LONGITUDE);
            bookingModel.setMobile(SharedPreference.getString(getApplicationContext(), Constants.MOBILE));
            bookingModel.setService(Constants.SERVICE);
            bookingModel.setUserId(SharedPreference.getString(getApplicationContext(), Constants.USER_ID));
            bookingModel.setTolatitude(Constants.TO_LATTITUDE);
            bookingModel.setTolongitude(Constants.TO_LONGITUDE);


            if (apiConnection != null) {
                apiConnection.confirmride(bookingModel, new GetRequestResponse() {
                    @Override
                    public void onResponse(String json) {
                        Gson gson = new Gson();
                        final BookingResponseModel bookingResponseModel = gson.fromJson(json, BookingResponseModel.class);
                        if (bookingResponseModel.getSuccess().equals("true")) {
                            Constants.bookingResponseModel = bookingResponseModel;

                            final Dialog dialogConfirm = new Dialog(RequestRideActivity.this);
                            dialogConfirm.requestWindowFeature(Window.FEATURE_NO_TITLE);
                            dialogConfirm.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                            dialogConfirm.setContentView(R.layout.dialog_booking_successfull);
                            dialogConfirm.setCancelable(false);
                            TextView DoneTextView = (TextView) dialogConfirm.findViewById(R.id.done);
                            TextView CancelTextView = (TextView) dialogConfirm.findViewById(R.id.cancel);
                            final TextView tvMinutes = (TextView) dialogConfirm.findViewById(R.id.tvMinutes);
                            tvMinutes.setText("Your booking has been confirmed.\n Driver will pickup you in " + bookingResponseModel.getDriverDetailsModels().get(0).getTime());
                            DoneTextView.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {
                                    dialogConfirm.cancel();

                                    Intent i = new Intent(RequestRideActivity.this, PickupArrivingActivity.class);
                                    i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                    i.putExtra(Constants.FROM_LOCATION, tvFromname.getText().toString());
                                    i.putExtra(Constants.TO_LOCATION, tvToName.getText().toString());
                                    i.putExtra("reachtime", bookingResponseModel.getDriverDetailsModels().get(0).getTime());
                                    startActivity(i);
                                }
                            });
                            CancelTextView.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {
                                    dialogConfirm.cancel();
                                }
                            });

                            dialogConfirm.show();
                            dialog.dismiss();
                        }
                    }

                    @Override
                    public void onFailure(String json) {
                        Gson gson = new Gson();
                        BookingResponseModel bookingResponseModel = gson.fromJson(json, BookingResponseModel.class);

                        dialog.dismiss();
                        Camera_Gallery.openSnackBar(tvConfirm, bookingResponseModel.getMessage(), getApplicationContext());

                    }
                });
            }

        }
        else
        {
            Camera_Gallery.openSnackBar(tvConfirm, getResources().getString(R.string.nointernet), getApplicationContext());

        }
    }
}
