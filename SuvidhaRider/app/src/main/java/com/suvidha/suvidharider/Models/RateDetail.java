package com.suvidha.suvidharider.Models;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class RateDetail {

    @SerializedName("Service")
    @Expose
    private String service;
    @SerializedName("basic_rate")
    @Expose
    private String basicRate;
    @SerializedName("km_rate")
    @Expose
    private String kmRate;
    @SerializedName("min_rate")
    @Expose
    private String minRate;
    @SerializedName("timetoreach")
    @Expose
    private Object timetoreach;
    @SerializedName("maxpersons")
    @Expose
    private String maxpersons;

//    @SerializedName("ServiceId")
//    @Expose
//    private String serviceId;

    public String getService() {
        return service;
    }

    public void setService(String service) {
        this.service = service;
    }

    public String getBasicRate() {
        return basicRate;
    }

    public void setBasicRate(String basicRate) {
        this.basicRate = basicRate;
    }

    public String getKmRate() {
        return kmRate;
    }

    public void setKmRate(String kmRate) {
        this.kmRate = kmRate;
    }

    public String getMinRate() {
        return minRate;
    }

    public void setMinRate(String minRate) {
        this.minRate = minRate;
    }

    public Object getTimetoreach() {
        return timetoreach;
    }

    public void setTimetoreach(Object timetoreach) {
        this.timetoreach = timetoreach;
    }

    public String getMaxpersons() {
        return maxpersons;
    }

    public void setMaxpersons(String maxpersons) {
        this.maxpersons = maxpersons;
    }

//    public String getServiceId() {
//        return serviceId;
//    }
//
//    public void setServiceId(String serviceId) {
//        this.serviceId = serviceId;
//    }
}