package com.suvidha.suvidharider.Adapters;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.suvidha.suvidharider.Interface.RecyclerItemClickInterface;
import com.suvidha.suvidharider.Models.CabTypesModel;
import com.suvidha.suvidharider.Models.RateDetail;
import com.suvidha.suvidharider.Models.ReasonsModel;
import com.suvidha.suvidharider.R;
import com.suvidha.suvidharider.Utilities.Camera_Gallery;
import com.suvidha.suvidharider.Utilities.Constants;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by cybertruimph on 3/17/2018.
 */

public class RecyclerCabTypes extends RecyclerView.Adapter<RecyclerCabTypes.MyCredits> {

    private List<RateDetail> pList;
    private int selectedPosition = -1;
    private View itemView;
    RecyclerItemClickInterface recyclerItemClickInterface;

    public RecyclerCabTypes(List<RateDetail> pList, RecyclerItemClickInterface recyclerItemClickInterface) {
        this.pList = pList;
        this.recyclerItemClickInterface = recyclerItemClickInterface;
    }

    public void updateRecycle(List<RateDetail> pList)
    {
        this.pList = pList;
        notifyDataSetChanged();
    }

    public class MyCredits extends RecyclerView.ViewHolder {

        private final TextView tvCabTime, tvService;
        private final ImageView imCabImage;

        public MyCredits(View itemView) {
            super(itemView);
            tvCabTime = (TextView) itemView.findViewById(R.id.tvCabTime);
            tvService = (TextView) itemView.findViewById(R.id.tvService);
            imCabImage = (ImageView) itemView.findViewById(R.id.imCabImage);

        }
    }

    @Override
    public RecyclerCabTypes.MyCredits onCreateViewHolder(ViewGroup parent, int viewType) {
        itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.custom_cab_list, parent, false);
        return new RecyclerCabTypes.MyCredits(itemView);
    }

    @Override
    public void onBindViewHolder(final RecyclerCabTypes.MyCredits holder, final int position) {

        holder.tvService.setText(pList.get(position).getService());

        if (pList.get(position).getTimetoreach() == null) {
            holder.tvCabTime.setText("No cabs");
        } else {
            holder.tvCabTime.setText(pList.get(position).getTimetoreach().toString());
        }

        if (position == 0) {
            holder.imCabImage.setImageResource(R.drawable.circlecar1_100);
        }
        if (position == 1) {
            holder.imCabImage.setImageResource(R.drawable.circlecar3_100);
        }
        if (position == 2) {
            holder.imCabImage.setImageResource(R.drawable.circlecar2_100);
        }

        if (position == selectedPosition) {
            if (pList.get(position).getService().equals("Cars")) {
                holder.imCabImage.setImageResource(R.drawable.circlecar1_150);
            }
            if (pList.get(position).getService().equals("ERikshaw")) {
                holder.imCabImage.setImageResource(R.drawable.circlecar3_150);
            }
            if (pList.get(position).getService().equals("Auto")) {
                holder.imCabImage.setImageResource(R.drawable.circlecar2_150);
            }

            ////////////////////////////////////////

//            if (pList.get(position).getServiceId().equals("1")) {
//                holder.imCabImage.setImageResource(R.drawable.circlecar1_150);
//            }
//            if (pList.get(position).getServiceId().equals("2")) {
//                holder.imCabImage.setImageResource(R.drawable.circlecar3_150);
//            }
//            if (pList.get(position).getServiceId().equals("3")) {
//                holder.imCabImage.setImageResource(R.drawable.circlecar2_150);
//            }
        }

        holder.imCabImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                selectedPosition = holder.getAdapterPosition();

                if (pList.get(position).getTimetoreach() != null) {

                    recyclerItemClickInterface.onClick(pList.get(position).getService(), position);

                } else {

                    Camera_Gallery.openSnackBar(view, "No cabs available", itemView.getContext());
                }

                notifyDataSetChanged();
            }
        });
    }

    @Override
    public int getItemCount() {
        return pList.size();
    }

}
