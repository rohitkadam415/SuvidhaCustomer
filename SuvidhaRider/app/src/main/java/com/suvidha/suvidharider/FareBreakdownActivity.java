package com.suvidha.suvidharider;

import android.content.Intent;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;

import com.google.gson.Gson;
import com.suvidha.suvidharider.Api.BaseClass;
import com.suvidha.suvidharider.Interface.GetRequestResponse;
import com.suvidha.suvidharider.Models.DriverModel;
import com.suvidha.suvidharider.Models.RatingModel;
import com.suvidha.suvidharider.Models.TripDetails;
import com.suvidha.suvidharider.Utilities.Camera_Gallery;
import com.suvidha.suvidharider.Utilities.Constants;
import com.suvidha.suvidharider.Utilities.SharedPreference;

import java.text.DecimalFormat;

public class FareBreakdownActivity extends BaseClass implements View.OnClickListener {

    private ImageButton back;
    private TextView tvTotalFare, tvtotalDistance, tvFrom, tvTo, tvTripFare, tvTolls, tvRiderDiscount, tvOutstanding, tvTotal, tvParking, tvDueAmt;
    private Button btnShare;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fare_breakdown);
        initialize();

        back.setOnClickListener(this);
        btnShare.setOnClickListener(this);

    }

    @Override
    protected void apiConnected() {
        bindData();
    }

    private void bindData() {

        if (apiConnection != null)
        {
            RatingModel ratingModel = new RatingModel();
            ratingModel.setUserId(SharedPreference.getString(getBaseContext(), Constants.USER_ID));
            ratingModel.setDriverId(getIntent().getStringExtra("DriverId"));
            ratingModel.setRequestId(getIntent().getStringExtra("RequestId"));
            apiConnection.getInvoiceDetails(ratingModel, new GetRequestResponse() {
                @Override
                public void onResponse(String json) {

                    Gson gson = new Gson();
                    final TripDetails tripDetails = gson.fromJson(json, TripDetails.class);

                    tvTotalFare.setText(getString(R.string.Rs)+" "+formatFigureTwoPlaces(tripDetails.getCompleteRideDetails().get(0).getTotalAmount()));
                    tvtotalDistance.setText(tripDetails.getCompleteRideDetails().get(0).getDistance());
                    tvFrom.setText(Camera_Gallery.getAddressFromLocation(getBaseContext(), Double.valueOf(tripDetails.getCompleteRideDetails().get(0).getFromLatitude()), Double.valueOf(tripDetails.getCompleteRideDetails().get(0).getFromLongitude())));
                    tvTo.setText(Camera_Gallery.getAddressFromLocation(getBaseContext(), Double.valueOf(tripDetails.getCompleteRideDetails().get(0).getToLatitude()), Double.valueOf(tripDetails.getCompleteRideDetails().get(0).getToLongitude())));
                    tvTripFare.setText(formatFigureTwoPlaces(tripDetails.getCompleteRideDetails().get(0).getFareAmount()));
                    tvTolls.setText(formatFigureTwoPlaces(tripDetails.getCompleteRideDetails().get(0).getTollAmount()));
                    tvTotal.setText(formatFigureTwoPlaces(tripDetails.getCompleteRideDetails().get(0).getTotalAmount()));
                    tvParking.setText(formatFigureTwoPlaces(tripDetails.getCompleteRideDetails().get(0).getParkingAmount()));
                    tvDueAmt.setText(formatFigureTwoPlaces(tripDetails.getCompleteRideDetails().get(0).getDueAmount()));

                }

                @Override
                public void onFailure(String json) {

                }
            });
        }

    }

    public String formatFigureTwoPlaces(String value) {

        float val = Float.valueOf(value);
        DecimalFormat myFormatter = new DecimalFormat("##0.00");
        return myFormatter.format(val);
    }

    private void initialize() {

        back = (ImageButton) findViewById(R.id.back);
        tvTotalFare = (TextView) findViewById(R.id.tvTotalFare);
        tvtotalDistance = (TextView) findViewById(R.id.tvtotalDistance);
        tvFrom = (TextView) findViewById(R.id.tvFrom);
        tvTo = (TextView) findViewById(R.id.tvTo);

        tvTripFare = (TextView) findViewById(R.id.tvTripFare);
        tvTolls = (TextView) findViewById(R.id.tvTolls);
        tvRiderDiscount = (TextView) findViewById(R.id.tvRiderDiscount);
        tvOutstanding = (TextView) findViewById(R.id.tvOutstanding);
        tvParking = (TextView) findViewById(R.id.tvParking);
        tvDueAmt = (TextView) findViewById(R.id.tvDueAmt);
        tvTotal = (TextView) findViewById(R.id.tvTotal);

        btnShare = (Button) findViewById(R.id.btnShare);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId())
        {
            case R.id.back:
                finish();
                break;

            case R.id.btnShare:
                Intent intent = new Intent(FareBreakdownActivity.this, HomeActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent);
                break;
        }
    }
}
